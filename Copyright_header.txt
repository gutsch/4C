Name...
Function of program...

Copyright (C) 1996-2018 Potsdam Institute for Climate Impact Reserach (PIK)
authors and contributors see AUTHOR file.
This file is part of 4C and is licensed under BSD-2-Clause. 
See LICENSE file or under http://www.https://opensource.org/licenses/BSD-2-Clause   
Contact: https://gitlab.pik-potsdam.de/foresee/4C
********************************************************************************