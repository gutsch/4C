README 4C - FORSEE - FORESt Ecosystems in a changing Environment

This file contains basic information on the open source distribution of
the computer simulation forest model 4C. The model has been developed and is maintained
at the Potsdam Institute for Climate Impact Research (PIK) in Potsdam, Germany,
see "opensource_PIK_approval".

All source code and documents provided are subject to copyright (c) by the 
Potsdam Institute for Climate Impact Research. All files are licenced under 
BSD 2-Clause License, see the LICENCE file for details. The 4C model is a 
collaborative work, see the AUTHOR file for details.

- See http://www.pik-potsdam.de/4c/ for information about 4C and the manual
- For the model description see 4C_description.pdf in the directory descriptions
- The directory executables contains three executables with self-explanatory names
- The directory source_code contains the corresponding source codes in three subdirectories with self-explanatory names
- Additional information can be found in following directories:
    * manuals: about "how to start" and " generate exe"  
    * descriptions/additional: background information documents 
    * descriptions/Fact_sheets: short summary of the model in English and German
    * source_code/additional: overview on variables with their names in the 4C description and the names in the source code
                              flow chart of the model 4C 
    * test_data_sets: test the correct function mode of the model with complete sets of input data and control files 
    * publications: relevant publications regarding the model 4C

The source code is distributed via a git repository at: 
https://gitlab.pik-potsdam.de/foresee/4C and is ready to use.
The code language is Fortran90. Since the model development is
finished, there is no further development and no further support in model 
download, setup, development or application. No merge or other development 
requests will be handeled. Therefore there are no code style recommendations or
instructions on how to create source code amendments.







