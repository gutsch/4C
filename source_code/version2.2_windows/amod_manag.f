!*****************************************************************!
!*                                                               *!
!*              4C (FORESEE) Simulation Model                    *!
!*                                                               *!
!*                                                               *!
!*      data module management SR                                *!
!*                                                               *!
!*                  Copyright (C) 1996-2018                      *!
!*     Potsdam Institute for Climate Impact Reserach (PIK)       *!
!*          Authors and contributors see AUTHOR file             *!
!*  This file is part of 4C and is licensed under BSD-2-Clause   *!
!*                   See LICENSE file or under:                  *!
!*     http://www.https://opensource.org/licenses/BSD-2-Clause   *!
!*                           Contact:                            *!
!*       https://gitlab.pik-potsdam.de/foresee/4C                *!
!*                                                               *!
!*****************************************************************!
module data_manag

real    ::  basarea_tot           ! total basal area
real    ::  tardiam_dstem=15.     ! diameter target for dead stems to C_opm_stems

integer ::  thin_type     ! type of management scenario
integer ::  thin_nr       ! Number of thinnings (years with management actions)
integer ::  act_thin_year ! year field index of thinning
integer ::  target_type     ! type of thinning in case of target thinning
integer,allocatable,save,dimension(:) :: thin_year ! Field of management years
integer,allocatable,save,dimension(:) :: thin_age  ! stand age of target thinning
integer,allocatable,save,dimension(:) :: thin_tree ! number of remaining stems after thinning
integer,allocatable,save,dimension(:) :: thin_spec ! species number for thinning (target)
integer,allocatable,save,dimension(:) :: thin_tysp ! type of thinning (for target thinning)
real, allocatable,save,dimension(:) :: target_mass ! target value of stem mass
integer,allocatable,save,dimension(:) :: thinyear  ! year of last thinning
integer, allocatable, save, dimension(:) :: thin_stor ! information of storey which hase to manage
real,allocatable,save,dimension(:)     :: np_mod      ! multiplier for 'Nutzungsprozent'
integer  :: thin_dead = 0  ! 0 dead stembiomass is accumulated in litter pools
                           ! 1 dead stem biomass is removed as harvested
integer   :: domspec       ! dominant species of initialised stnad for management
integer   :: domspec_reg   ! dominant species of regenerated/planted stand after clear cut/shelterwood

real      :: stump_sum= 0
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

!  thinning types
!  1 - Niederdurchforstung (m��ig)  low thinning ( moderate)
!  2 - Niederdurchforstung (stark)  low thinning (heavy)
!  3 - Hochdurchforstung            crown thinning
!  4 - Auslesedurchforstung         selective thinning

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

real    :: ho1=0.       !  dominant height first thinning
real    :: ho2=0.       !  dominant height second thinning
real    :: ho3=0.       !  dominant height third thinning
real    :: ho4=0.       !  dominant height fourth thinning
integer :: thr1=0       !  thinning regime for ho2
integer :: thr2=0       !  thinning regime for ho3
integer :: thr3=0       !  thinning regime for ho4
integer :: thr4=0       !  thinning regime for ho>ho4
integer :: thr5=0       !  'R�ckegassen'
real    :: thr6=0.      ! if thr5=1 this flag control time of realization =ho1,ho2,ho3 or ho4
integer :: thr7=0       ! management regime for rotation year
integer :: mgreg=0      ! regeneration, natural/artificial
integer :: thin_ob      ! control of optimal basal area thinning, =1 yes, =0 no
real    :: optb= 1.     ! optimal 'Bestockungsgrad'
integer :: thinonce =0  ! special case of managemnet for only one single management activity; default=0
integer, allocatable,save,dimension(:)   :: thin_flag1  ! aux. varaibles for adaptive management
integer, allocatable,save,dimension(:)   :: thin_flag2
integer, allocatable,save,dimension(:)   :: thin_flag3
integer, allocatable,save,dimension(:)   :: thin_flag4
real, allocatable, save, dimension(:) :: zbnr     ! number of 'Zielb�ume'/target trees

real, allocatable, save, dimension(:) :: tend        ! percentage of young tree tending/'tending of plantations'
integer, allocatable,save,dimension(:)   :: rot      ! rotation
integer, allocatable, save, dimension(:) :: regage   ! age of natural/planted regeneration
integer  :: flag_direct=0 !
integer  :: thinstep=0    ! number of years between thinning  if ho>ho4
integer  :: flag_brush=1  ! defaul, if 1 then all harvested stems remain in the litter and are not removed from the stand
integer  :: cutyear =0    ! year of cutting
real     :: direcfel=0.   ! percentage display of 'R�ckegassen' creation 'directional felling'
real     :: limit=0.      ! limit f�r hight query (+- range)

integer  :: shelteryear=0   ! year of last shelterwood mang.
integer  :: stand_age =0    ! age of stand
integer  :: flag_manreal=0  ! management no/yes
integer  :: flag_shelter = 0! shelterwood management started
integer  ::  flag_sh_first=0   ! aux variable for the case age(1) > regage and age(1)> rotage-20
integer  :: flag_plant_shw = 0 ! flag for planting in specieal case trhat initial age is > rot-20
character(30) ::  maninf     ! description of measure
integer  :: meas             ! flag of measure
! parameter for thinning depending on age ang stand density : percent of using
real, dimension(20,20)    :: usp

! multi-species management
integer,allocatable,save,dimension(:)  :: specnr, age_spec,anz_tree_spec

! Austrian management
integer,dimension(10)         :: num_rel_cl     ! number of relative diameter classes
integer                       :: num_man        ! total numbe rof management treatments
integer, allocatable, save, dimension(:)   ::  yman      ! years of management for each species
integer, allocatable, save, dimension(:)   ::  dbh_clm   ! number of relative dbh class wihich is used for thinning
integer,  allocatable, save, dimension(:)   :: spec_man  ! number of species for treatment
real, allocatable, save, dimension(:)      ::  rem_clm   ! removal of biomass
integer, allocatable, save, dimension(:)   ::  act       ! activity flag
real, allocatable,save, dimension(:)       ::  rel_part  ! mixture flag for planting

! disturbance management
integer, allocatable, save, dimension(:)      :: dis_id                  ! number of standid with disturbance
integer                                       :: dis_row_nr              ! the total number of disturbance events (line number of disturbance section)
integer, dimension(1:6,1:2)                   :: dis_control             ! array which is used to control the dirsturbance simulation (dim1=disturbance type(D,X,P,R,S), dim2=zeile man-file)
character(1), allocatable, save, dimension(:) :: dis_type                ! disturbance type D - defoliator, X - xylem clogger,  P - phloem feeder
                                                                         ! R - root pathogen or feeder, S - stem rot
integer, allocatable, save, dimension(:)      :: fortype                 ! forest type 1 managed forest, 2 - naturla forest
integer, allocatable, save, dimension(:)      :: dis_year                ! date of disturbance
integer, allocatable, save, dimension(:)      :: dis_spec                ! disturbed tree species
integer, allocatable, save, dimension(:)      :: dis_start               ! start of disturbance within year
real, allocatable, save, dimension(:)         :: dis_rel                 ! relative value of disturbed area
real, allocatable,save, dimension(:)          :: sum_dis                 ! accumulated value of disturbed area (relative), for each standid
real, allocatable, save, dimension(:)           :: dis_year_id           ! year of last disturbance for each standid
integer                                 ::dis_number
integer                                 :: count_dis_real =0             ! counter for realised disturbances

! aspen managment
integer                         :: nsprout  = 3     ! number of sprouts per tree
integer                         :: flag_sprout = 0  ! 0 - sprouting 1-if sprouts exist

! liocourt management
real                     :: dbh_max      ! maximum diameter
real                     :: lic_a        ! parameter a of licourt function
real                     :: lic_b        ! parameter b of licourt function
real                     :: thin_proc    ! volume removal percent
integer                  :: spec_lic      ! species number f�r li management
integer                  :: thin_int      ! thinning interval
integer, dimension(22,11) ::  ntree_lic   ! filed for calculation of licourt function  for species i and diamter class j

end module data_manag
