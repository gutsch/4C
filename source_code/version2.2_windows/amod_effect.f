!*****************************************************************!
!*                                                               *!
!*              4C (FORESEE) Simulation Model                    *!
!*                                                               *!
!*            environmental variables and indices                *!
!*                                                               *!
!*    containes:                                                 *!
!*    DATA_BIODIV                                                *!
!*    DATA_FROST                                                 *!
!*                                                               *!
!*                  Copyright (C) 1996-2018                      *!
!*     Potsdam Institute for Climate Impact Reserach (PIK)       *!
!*          Authors and contributors see AUTHOR file             *!
!*  This file is part of 4C and is licensed under BSD-2-Clause   *!
!*                   See LICENSE file or under                   *!
!*     http://www.https://opensource.org/licenses/BSD-2-Clause   *!
!*                           Contact:                            *!
!*       https://gitlab.pik-potsdam.de/foresee/4C                *!
!*                                                               *!
!*****************************************************************!

module data_biodiv
! indices of fire and biodiversity

! sum of hot days (Tmax>=25�C) and precipitation sum in the potential fire period
integer ::  Ndayshot
real    ::  Psum_FP
integer :: prec_flag1 = 0            ! flag  is equal 1 if first time precipitation of 5 mm occurs after bud burst of birch
integer :: prec_flag2 = 0            

real    ::  ntindex                  !  temperature index

! fire risk
integer ::  fire_indw         = -99  ! fire index west
integer ::  fd_fire_indw(1:5) = 0    ! frequency distribution of fore index west values
integer ::  fire_inde         = -99  ! fire index east
integer ::  fire_indi_day     = 0    ! days with forest fire indicator greater then a threshold (east)
real    ::  fire_indi         = 0.0  ! forest fire indicator (east)
real    ::  fire_indi_max     = 0.0  ! maximum forest fire indicator (east)
real    ::  fire_indb         = -99  ! fire index Bruschek
real    ::  fire_indb_m       = -99  ! mean yearly fire index Bruschek of simulation period
real    ::  tsumrob                  ! temperature sum 'Robinie'
real    ::  day_bb_rob        = 0    ! day of budburst 'Robinie'
real    ::  tsumbi                   ! temperature sum birch
real    ::  day_bb_bi         = 0    ! day of budburst birch
integer ::  day_nest          = 0    ! days since the last prec. greater then 3 mm (Nesterov)
real    ::  p_nest            = 0.0  ! ignition index of Nesterov

type fire_risk 
   integer               :: index    ! daily fire risk level
   integer, dimension (5):: frequ    ! frequency of of fire risk levels (5 classes) of a year
   real                  :: mean     ! mean fire risk level of a year
   real                  :: mean_m   ! mean yearly fire risk level of simulation period
end type fire_risk

type (fire_risk),dimension(3) :: fire    !  1 - fire index west
                                         !  2 - fire index east (M68 international)
                                         !  3 - fire index Nesterov

! upper limit of climatic water balance for fire risk class (west)
real, dimension(4,7) :: risk_class
DATA risk_class &
               / 5.,  -3.,  -9., -15., &  ! march
                 3.,  -8., -16., -27., &  ! april
                -3., -16., -25., -35., &  ! may
               -12., -24., -32., -41., &  ! june
               -12., -24., -31., -40., &  ! july
                -8., -20., -28., -37., &  ! august
                -6., -18., -26., -35./    ! september

integer, dimension(38)  :: daybb_rob
integer, dimension(38)  :: daybb_bi
DATA daybb_bi/100,114,115,113,120,115,111,109,123,124,113,110,119,99,117,117,118,117,120,124,101,117,113,117,112,119,116,112,  &
              102,92,106,109,109,110,111,112,112,101/


DATA daybb_rob/152,165,156,151,166,148,153,153,160,163,151,161,160,163,159,161,162,158,153,163,153,153,154,159,151,152,166,154, &
               147,143,168,145,135,148,151,155,155,138/

end module data_biodiv

module  data_frost

integer, allocatable, save, dimension(:)  :: dnlf               ! number of days with late frost during vegetation period
real, allocatable, save, dimension (:)    :: tminmay_ann        ! minimum temperature in may
integer, allocatable, save, dimension(:)  :: date_lf            ! date of last frost after start of vegetation period per year
integer, allocatable, save, dimension(:)  :: date_lftot         ! annual date of last frost event
integer, allocatable, save, dimension(:)  :: anzdlf             ! number of days with frost from April until June
integer, allocatable, save, dimension(:)  :: sumtlf             ! sum of temperature of days with frost from April until June
integer                                   :: dlfabs             ! number of day of the last frost for the whole simulation period
real                                      :: tminmay            ! minimum temperature of may of the whole simulation period
integer, allocatable, save, dimension(:)  :: dnlf_sp            ! number of day with late frost during vegetation period
integer                                   :: dlfabs_sp          ! number of day of the last frost for the whole simulation period
real                                      :: tminmay_sp         ! minimum temperature of may of the whole simulation period

real                                      :: temp_frost = 0.    ! temperature threshold of frost
integer                                   :: lfind              ! last frost index
real                                      :: mlfind             ! mean lfind
integer                                   :: maxlfind           ! maximum value of 5 part inidces
integer                                   :: lfind_sp           ! last frost index  birch
integer                                   :: maxlfind_sp        ! last frost index  beech
real                                      :: mlfind_sp          ! mean lfind
integer                                   :: taxnum
end module data_frost

