!*****************************************************************!
!*                                                               *!
!*              Post Processing for 4C (FORESEE)                 *!
!*                                                               *!
!*                                                               *!
!*                  Modules and Subroutines:                     *!
!*                                                               *!
!*      data_tsort:  module to store timber assortments          *!
!*                                                               *!
!*                  Copyright (C) 1996-2018                      *!
!*     Potsdam Institute for Climate Impact Reserach (PIK)       *!
!*          Authors and contributors see AUTHOR file             *!
!*  This file is part of 4C and is licensed under BSD-2-Clause   *!
!*                   See LICENSE file or under:                  *!
!*     http://www.https://opensource.org/licenses/BSD-2-Clause   *!
!*                           Contact:                            *!
!*       https://gitlab.pik-potsdam.de/foresee/4C             *!
!*                                                               *!
!*************************************************************** *! 
module data_tsort

! species specific parameter for  sorting of harvested timber
! fagus, picea, pinus, quercus, betula
real, dimension(11)     :: stoh=(/10.,10.,10.,10.,10.,10.,10.,10.,10.,10.,10./)
real, dimension(5)     :: lmin=(/400.,400.,400.,400.,400./)
real, dimension(5)     :: ldmin=(/30.,30., 30., 30.,35./)
real, dimension(5)     :: lzmin=(/20.,14.,14.,20.,20./)
real, dimension(5)     :: lasfixl1=(/400.,400.,400.,400.,400./)
real, dimension(5)     :: lasfixl2= (/300.,300.,300.,300.,300./)
real, dimension(5)     :: lasdmin= (/20.,15.,15.,20.,20./)
real, dimension(5)     :: las1zmin= (/0.,0.,11.,0.,0./)
real, dimension(5)     :: las1dmin= (/0.,0.,11.,0.,0./)
real, dimension(5)     :: laszmin= (/11.,11.,11.,11.,11./)
real, dimension(5)     :: isfixl1= (/200.,200.,200.,200.,200./)
real, dimension(5)     :: isfixl2= (/100.,100.,100.,100.,100./)
real, dimension(5)     :: isdmin= (/10.,10.,10.,10.,10./)
real, dimension(5)     :: iszmin= (/7.,7.,7.,7.,7./)
real   rabth(5,2)
real,dimension(5,3)    :: rabz
real                   :: zug =10                        ! addition  [cm]
real, allocatable,save, dimension(:,:,:,:) :: sort       ! per year and species for different cohorts:
integer, parameter  :: dg=kind(0.0D0)                    ! identifier, lenght, diamter, volume, number of pieces
integer                :: anz_list
integer                :: flag_sort= 1      !  0: with stem timber; 1: without stem timber, 2:only LAS 3m + Ind +Fuel
                                            !  3: only LAS 4m * Ind + Fuel
integer                :: flag_deadsort =0

type timber
  integer     :: year
  integer     :: count
  character(4):: ttype
  character(2):: stype     !  stand type (vb or ab)
  integer     :: specnr
  real        :: zapfd     ! diameter at the top
  real        :: zapfdor    ! without bark
  real        :: length
  real        :: dia       ! diameter at thre middle
  real        :: diaor     ! without bark
  real(kind =dg)       :: vol
  real        :: tnum
  real        ::hei_tree
  real        :: hbo_tree
  real        :: diab      ! diameter at base
  real        :: dcrb
end type timber

type  tim_obj
  type(timber)               ::   tim      ! cohort data structure
   type(tim_obj), pointer :: next     ! pointer to next cohort
end type tim_obj

type tim_list
   type(tim_obj), pointer :: first    ! List of cohorts
end type tim_list
type(tim_list)            :: st       ! variable for whole stand, all cohorts

type(tim_obj), pointer    :: ztim     ! pointer variable for manipulating cohorts

DATA rabth  /35.,25.,20.,40.,40.,0.,40.,30.,60.,0./
DATA rabz /1.,1.,1.,3.,2.,2.,2.,2.,5.,4.,2.,3.,4.,6.,4./
end module data_tsort
