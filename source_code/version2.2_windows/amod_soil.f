!*****************************************************************!
!*                                                               *!
!*              4C (FORESEE) Simulation Model                    *!
!*                                                               *!
!*                                                               *!
!*              data modules of soil submodels                   *!
!*                                                               *!
!*    containes:                                                 *!
!*    DATA_SOIL                                                  *!
!*    DATA_SOIL_CN                                               *!
!*    HELP_SOIL_CN                                               *!
!*    DATA_SOIL_T                                                *!
!*    DATA_SOIL_PARAM                                            *!
!*                                                               *!
!*                  Copyright (C) 1996-2018                      *!
!*     Potsdam Institute for Climate Impact Reserach (PIK)       *!
!*          Authors and contributors see AUTHOR file             *!
!*  This file is part of 4C and is licensed under BSD-2-Clause   *!
!*                   See LICENSE file or under:                  *!
!*     http://www.https://opensource.org/licenses/BSD-2-Clause   *!
!*                           Contact:                            *!
!*       https://gitlab.pik-potsdam.de/foresee/4C                *!
!*                                                               *!
!*****************************************************************!

module data_soil

! Variables and parameters of soil model

 	integer 	:: soil_id   = -1 	! soil type	identification
	integer		:: nlay      = -1  	! number of soil layers
	integer     :: nroot_max = 1    ! number of rooting layers       
 	integer 	:: s_typen   = -1 	! soil type	number: 1 - sand, 2 - loam,
                                    ! 3 - silt, 4 - clay
    integer     :: nlgrw            ! number of layer with ground water
    real        :: grwlev           ! groundwater level
    real        :: rmass1           ! rest of dry mass , 1. layer

	! arrays with dimension nlay
	real, allocatable, save, dimension(:) ::     &   
		! Description of soil layers
				thick,    & 	! thickness of the layer   cm 
				mid,      &  	! middle of the layer	   cm
				depth,    & 	! depth of the layer	   cm
		! soil parameter
				pv,        &    ! pore volume	   mm    
				pv_v,      &    ! pore volume	   vol%    
				dens,	   &	! soil density     g/cm3
				field_cap ,&	! field capacity   mm
				wilt_p    ,&	! wilting point	   mm
				f_cap_v   ,&	! field capacity   vol-%
				wilt_p_v  ,&	! wilting point	   vol%
				spheat,    &	! specific heat capacity   J/(g K)
				phv,       &    ! pH-value
                quarzv,    &    ! content of quarz (Vol%)
                sandv,     &    ! content of sand (Vol%, input: Mass%)
                clayv,     &    ! content of clay (Vol%, input: Mass%)
                siltv,     &    ! content of silt (Vol%, input: Mass%)
                humusv,    &    ! content of humus (Vol%, input: Mass%)
                skelv,     &    ! content of skeleton Vol%
                skelfact,  &    ! skeleton factor for water calculation
                vol,       &    ! volume of layer (cm3)
                dmass,     &    ! dry mass of layer (g/m2)
		! model parameter
				wlam,      &	! Lambda parameter for percolation
		! soil state variables
				wats,	  &	! water content	  mm
				wats_1,	  &	! water content of previous day   mm
				watvol,   & ! water content in vol%
				wat_res,  & ! water uptake resistance
                perc,	  &	! percolation water	  mm
				wupt_r,   &	! water uptake by roots	  mm
				wupt_ev,  &	! water taking by evaporation   mm
				temps,	  &	! soil temperature   �C
		! soil help variables
				fcaph,	  &	! field capacity without humus  vol%
				wiltph,	  &	! wilting point without humus  vol%
				pvh,	  &	! pore volume without humus  vol%

		! soil stress variables		
				BDopt, &        ! optimum bulk density for root growth
                fr_loss, &		! yearly fine root loss [%]
				redis			! yearly part of redistribution [%]
				
	integer, allocatable, save, dimension(:) ::     &   
				s_drought   ! number of drought days per layer

	! other scalar state variables and parameter 
	integer		:: snow_day = 0 	 ! days with continious snow cover   day
	real		:: snow     = 0.	 ! water equivalent of snow  mm
	real		:: snow_m   = 0.	 ! water from melting of snow   mm
	real		:: cover    = -99.   ! percent of covering
    real        :: grwsup            ! groundwater supply per day
    real        :: bucks_root        ! bucket size (mm) of rooting zone
    real        :: bucks_100         ! bucket size (mm) of 1 m depth
    real        :: thick_1           ! thickness of first layer (old value) 
	! disturbance variable if xylem disturbance influence water uptake
    real        :: xylem_dis         ! percentage of root water uptake reduction by xylem disturbance (flag_dis=1) 
	
	! yearly cumulative quantities
	real 		:: perc_cum = 0.	! cumulative percolation water from last layer
	real 		:: perc_sum = 0.	! sum of percolation water from last layer for weeks or months  
	real		:: wupt_r_c = 0.	! cumulative water uptake by roots
	real		:: wupt_e_c = 0.	! cumulative soil evaporation
	real		:: wupt_cum = 0.	! cumulative whole water uptake		
	real		:: wat_tot  = 0.	! total water content of the soil profile		
    real        :: grwsup_cum=0.    ! groundwater supply per year
    real, dimension(12) :: perc_mon  ! monthly percolation water from last layer
    real, dimension(53) :: perc_week ! wekkly percolation water from last layer

	! mean quantities (per year)
	real 		:: perc_m = 0.	    ! mean yearly percolation water from last layer
	real		:: wupt_r_m = 0.	! mean yearly  water uptake by roots

	! parameter
	real		:: fakt   = 0.4		! percolation factor
	real		:: w_ev_d = 7.		! depth of water taking out by evaporation (cm)
	integer		:: n_ev_d = 1		! corresponding number of layer for w_ev_d
     
	real, allocatable, save, dimension(:,:) ::  xwatupt   ! temp. aux. field of water uptake per cohort and layer  

! arrays of given root distribution (defined input)
	real, allocatable, save, dimension(:)   :: root_fr    ! root fraction per soil layer 

! yearly fine root loss after Rasse et al. 2001
	integer		:: rdepth_kind				! kind of calculation of root depth
	real, allocatable, dimension(:) :: wat_left		! auxiliary variable for coh%watleft to determin annual sum of available water in soil layer boardering on root zone 
	real, allocatable, dimension(:) :: wat_root		! auxiliary variable for coh%watleft to determin annual sum of availabel water in soil layer boardering on root zone
	integer, allocatable, dimension(:) :: root_lay		! auxiliary variable for coh%nroot to determin root zone layer 
    real, allocatable, dimension(:) :: gr_depth		! auxiliary variable for coh%x_rdpt to determin annual sum of root growth 

end module data_soil

!------------------------------------------------------------------------

module data_soil_cn	

! Variables and parameters of soil_cn-model 	
integer   :: nspeclit = 5 ! number of species-litter for decomposition and min.
integer   :: kmint = 1 ! kind of reduction function of min. for temp.
integer   :: knitt = 1 ! kind of reduction function of nit. for temp.
integer   :: kminw = 1 ! kind of reduction function of min. for water
integer   :: knitw = 1 ! kind of reduction function of nit. for water

	! arrays with dimension nlay
	real, allocatable, save, dimension(:) ::     &   
		!  C and N pools per layer
				C_opm,  &   ! whole C-content of dead biomass per layer without stems / g/m2
				C_hum,  &   ! C-content of humus per layer / g/m2
				N_opm,  &   ! whole N-content of dead biomass per layer without stems / g/m2
				N_hum,  &   ! N-content of humus per layer / g/m2
				C_opmfrt, & ! C-content of dead fine roots per layer / g/m2
				N_opmfrt, & ! N-content of dead fine roots per layer / g/m2
				C_opmcrt, & ! C-content of dead coarse roots per layer / g/m2
				N_opmcrt, & ! N-content of dead coarse roots per layer / g/m2
				C_bc,     & ! C-content of biochar per layer / g/m2
				N_bc,     & ! N-content of biochar per layer / g/m2
				NH4,    & 	! NH4-content of the soil layer / g/m2 
 				NO3,    & 	! NO3-content of the soil layer / g/m2 
 				Nupt,   &   ! N uptake from the soil layer / g/m2 
 				Nmin,   &   ! N mineralisation per day and soil layer / g/m2 
		 ! model parameter
		        rmin_phv, &	! reduction of mineralization depending on pH-value
		        rnit_phv, &	! reduction of nitrification depending on pH-value
                cnv_opm,  & ! C/N-ratio of dead biomass
                cnv_hum,  & ! C/N-ratio of humus
                cnv_bc,   & ! C/N-ratio of biochar
                cpart_bc, & ! part of C in biochar
                dens_bc     ! density of biochar

	real, allocatable, save, dimension(:) ::     &   
				C_bc_appl,& ! C-content of biochar application per layer / g/m2
				N_bc_appl   ! C/N-ratio of biochar application per layer / g/m2

	integer, allocatable, save, dimension(:) ::     &   
                y_bc,     & ! year of application of biochar
				bc_appl_lay ! layer of biochar application
	real        :: Nleach   ! N leaching from last layer per day / g/m2 
	real        :: Nupt_d   ! total daily N uptake / g/m2 
	real        :: NH4_in, NO3_in  ! input of NH4 and NO3 into the actual layer as
	                               ! deposition or transport / g/m2 
	real        :: respsoil ! daily heterotrophic respiration / gC/m2
	
	! Model  parameter
	real        :: k_nit  =0.0025    ! nitrification constant / per day
	real        :: pNH4f  =0.1       ! part of free available NH4-N
	real        :: pNO3f  =1.0       ! part of free available NO3-N
	real        :: k_hum_r=0.0003  ! mineralization constant of humus in mineral soil / per day
	real        :: k_hum  =0.0002    ! mineralization constant of humus in humus layer / per day
	real        :: k_bc     =0.00001  ! mineralization constant of biochar / per day
	real        :: k_syn_bc =0.003     ! synthesis coefficient of biochar / per day
	integer     :: y_bc_n            ! actual array number of list of biochar application
	integer     :: n_appl_bc         ! number of biochar applications

  type species_litter
        character (len=20) :: species_name

     ! soil C- and N-pools of primary organic matter per species and fraction
	real    :: C_opm_fol      ! C-content of foliage litter pool / g/m2
 	real    :: N_opm_fol      ! N-content of foliage litter pool / g/m2
	real    :: C_opm_tb       ! C-content of twigs and branches litter pool / g/m2
 	real    :: N_opm_tb       ! N-content of twigs and branches litter pool / g/m2
	real    :: C_opm_stem     ! C-content of stemwood litter pool / g/m2
 	real    :: N_opm_stem     ! N-content of stemwood litter pool / g/m2
	real,dimension(50):: C_opm_frt  ! C-content of fine root litter pool / g/m2
 	real,dimension(50):: N_opm_frt  ! N-content of fine root litter pool / g/m2
	real,dimension(50):: C_opm_crt  ! C-content of coarse root litter pool / g/m2
 	real,dimension(50):: N_opm_crt  ! N-content of coarse root litter pool / g/m2

    ! C/N-ratios of organic primary matter fractions
	real    :: cnv_opm_fol    ! C/N-ratio of foliage litter pool
	real    :: cnv_opm_tb     ! C/N-ratio of twigs, branches litter pool
	real    :: cnv_opm_stem   ! C/N-ratio of stemwood litter pool
	real    :: cnv_opm_frt    ! C/N-ratio of fine root litter pool
	real    :: cnv_opm_crt    ! C/N-ratio of coarse root litter pool
	
  end type species_litter

  type (species_litter),allocatable,dimension(:),target :: slit, slit_1

	! yearly and cumulative quantities
        real    :: N_min   = 0.	   ! cumulative netto mineralisation per year
        real    :: N_min_m = 0.	   ! mean cumulative netto mineralisation of all years 
        real    :: N_tot   = 0.    ! total N content of the soil profil at the end of the year
        real    :: C_tot   = 0.    ! total C content of the soil profil at the end of the year
        real    :: N_lit   = 0.    ! N content of total litter per year
        real    :: C_lit   = 0.	   ! C content of total litter per year
        real    :: N_lit_m = 0.    ! mean cumulative N content of total litter of all years
        real    :: C_lit_m = 0.	   ! mean cumulative C content of total litter of all years
        real    :: N_lit_fol  = 0. ! N content of foliage litter per year
        real    :: C_lit_fol  = 0. ! C content of foliage litter per year
        real    :: N_lit_frt  = 0. ! N content of fine root litter per year
        real    :: C_lit_frt  = 0. ! C content of fine root litter per year
        real    :: N_lit_crt  = 0. ! N content of coarse root litter per year
        real    :: C_lit_crt  = 0. ! C content of coarse root litter per year
        real    :: N_lit_tb   = 0. ! N content of litter from twigs and branches per year
        real    :: C_lit_tb   = 0. ! C content of litter from twigs and branches per year
        real    :: N_lit_stem = 0. ! N content of new dead stems per year
        real    :: C_lit_stem = 0. ! C content of new dead stems per year
        real    :: N_hum_tot  = 0. ! N content of total humus
        real    :: C_hum_tot  = 0. ! C content of total humus
        real    :: N_an_tot   = 0. ! total anorganic N
        real    :: Nupt_c     = 0. ! total N uptake per year / g N/m2 
        real    :: Nupt_m     = 0. ! mean total N uptake per year
        real    :: Nleach_c   = 0. ! cumul. N leaching from last layer per year
        real    :: Nleach_m   = 0. ! mean cumulative N leaching from last layer of all years
        real    :: resps_c    = 0. ! yearly soil respiration / gC/m2
        real    :: resps_c_m  = 0. ! mean yearly soil respiration / gC/m2
        real    :: C_opm_fol       ! C-content of total foliage litter pool / g/m2
        real    :: N_opm_fol       ! N-content of total foliage litter pool / g/m2
        real    :: C_opm_stem      ! C-content of total stemwood litter pool / g/m2
        real    :: N_opm_stem      ! N-content of total stemwood litter pool / g/m2
        real    :: C_opm_tb        ! C-content of total twigs, branches root litter pool / g/m2
        real    :: N_opm_tb        ! N-content ofv twigs, branches litter pool / g/m2
        real    :: C_opm_frt       ! C-content of total fine root litter pool / g/m2
        real    :: N_opm_frt       ! N-content of total fine root litter pool / g/m2
        real    :: C_opm_crt       ! C-content of total coarse root litter pool / g/m2
        real    :: N_opm_crt       ! N-content of total coarse root litter pool / g/m2
        real    :: C_accu   = 0.   ! C accumulation (new C_tot - old C_tot) / t C/ha
                                   ! (mean of all years at the end of simulation) 
        real    :: C_hum_1         ! C content in humus of the litter layer / t C/ha
        real    :: C_tot_1         ! total C content of the litter layer / t C/ha
        real    :: C_hum_40        ! C content in humus of the soil profil up to 40cm depth / t C/ha
        real    :: C_tot_40        ! total C content of the soil profil up to 40cm depth / t C/ha
        real    :: C_hum_80        ! C content in humus of the soil profil up to 80cm depth / t C/ha
        real    :: C_tot_80        ! total C content of the soil profil up to 80cm depth / t C/ha
        real    :: C_hum_100       ! C content in humus of the soil profil up to 100cm depth / t C/ha
        real    :: C_tot_100       ! total C content of the soil profil up to 100cm depth / t C/ha
        real    :: C_bc_tot        ! total C content of biochar / g C/m2
        real    :: N_bc_tot        ! total N content of biochar / g N/m2

    real, dimension(12) :: resps_mon  ! mean monthly soil respiration / gC/m2
    real, dimension(53) :: resps_week ! mean weekly soil respiration / gC/m2
	real, allocatable, save, dimension(:,:) ::  xNupt   ! temp. aux. field of N uptake per cohort and layer  

    integer unit_litter

end module data_soil_cn	

!------------------------------------------------------------------------

module help_soil_cn	

! internal variables for decomposition calculation

real khr, knr, ks, kbc      ! reduced humif., nitr. and syth. coeff.
real remin                  ! reduction function of mineralisation
real reptermc, reptermn     ! reprod. terms of C-/ N-pools
real term1, term2, term3, term4	 ! parts of equ. III
real hexph, hexpn           ! exponential parts
real cnvh                   ! reciprocal C/N-ratio of humus

end module help_soil_cn	

!------------------------------------------------------------------------

module data_soil_t	

! Variables and parameters for soil temperature calculation

integer :: flag_surf = 0   ! calculation of soil surface temperature
                        ! 0 - surface temperature equals temperature of first layer
                        ! 1 - with explicit surface temperature

real  temps_surf        ! soil surface temperature  
real  hflux_surf        ! soil heat flux at soil surface  

! model parameters
real     :: C0 = 0.76,  &     ! coefficients for calculation of surface temperature
            C1 = 0.05,  &
            C2 = 0.3

! arrays with dimension nlay2
real, allocatable, save, dimension(:) ::     &   
            t_cond,   &  ! thermal conductivity J/(cm s K)
            t_cb  ,   &  ! weighted mean of thermal conductivity (term of values b)
            h_cap,    &  ! heat capacity J/(cm3 K) 
            t_diff       ! thermal diffusivity cm2/s

! internal variables for calculation of thermal conductivity
type therm_par    ! parameter of soil fractions (particles) 
     real:: vf       ! volume fraction
     real:: hc       ! heat capacity J/(cm3 K)
     real:: tc       ! thermal conductivity J/(cm s K)
     real:: kwa      ! weighting factor k for continous medium air
     real:: kww      ! weighting factor k for continous medium water
     real:: ga       ! shape factor of particles
end type therm_par

type (therm_par):: water 
type (therm_par):: quarz 
type (therm_par):: clay 
type (therm_par):: silt 
type (therm_par):: humus 
type (therm_par):: air 
type (therm_par):: ice 
type (therm_par):: stone 

! internal variables for the numerical solution
integer  :: nlay1, nlay2        ! number of 2 additional layers

! diagonals of the matrix
! arrays with dimension nlay2
real, allocatable, save, dimension(:) ::     &
                        sb,  &  ! term of values b (reciprocal mean of thickness) 
                        sv,  &  ! thickness times time step
                        sh,  &  ! thickness  
                        sbt, &  ! aux. array of soil temperature	
                        sxx, &  ! right side and result (soil temperature)
                        svv, &  ! thickness times heat capacity
                        svva,&  ! svv from previous time step
                        soh     ! Hauptdiagonale
! array with dimension nlay2+1
real, allocatable, save, dimension(:) :: son  ! Nebendiagonale
integer mfirst   ! first elemet number of matrix 
logical lfirst    ! .true for the first time

! variables for Fourier analysis
integer              :: NK        ! Anzahl der Fourier-Koeffizienten
real, dimension(200) :: FTA, FTO  ! Fourier-Koeffizienten
real, dimension(366) :: Four_sp   ! Stuetzstellen
real                 :: TQ        ! mittlere Temp.
integer              :: it = 1    ! Starttag fuer Temp.-Profil 

end module data_soil_t	

!------------------------------------------------------------------------

module data_soil_param	

! soil type parameters

    real, dimension(13):: grwdist   ! distance groundwater level to root depth

    type soiltype
        character(10)      :: stype  ! soil type
        real               :: lambda ! percolation coefficient lambda 
        real, dimension(13):: rate   ! supply of groundwater to root
    end type soiltype

    type(soiltype), dimension(40):: soil  ! parameter setting in subroutine soil_ini_param

DATA grwdist / 20, 30, 40, 50, 60, 70, 80, 90, 100, 120, 140, 170, 200/
DATA soil%stype / 'Ss','gS','mS','fS','Su2','St2','Sl2','Su3','St3','Sl3','Su4','Slu','Sl4','Ls2', &
                  'Ls4','Lt2','Ts3','Ts4','Lts','Lt3','Tu3','Tu4','Tt','Tu2','Ts2','Tl','Lu', &
                  'Ut4','Us','Uls','Ut2','Ul2','Ut3','Ul3','Uu','Hum','Hh','Hu','Hn','' /
DATA soil%lambda / 1.50, 1.50, 1.50, 1.15, 0.90, 0.67, 0.60, 0.50, 0.30, 0.38, 0.37, 0.27, 0.30, &
                   0.30, 0.24, 0.23, 0.23, 0.22, 0.22, 0.22, 0.24, 0.26, 0.30, 0.15, 0.15, 0.15, &
                   0.15, 0.27, 0.25, 0.29, 0.29, 0.27, 0.27, 0.25, 0.25, 0.27, -99., -99., -99., -99. /

DATA soil(1)%rate / 5.2, 5.0, 1.5, 0.5, 0.2, 0.1, 0, 0, 0, 0, 0, 0, 0.0 /
DATA soil(2)%rate / 5.2, 5.0, 1.5, 0.5, 0.2, 0.1, 0, 0, 0, 0, 0, 0, 0.0 /
DATA soil(3)%rate / 5.8, 5.5, 5.3, 3, 1.2, 0.5, 0.2, 0.1, 0, 0, 0, 0, 0 / 
DATA soil(4)%rate / 5.8, 5.5, 5.3, 5.1, 3, 1.5, 0.7, 0.3, 0.15, 0.1, 0, 0, 0 / 
DATA soil(5)%rate / 5.8, 5.5, 5.3, 5.1, 4.5, 2.5, 1.5, 0.7, 0.4, 0.1, 0.08, 0, 0 / 
DATA soil(6)%rate / 5.8, 5.5, 5.3, 5.1, 4.5, 2.5, 1.5, 0.7, 0.4, 0.1, 0.08, 0, 0 / 
DATA soil(7)%rate / 5.8, 5.5, 5.3, 5.1, 4.5, 2.5, 1.5, 0.7, 0.4, 0.1, 0.08, 0, 0 / 

! 6      > 5.0;> 5.0;> 5.0;> 5.0;4.5;2.5;1.5;0.7;0.4;0.1;< 0.1;0;0;;;;;;;;;;;;;;;;;;;;;;;;;;;;
! 7      > 5.0;> 5.0;> 5.0;> 5.0;4.5;2.5;1.5;0.7;0.4;0.1;< 0.1;0;0;;;;;;;;;;;;;;;;;;;;;;;;;;;;
! 8      > 5.0;> 5.0;> 5.0;> 5.0;5;3.5;2;1.5;0.8;0.3;0.1;< 0.1;0;;;;;;;;;;;;;;;;;;;;;;;;;;;;
! 9      > 5.0;> 5.0;> 5.0;> 5.0;3;2;1;0.7;0.4;0.15;< 0.1;0;0
! 10      > 5.0;> 5.0;> 5.0;> 5.0;5;3.5;2;1.5;0.8;0.3;0.1;< 0.1;0
! 11      > 5.0;> 5.0;> 5.0;> 5.0;> 5.0;> 5.0;5;3;2;1;0.5;0.15;0
! 12      > 5.0;> 5.0;> 5.0;> 5.0;> 5.0;> 5.0;5;3;2;1;0.5;0.15;0
! 13      > 5.0;> 5.0;> 5.0;> 5.0;3;2;1;0.7;0.4;0.15;< 0.1;0;0
! 14 
! 15      > 5.0;> 5.0;> 5.0;3.5;2;1.3;0.8;0.5;0.3;0.15;< 0.1;0;0
! 16      > 5.0;> 5.0;> 5.0;3.5;2;1.3;0.8;0.5;0.3;0.15;< 0.1;0;0
! 17      > 5.0;> 5.0;> 5.0;3.5;2;1.3;0.8;0.5;0.3;0.15;< 0.1;0;0
! 18      > 5.0;> 5.0;4;2;1;0.7;0.5;0.3;0.2;0.1;< 0.1;0;0
! 19
! 20
! 21      > 5.0;> 5.0;2.5;1.2;0.7;0.5;0.3;0.2;0.15;< 0.1;0;0;0
! 22      > 5.0;> 5.0;2.5;1.2;0.7;0.5;0.3;0.2;0.15;< 0.1;0;0;0
! 23      > 5.0;> 5.0;4;2;1;0.7;0.5;0.3;0.2;0.1;< 0.1;0;0
! 24      > 5.0;> 5.0;> 5.0;> 5.0;4.5;3.5;2.5;2;1.5;0.8;0.4;0.2;< 0.1
! 25      4;2;1.1;0.7;0.5;0.4;0.35;0.3;0.22;0.17;0.14;0.1;< 0.1
! 26      4;2;1.1;0.7;0.5;0.4;0.35;0.3;0.22;0.17;0.14;0.1;< 0.1
! 27      
! 28      4;2;1.1;0.7;0.5;0.4;0.35;0.3;0.22;0.17;0.14;0.1;< 0.1
! 29      > 5.0;> 5.0;> 5.0;> 5.0;4.5;3.5;2.5;2;1.5;0.8;0.4;0.2;< 0.1
! 30      > 5.0;> 5.0;> 5.0;> 5.0;4.5;3.5;2.5;2;1.5;0.8;0.4;0.2;< 0.1
! 31      > 5.0;> 5.0;> 5.0;> 5.0;> 5.0;> 5.0;> 5.0;5;3.5;2;1;0.5;0.15
! 32      > 5.0;> 5.0;> 5.0;> 5.0;> 5.0;> 5.0;4.5;3;2.5;1.5;0.7;0.3;0.1
! 33      > 5.0;> 5.0;> 5.0;> 5.0;> 5.0;> 5.0;4.5;3;2.5;1.5;0.7;0.3;0.1
! 34
! 35      > 5.0;> 5.0;> 5.0;> 5.0;> 5.0;> 5.0;4.5;3;2.5;1.5;0.7;0.3;0.1
! 36
! 37      > 5.0;> 5.0;> 5.0;> 5.0;> 5.0;> 5.0;> 5.0;5;3.5;2;1;0.5;0.15

end module data_soil_param	
