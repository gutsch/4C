!*****************************************************************!
!*                                                               *!
!*                         4C (FORESEE)                          *!
!*                                                               *!
!*                                                               *!
!*     growth_seed_week - Growth of seedling cohorts weekly      *!
!*                            Allocation with weekly NPP         *!
!*                                                               *!
!*                  Copyright (C) 1996-2018                      *!
!*     Potsdam Institute for Climate Impact Reserach (PIK)       *!
!*          Authors and contributors see AUTHOR file             *!
!*  This file is part of 4C and is licensed under BSD-2-Clause   *!
!*                   See LICENSE file or under:                  *!
!*     http://www.https://opensource.org/licenses/BSD-2-Clause   *!
!*                           Contact:                            *!
!*       https://gitlab.pik-potsdam.de/foresee/4C                *!
!*                                                               *!
!*****************************************************************!

SUBROUTINE growth_seed_week (jx)
USE data_stand
USE data_species
USE data_simul 

IMPLICIT NONE 
  REAL   :: lambdaf = 0.,  &      ! partitioning coefficients
            lambdas = 0.,  &
            lambdar = 0.,  &
            NPP = 0.,      &      ! NPP available for allocation
            F = 0.,        &      ! state variables: foliage,
            S = 0.,        &      ! shoot biomass,
            R = 0.,        &      ! fine roots,
            H = 0.,        &      ! total tree height
            FNew, SNew,    &      ! new state variables
            RNew,          &
            sigmaf = 0.,   &      ! current leaf activity rate
            ar = 0.
  REAL     :: Gf,            &          ! growth rates 
              Gs,            &
              Gr
  REAL     :: pab,helpdr

  INTEGER  :: jx
  TYPE(coh_obj), POINTER :: p

  p=>pt%first
 DO
  IF(.not.associated(p)) exit
  IF( p%coh%fl_sap.eq.0) then
     ns   = p%coh%species
     F    = p%coh%x_fol
     S    = p%coh%x_sap
     R    = p%coh%x_frt
     NPP  = p%coh%weekNPP              ! [kg]
     H   = p%coh%height 

! only allocate if enough NPP is available and day < a fixed limit 
  IF (NPP>1.0E-9 .and. iday<190) THEN  
     p%coh%NPPpool = p%coh%NPPpool + NPP
! calculate leaf activity based on net PS and leaf mass
  	  sigmaf = NPP/F
! calculate root activity based on drought index
     helpdr= p%coh%drIndPS
! auxiliary variables for fine roots
     ar = 1./helpdr
     if(helpdr.lt.0.001) ar = 1.
! calculate coefficients for roots and foliage and shoot
     pab  = spar(ns)%seeda*spar(ns)%seedb*S**(spar(ns)%seedb-1)
! new model without senescence within the year:
     lambdas=1./(1.+pab+pab*ar)
     lambdaf=(1.-lambdas)/(1.+ar)
     lambdar=1.-lambdas-lambdaf

     IF (lambdas.lt.0.) THEN
        lambdas = 0.
    	  lambdaf = 1./(ar+1.)
    	  lambdar = 1.-lambdaf
     END IF
     IF (lambdar<0) THEN
        lambdar=0.
        lambdas=0.
        lambdaf=1.
     END IF
     IF (lambdaf<0) THEN
        lambdar=0.
        lambdas=0.
        lambdaf=1.
     END IF
  ELSE
     lambdaf   = 0.
     lambdas   = 0.
     lambdar   = 0.
  END IF  

   Gf = lambdaf*NPP
   Gr = lambdar*NPP
   Gs = lambdas*NPP
   p%coh%gfol = Gf
   p%coh%gfrt = Gr
   p%coh%gsap = Gs

   ! update of state vector
   FNew  = F + Gf
   SNew  = S + Gs
   RNew  = R + Gr   
   p%coh%x_fol  = FNew
   p%coh%x_sap  = SNew
   p%coh%x_frt  = RNew  
   p%coh%fol_inc_old = p%coh%fol_inc
   p%coh%fol_inc = Gf
   p%coh%stem_inc = Gs

  ! update height and shoot base diameter (regression functions from Schall 1998) 
  IF(ns.ne.2) p%coh%height = spar(ns)%pheight1* (snew*1000000.) **spar(ns)%pheight2 
  IF(ns.eq.2) p%coh%height = 10**(spar(ns)%pheight1+ spar(ns)%pheight2*LOG10(snew*1000000.)+ &
                              spar(ns)%pheight3*(LOG10(snew*1000000.))**2)
   p%coh%height_ini =   p%coh%height

! update foliage area, parameter med_sla
   SELECT CASE (flag_light) 
      CASE (1:2) 
      p%coh%med_sla = spar(ns)%psla_min + spar(ns)%psla_a*(1.- vstruct(lowest_layer)%irel)
      CASE(3,4)
      p%coh%med_sla = spar(ns)%psla_min + spar(ns)%psla_a*(1.-irelpool(lowest_layer))!
   END SELECT 

! total leaf area of a tree in this cohort [m**2]as as crown area 
    p%coh%ca_ini = p%coh%med_sla * p%coh%x_fol

! weekNPP equal zero for next calculation
   p%coh%weekNPP = 0.
 END IF  
  p=> p%next
  END DO
END SUBROUTINE growth_seed_week