!*****************************************************************!
!*                                                               *!
!*  4C (FORESEE) Simulation Model                                *!
!*                                                               *!
!*                                                               *!
!*     module data_init                                          *!
!*     declaration of variables for additional information       *!
!*     used during initialisation                                *!
!*                                                               *!
!*                  Copyright (C) 1996-2018                      *!
!*     Potsdam Institute for Climate Impact Reserach (PIK)       *!
!*          Authors and contributors see AUTHOR file             *!
!*  This file is part of 4C and is licensed under BSD-2-Clause   *!
!*                   See LICENSE file or under:                  *!
!*     http://www.https://opensource.org/licenses/BSD-2-Clause   *!
!*                           Contact:                            *!
!*       https://gitlab.pik-potsdam.de/foresee/4C                *!
!*                                                               *!
!*****************************************************************!

MODULE data_init

IMPLICIT NONE

   INTEGER         :: spec_nrDSW(120)  ! species ordinal number in DSW according to BRA
   INTEGER         :: spec_nrBAY(120)  ! species ordinal number in Bavaria
   INTEGER         :: spec_4C(120)     ! species code number of 4C assigned to DSW species
   CHARACTER (3)   :: spec_code(120)   ! specifies three letter code in DSW according to BRA
   CHARACTER (50)  :: GER_name(120)    ! german name
   CHARACTER (50)  :: LAT_name(120)    ! scientific, latin name
   CHARACTER (50)  :: ENG_name(120)    ! english name
   INTEGER         :: spnum_for_DSW(800)  ! species ordinal number (1..120) for DSW in element of vector
                                       ! which corresponds to species number according to BRA

   ! variables for treatment of DSW initialisation data
   TYPE group_vec
       INTEGER :: locid         ! ID for stand
       INTEGER :: taxid         ! 4C species number
       INTEGER :: BRAid         ! DSW species code
       INTEGER :: alter
       INTEGER :: baumzahl
       INTEGER :: schicht       ! 10 = upper storey trees/Oberstand, 20 = retention trees/Überhälter, 40 = understorey 50 = selction/plenter forest/ plenterartig 
       REAL :: dm
       REAL :: mhoe
       REAL :: gf
       REAL :: patchsize
       REAL :: standsize
       REAL :: volume
    END TYPE group_vec
    TYPE(group_vec), DIMENSION(:), ALLOCATABLE :: ngroups

    ! variables for plenterwald initialisation
    INTEGER, DIMENSION(4) :: low_age, high_age

    ! Parameter for volume functions provided by Eberswalde
    REAL, DIMENSION (10,3) :: parEBW
    ! Parameter Pine (Kiefer)    EBERSWALDE
    DATA parEBW(10,1:3)/-9.780614,1.96047,0.89443/
    ! Parameter Ponderosa pine taken equal to Pine (Kiefer)    EBERSWALDE
    DATA parEBW(10,1:3)/-9.780614,1.96047,0.89443/

    ! Parameter for volume function adapted from SILVA
    REAL, dimension (11,9) :: par_S
    ! Paramter Fichte/spruce   SILVA
    DATA par_S(2,1:9)/-3.59624,1.80213,-0.288243, 1.06247, -0.128993, 0.0353434, 0.142264, -0.058259, 0.00459854/
    ! Parameter Buche/beech  SILVA
    DATA par_S(1,1:9)/-2.7284,0.837563,-0.105843,1.62283,-0.214812,0.0289272,-0.0879719,0.0325667,-0.00446295/
    ! Parameter Eiche/oak  SILVA
    DATA par_S(4,1:9)/-3.06118,1.45506,-0.19992,1.93898,-0.689727,0.112653,-0.165102,0.120127,-0.0202543/
    ! Parameter Kiefer/ pine SILVA
    DATA par_S(3,1:9)/-5.80915,3.387,-0.494392,3.67116,-1.83211,0.273999,-0.459282,0.29989,-0.0444931/
    ! Parameter Birke/birch SILVA = Weichlaub
    DATA par_S(5,1:9)/-5.98031,2.65905,-0.3374,3.78395,-1.47318,0.188661,-0.540955,0.296957,-0.0385165/
    ! Parameter Pinus contorta (von Kiefer)
    DATA par_S(6,1:9)/ -5.80915,3.387,-0.494392,3.67116,-1.83211,0.273999,-0.459282,0.29989,-0.0444931/
    ! Parameter Pinus ponderosa (von Kiefer)
    DATA par_S(7,1:9)/ -5.80915,3.387,-0.494392,3.67116,-1.83211,0.273999,-0.459282,0.29989,-0.0444931/
    ! parameter Populus tremula
    DATA par_S(8,1:9)/ -5.98031,2.65905,-0.3374,3.78395,-1.47318,0.188661,-0.54095500,0.296957,-0.03851650/
    ! parameter Robinie( black locust)
    DATA par_S(11,1:9)/-2.7284,0.837563,-0.105843,1.62283,-0.214812,0.0289272,-0.0879719,0.0325667,-0.00446295/


END   ! module data_init
