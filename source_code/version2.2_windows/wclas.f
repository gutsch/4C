!*****************************************************************!
!*                                                               *!
!*              4C (FORESEE) Simulation Model                    *!
!*                                                               *!
!*                                                               *!
!*  Subroutine		                                             *!
!*	- wclas: classification of forest type according to the      *!
!*           present species share   (M.Lindner, 8.8.96)         *!
!*  - clas_grob                                                  *!
!*  - indexx                                                     *!
!*                                                               *!
!*                  Copyright (C) 1996-2018                      *!
!*     Potsdam Institute for Climate Impact Reserach (PIK)       *!
!*          Authors and contributors see AUTHOR file             *!
!*  This file is part of 4C and is licensed under BSD-2-Clause   *!
!*                   See LICENSE file or under:                  *!
!*     http://www.https://opensource.org/licenses/BSD-2-Clause   *!
!*                           Contact:                            *!
!*       https://gitlab.pik-potsdam.de/foresee/4C                *!
!*                                                               *!
!*****************************************************************!

    
subroutine wclas(btyp) 

  use data_stand
  use data_species
  use data_clas

!     ------------------------------------------------------------------
!	----VARIABLEN---
      INTEGER btyp, ns, ntr, i
      real sumbio1
      allocate(bpart(nspec_tree))
!     ------------------------------------------------------------------
!     Berechnung der Baumartenanteile (bpart) als Anteil an der
!     Gesamtbiomasse
!     ------------------------------------------------------------------
      if(sumbio.eq.0) then

	     zeig=>pt%first
                     do
                       if(.not.associated(zeig)) exit
    
                       ns  = zeig%coh%species
                   	    if(ns.le.nspec_tree) then
                          ntr = zeig%coh%ntreeA
                          zeig%coh%totBio = zeig%coh%x_fol + (1.+spar(ns)%alphac)*(zeig%coh%x_sap + zeig%coh%x_hrt) + zeig%coh%x_frt
                          sumbio  = sumbio  + ntr * zeig%coh%totBio
						  svar(ns)%sum_bio = svar(ns)%sum_bio + ntr*zeig%coh%totBio
                        end if
                        zeig=>zeig%next


                      end do
		end if

      sumbio1 = sumbio*kpatchsize/10000.


      if (sumbio1.lt.1) sumbio1=sumbio1+0.0001

      do i=1,nspec_tree
	      if( sumbio.ne.0) then
            bpart(i)=svar(i)%sum_bio/sumbio
		  end if
      end do
!-----------------------------------------------------------------
!     Wald vorhanden? Freiflaeche (btyp300) unter 5m3 Biomasse
!-----------------------------------------------------------------
      btyp=0
      if (sumbio1.le.5) then
         btyp=300
         goto 201
      end if
!     ------------------------------------------------------------------
!     Klassifikation in Bestandestypen (Beschreibung in waldtyp.txt).
!     ------------------------------------------------------------------
      nhpar =0
      lhpar=0
      alhpar=0
      alnpar=0
!-----------------------------------------------------------------
!     Baumartengruppen definieren:
!     Eichenanteile (Q. petrea und Q. robur) werden zusammengefasst
!     Aln=Laubholz mit niedriger Lebensdauer (Pioniere:Asp,Birk,Erl)
!     Alh=Laubholz mit hoher Lebensdauer (Edellh:Ah,Es,Hbu,Lind,Ulm)
!     Lh=Laubholz
!     Nh=Nadelholz
!-----------------------------------------------------------------

        alnpar = bpart(5)

        alhpar = bpart(4)

         lhpar = alnpar + alhpar + bpart(1)

         nhpar = bpart(2) + bpart(3) + bpart(10)

!     ------------------------------------------------------------------
!     Unterprogramm zur Klassifikation (!lasgrob) bzw. (!lasfein)
!     ------------------------------------------------------------------
      CALL clasgrob(btyp)

  201 CONTINUE

  deallocate(bpart)

     END subroutine wclas

      subroutine clasgrob(btyp)
!    ------------------------------------------------------------------
!     Unterprogramm zur Klassifizierung von Simulationsergebnissen
!     3. Version; Index nach Baumartenanteilen inclusive Grundwasser-
!     response. 29 Klassen (M.Lindner, 8.8.96)
!     ------------------------------------------------------------------
! 
      use data_clas
      use data_species

!	----VARIABLEN---
      REAL aa
      INTEGER  i, btyp, indx(18), top1,top2, maxspe
      real bparth(18)

!    ----Konstanten----
      REAL T1, T2, T3, T4
      PARAMETER (T4=.9, T3=.5, T2=.3, T1=.2)

!-----------------------------------------------------------------
!     Index nach aufsteigend sortierten Baumartenanteilen erzeugen
!-----------------------------------------------------------------

      do i =1, 18
        bparth(i) = 0.
      end do
      bparth(8) = bpart(1)
      bparth(10) = bpart(2)
      bparth(11) = bpart(3)
      bparth(13) = bpart(4)
      bparth(5) = bpart(5)
! eingef�gt f�r Douglasie
	  bparth(6) = bpart(10)
      maxspe = 18
      call indexx(maxspe,bparth,indx)

      top1=indx(maxspe)
      top2=indx(maxspe-1)

!-------Hauptbaumart > 90%---------------------------------------------
      if (bparth(indx(maxspe)).ge.T4) then
          if (top1.eq.1) then
              btyp=70
          else if (top1.eq.8) then

             btyp=110
          else if (top1.eq.10) then
              btyp=10
          else if (top1.eq.11) then
              btyp=40
          else if (top1.eq.13) then
              btyp=140
          else if (alnpar.ge.T4) then
              btyp=180
          else if (alhpar.ge.T4) then
             if (top1.eq.16) then
                   btyp=190
             else
                   btyp=191
             endif
          else if (nhpar.ge.T4) then
              btyp=90
          endif

!-------Hauptbaumart 50-90%--------------------------------------------
      else if (bparth(indx(maxspe)).ge.T3) then
          if (top1.eq.1) then
              btyp=70
          else if (top1.eq.8) then
              aa=lhpar - bparth(8)
              if (top2.eq.1) then
                  btyp=125
              else if (top2.eq.10) then
                  btyp=125
              else if (top2.eq.11) then
                  btyp=125
              else if (top2.eq.13) then
                  btyp=122
              else if (aa.gt.nhpar) then
                  if (alhpar.gt.alnpar) then
                     btyp=120
                  else
                     btyp=120
                  endif
              else
                  btyp=125
              endif
          else if (top1.eq.10) then
              aa=(nhpar-bparth(10))
              if (top2.eq.1) then
                  btyp=25
              else if (top2.eq.8) then
                  btyp=20
              else if (top2.eq.11) then
                  btyp=25
              else if (top2.eq.13) then
                  btyp=20
              else if (aa.gt.lhpar) then
                  btyp=25
              else
                  btyp=20
              endif
          else if (top1.eq.11) then
              aa=(nhpar-bparth(11))
              if (top2.eq.1) then
                  btyp=55
              else if (top2.eq.8) then
                  btyp=50
              else if (top2.eq.10) then
                  btyp=55
              else if (top2.eq.13) then
                  btyp=52
              else if (aa.gt.lhpar) then
                  btyp=55
              else
                  btyp=50
              endif
          else if (top1.eq.13) then
              aa=(lhpar-bparth(13))
              if (top2.eq.8) then
                  btyp=151
              else if (top2.eq.11) then
                  btyp=157
              else if (aa.gt.nhpar) then
                 if (alhpar.gt.alnpar) then
                    btyp=154
                 else
                    btyp=150
                 endif
              else
                  btyp=155
              endif
          else if (alnpar.gt.T3) then
              btyp=180
          else if (alhpar.gt.T3) then
             if (top1.eq.16) then
                btyp=190
             else
                btyp=191
             endif
          else if (nhpar.ge.T3) then
              btyp=90
          endif

!-------Hauptbaumart 30-50%--------------------------------------------
      else if (bparth(indx(maxspe)).ge.T2) then
          if (top1.eq.1) then
              if (top2.eq.8) then
                  btyp=75
              else if (top2.eq.10) then
                  btyp=75
              else
                  btyp=75
              endif
          else if (top1.eq.8) then
              aa=(lhpar-bparth(8))
              if (top2.eq.1) then
                  btyp=125
              else if (top2.eq.10) then
                  btyp=125
              else if (top2.eq.11) then
                  btyp=125
              else if (top2.eq.13) then
                  btyp=122
              else if (aa.gt.nhpar) then
                  if (alhpar.gt.alnpar) then
                     btyp=120
                  else
                     btyp=120
                  endif
              else
                  btyp=125
              endif
          else if (top1.eq.10) then
              aa=(nhpar-bparth(10))
              if (top2.eq.1) then
                  btyp=25
              else if (top2.eq.8) then
                  btyp=20
              else if (top2.eq.11) then
                  btyp=25
              else if (top2.eq.13) then
                  btyp=20
              else if (aa.gt.lhpar) then
                  btyp=25
              else
                  btyp=20
              endif
          else if (top1.eq.11) then
              aa=(nhpar-bparth(11))
              if (top2.eq.8) then
                  btyp=50
              else if (top2.eq.10) then
                  btyp=55
              else if (top2.eq.13) then
                  btyp=52
              else if (aa.gt.lhpar) then
                  btyp=55
              else
                  btyp=50
              endif
          else if (top1.eq.13) then
              aa=(lhpar-bparth(13))
              if (top2.eq.8) then
                  btyp=151
              else if (top2.eq.11) then
                  btyp=157
              else if (aa.gt.nhpar) then
                 if (alhpar.gt.alnpar) then
                    btyp=154
                 else
                    btyp=150
                 endif
              else
                  btyp=155
              endif
          else if (nhpar.gt.lhpar) then
              btyp=100
          else if (alnpar.gt.alhpar) then
              if (top2.eq.11) then
                  btyp=185
              else if (top2.eq.13) then
                  btyp=185
              else
                  btyp=185
              endif
          else if (alhpar.ge.T2) then
              if (top2.eq.8) then
                  btyp=195
              else if (top2.eq.13) then
                  btyp=154
              else if (top1.eq.16) then
                  btyp=195
              else
                  btyp=191
              endif
          else
              btyp=250
          endif

!-------Hauptbaumart 20-30%--------------------------------------------
      else if (bparth(indx(maxspe)).ge.T1) then
          if (top1.eq.1) then
              if (top2.eq.8) then
                  btyp=75
              else if (top2.eq.10) then
                  btyp=75
              else
                  btyp=75
              endif
          else if (top1.eq.8) then
              aa=(lhpar-bparth(8))
              if (top2.eq.1) then
                  btyp=125
              else if (top2.eq.10) then
                  btyp=125
              else if (top2.eq.11) then
                  btyp=125
              else if (top2.eq.13) then
                  btyp=122
              else if (aa.gt.nhpar) then
                  if (alhpar.gt.alnpar) then
                     btyp=120
                  else
                     btyp=120
                  endif
              else
                  btyp=125
              endif
          else if (top1.eq.10) then
              aa=(nhpar-bparth(10))
              if (top2.eq.1) then
                  btyp=25
              else if (top2.eq.8) then
                  btyp=20
              else if (top2.eq.11) then
                  btyp=25
              else if (top2.eq.13) then
                  btyp=20
              else if (aa.gt.lhpar) then
                  btyp=25
              else
                  btyp=20
              endif
          else if (top1.eq.11) then
              aa=(nhpar-bparth(11))
              if (top2.eq.8) then
                  btyp=50
              else if (top2.eq.10) then
                  btyp=55
              else if (top2.eq.13) then
                  btyp=52
              else if (aa.gt.lhpar) then
                  btyp=55
              else
                  btyp=50
              endif
          else if (top1.eq.13) then
              aa=(lhpar-bparth(13))
              if (top2.eq.8) then
                  btyp=151
              else if (top2.eq.11) then
                  btyp=157
              else if (aa.gt.nhpar) then
                 if (alhpar.gt.alnpar) then
                    btyp=154
                 else
                    btyp=150
                 endif
              else
                  btyp=155
              endif
          else if (alnpar.gt.alhpar) then
                  btyp=185
          else if (alhpar.gt.T2) then
             if (top2.eq.13) then
                btyp=154
             else if (top1.eq.16) then
                  btyp=195
             else
                  btyp=191
             endif
          else if (lhpar.le.T2) then
             btyp=100
          else if (nhpar.le.T2) then
             btyp=200
          else
             btyp=250
          endif

!------------Hauptbaumart unter 20%-------------------------
      else
          if (lhpar.le.T2) then
             btyp=100
          else if (nhpar.le.T2) then
             btyp=200
          else
             btyp=250
          endif
      endif
      END  subroutine clasgrob

      SUBROUTINE indexx(n,arr,indx)
      INTEGER n,indx(n),M,NSTACK
      REAL arr(n)
      PARAMETER (M=7,NSTACK=50)
      INTEGER i,indxt,ir,itemp,j,jstack,k,l,istack(NSTACK)
      REAL a
      do 11 j=1,n
        indx(j)=j
11    continue
      jstack=0
      l=1
      ir=n
1     if(ir-l.lt.M)then
        do 13 j=l+1,ir
          indxt=indx(j)
          a=arr(indxt)
          do 12 i=j-1,1,-1
            if(arr(indx(i)).le.a)goto 2
            indx(i+1)=indx(i)
12        continue
          i=0
2         indx(i+1)=indxt
13      continue
        if(jstack.eq.0)return
        ir=istack(jstack)
        l=istack(jstack-1)
        jstack=jstack-2
      else
        k=(l+ir)/2
        itemp=indx(k)
        indx(k)=indx(l+1)
        indx(l+1)=itemp
        if(arr(indx(l+1)).gt.arr(indx(ir)))then
          itemp=indx(l+1)
          indx(l+1)=indx(ir)
          indx(ir)=itemp
        endif
        if(arr(indx(l)).gt.arr(indx(ir)))then
          itemp=indx(l)
          indx(l)=indx(ir)
          indx(ir)=itemp
        endif
        if(arr(indx(l+1)).gt.arr(indx(l)))then
          itemp=indx(l+1)
          indx(l+1)=indx(l)
          indx(l)=itemp
        endif
        i=l+1
        j=ir
        indxt=indx(l)
        a=arr(indxt)
3       continue
          i=i+1
        if(arr(indx(i)).lt.a)goto 3
4       continue
          j=j-1
        if(arr(indx(j)).gt.a)goto 4
        if(j.lt.i)goto 5
        itemp=indx(i)
        indx(i)=indx(j)
        indx(j)=itemp
        goto 3
5       indx(l)=indx(j)
        indx(j)=indxt
        jstack=jstack+2
        if(jstack.gt.NSTACK)pause 'NSTACK too small in indexx'
        if(ir-i+1.ge.j-l)then
          istack(jstack)=ir
          istack(jstack-1)=i
          ir=j-1
        else
          istack(jstack)=j-1
          istack(jstack-1)=l
          l=i
        endif
      endif
      goto 1
      END
!  (C) Copr. 1986-92 Numerical Recipes Software 0)+0143$!-.
