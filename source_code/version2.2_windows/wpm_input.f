!*****************************************************************!
!*                                                               *!
!*               4C (FORESEE) Simulation Model                   *!
!*                                                               *!
!*                                                               *!  
!*           Post Processing: read the mansort files             *!
!*                                                               *!
!*    Subroutines for:                                           *!
!*    - input_manrec: call from management, fills values into    *!
!*                    manrec structure                           *!
!*    - read_spinup:  reads the spinup file                      *!
!*    - read_input:   read the mansort and manrec files		     *!
!*                                                               *!
!*                  Copyright (C) 1996-2018                      *!
!*     Potsdam Institute for Climate Impact Reserach (PIK)       *!
!*          Authors and contributors see AUTHOR file             *!
!*  This file is part of 4C and is licensed under BSD-2-Clause   *!
!*                   See LICENSE file or under:                  *!
!*     http://www.https://opensource.org/licenses/BSD-2-Clause   *!
!*                           Contact:                            *!
!*       https://gitlab.pik-potsdam.de/foresee/4C                *!
!*                                                               *!
!*****************************************************************!

subroutine input_manrec

use data_simul
use data_manag
use data_wpm

implicit none


type(manrec_type) :: manrec_ini




if ( flag_wpm > 0 ) then

	manrec_ini%year			= time
	manrec_ini%management	= maninf
	manrec_ini%measure		= meas

	if (.not. associated(first_manrec)) then
	  allocate (first_manrec)
	  first_manrec%manrec = manrec_ini
	  nullify(first_manrec%next)
	  nr_management_years = 1
    else
	  ! build new manrec object	
	  allocate(act_manrec)
	  act_manrec%manrec = manrec_ini
	  ! chain into the list
	  act_manrec%next => first_manrec
	  ! set the first pointer to the new object
	  first_manrec => act_manrec
	  nr_management_years = nr_management_years + 1
 	end if

end if

end subroutine input_manrec





subroutine read_mansort(mansortFile, manrecFile)


use data_wpm
use data_tsort

implicit none

character(70) mansortFile, manrecFile
integer i, k
integer ios, un
real buffer
type(mansort_type) mansort_ini
type(manrec_type)  manrec_ini	
	
	! set the external unit
	un = 100
	ios = 0

	! read manrec file
	! leave header
    open (un, file = manrecFile, iostat = ios, status = 'OLD', action = 'read')
    do i=1,3
      read (un, *)
    enddo

	! read lines
    do
		read (un,'(I16,A28,I8)',iostat=ios)		&
			manrec_ini%year,	manrec_ini%management,	&
			manrec_ini%measure

		! set the manrec list pointer		
		if (ios == 0) then
			if (.not. associated(first_manrec)) then
			  allocate (first_manrec)
			  first_manrec%manrec = manrec_ini
			  nullify(first_manrec%next)
			  nr_management_years = 1
			else
			  ! build the manrec object	
			  allocate(act_manrec)
			  act_manrec%manrec = manrec_ini
			  ! chain into the list
			  act_manrec%next => first_manrec
			  ! set the actual pointer to the new object
			  first_manrec => act_manrec
			
			  nr_management_years = nr_management_years + 1
			end if
		end if

		if (ios > 0) then
			stop
		else if (ios < 0) then
			exit
		endif
	    k=k+1
    end do

	close(un, status="keep")

	! read mansort file
	! leave header
    open (un, file = mansortFile, iostat = ios, status = 'OLD', action = 'read')
    do i=1,3
		read (un,*)
    end do
	
	! read lines
    do
		read (un,'(I9,I8,I3, A3,  F10.3, 4(F8.3), F10.4, F15.3,   I9)',iostat=ios)&
			mansort_ini%year,	mansort_ini%count,	mansort_ini%spec,		&
			mansort_ini%typus,												&
			buffer,															&
			mansort_ini%diam, mansort_ini%diam_wob, buffer, buffer,			&
			mansort_ini%volume,												&		
			mansort_ini%dw,													&
			mansort_ini%number
			
		! set the mansort list pointer		
		if (ios == 0) then
			if (.not. associated(first_mansort)) then
			  allocate (first_mansort)
			  first_mansort%mansort = mansort_ini
			  nullify(first_mansort%next)
			  anz_list = 1
			 else
			  ! build new mansort object	
			  allocate(act_mansort)
			  act_mansort%mansort = mansort_ini
			  ! chain into the list
			  act_mansort%next => first_mansort
			  ! set the first pointer to the new object
			  first_mansort => act_mansort
			  anz_list = anz_list +1
			end if
		end if

		if (ios.gt.0) then
			stop
		elseif (ios.lt.0) then
			exit
		endif
		k=k+1
	enddo

	close(un, status="keep")


    end subroutine read_mansort

!*****************************************************************************
    
subroutine read_spinup(spinupFile)

use data_wpm

character(70) spinupFile
integer i, j, unit, ios
integer max
real, dimension(nr_use_cat + 1) :: spinny
real dummy

	unit = 20

	! Headers to output files
	open(unit,	FILE=spinupFile, STATUS='OLD', action='read')
	do i=1,3
      read (unit, *)
    enddo

	! how many years?
	max = max_age(1)
	do j = 1,nr_use_cat
		if ( max < max_age(j) ) max = max_age(j)
	end do

	do i=1,max
		read(unit,	'(I9,8(F15.3) )',iostat=ios)		&
				dummy,									&
				spinny(1),	&
				spinny(2),	&
				spinny(3),	&
				spinny(4),	&
				spinny(5),	&
				spinny(6),	&
				spinny(7),	&
				spinny(8)

		if ( ios == 0 ) then
			do j = 1,nr_use_cat
				if ( i <= max_age(j) ) then
					use_categories(j)%spinup(i) = spinny(j)
				end if
			end do
			if (i == 1) then
				 landfill_spinup = spinny(8)
			end if	
		end if
	end do

	close(unit, status="keep")

end subroutine read_spinup
