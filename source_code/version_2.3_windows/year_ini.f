!*****************************************************************!
!*                                                               *!
!*              4C (FORESEE) Simulation Model                    *!
!*                                                               *!
!*                                                               *!
!*                    Subroutines for:                           *!
!*              Initialization and calculation                   *!
!*                       per year                                *!
!*                                                               *!
!* - YEAR_INI:       Initialization (yearly)                     *!
!* - REDN_INI:       Calculation of RedN                         *!
!* - REDN_CALC:      Calculation of RedN                         *!
!* - SAVE_COHORT:    Save intialisation of cohorts (optional)    *!
!* - RESTORE_COHORT: Restore intialisation of cohorts (optional) *!
!* - S_YEAR:         Calculation of yearly values                *!
!* - FIRE_YEAR:      Calculation of yearly fire indices          *!
!* - T_INDICES:      Calculation of the nun temperature index    *!
!*                                                               *!
!*                  Copyright (C) 1996-2018                      *!
!*     Potsdam Institute for Climate Impact Reserach (PIK)       *!
!*          Authors and contributors see AUTHOR file             *!
!*  This file is part of 4C and is licensed under BSD-2-Clause   *!
!*                   See LICENSE file or under:                  *!
!*     http://www.https://opensource.org/licenses/BSD-2-Clause   *!
!*                           Contact:                            *!
!*       https://gitlab.pik-potsdam.de/foresee/4C                *!
!*                                                               *!
!*****************************************************************!

SUBROUTINE year_ini

!initialization of several variables for yearly calculation and output

use data_biodiv
use data_climate
use data_depo
use data_evapo
use data_inter
use data_par
use data_out
use data_simul
use data_soil
use data_soil_cn
use data_species
use data_stand
use data_manag

implicit none

integer i, j, k, helpnl
real    help, hCbc
real    thickh, thicki, thicki1, &
        pvi , &
        fcapi , &
        wilti , &
        sandi , &
        clayi , &
        silti , &
        humi  , &
        nfki  , &
        densi , &
        skeli , &
        pHi , wati, watsi, wlami, &
        Copmi , Chumi, &
        Nopmi, Nhumi, NH4i, NO3i, &
         voli, vol_bc
real, dimension (nlay) :: xfcap, xwiltp, xpv

DOUBLE PRECISION   :: co2_annual

time_cur = time_cur+1
call pheno_ini

flag_vegper = 0
flag_tveg = 0
iday_vegper = 0

med_air     = 0.
sum_prec    = 0.
med_rad     = 0.
sum_prec_ms = 0.
sum_prec_mj = 0.
med_air_ms  = 0.
med_air_mj  = 0.
gdday       = 0.
days_summer = 0
days_hot    = 0
days_ice    = 0
days_dry    = 0
days_hrain  = 0
days_rain   = 0
days_rain_mj= 0
days_snow   = 0
days_wof    = 0	
int_cum_can = 0.
int_cum_sveg= 0.
perc_cum    = 0.
aet_cum     = 0.
aet_mon     = 0.
aet_week    = 0.
pet_cum     = 0.
pet_mon     = 0.
pet_week    = 0.
Rnet_cum    = 0.
perc_mon    = 0.
perc_week   = 0.
dew_cum     = 0.
tra_tr_cum  = 0.
tra_sv_cum  = 0.
wupt_r_c    = 0.
wupt_e_c    = 0.
wupt_cum    = 0.
s_drought   = 0
N_min       = 0.
Nleach_c    = 0.
Nupt        = 0.
Nupt_c      = 0.
Ndep_cum    = 0.
resps_c     = 0.
resps_mon   = 0.
resps_week  = 0.
totfol_lit  = 0.
totfol_lit_tree = 0.
totfrt_lit  = 0.
totfrt_lit_tree = 0.
tottb_lit   = 0.
totcrt_lit  = 0.
totstem_lit = 0.
C_opm_fol   = 0.
C_opm_frt   = 0.
C_opm_crt   = 0.
C_opm_tb    = 0.
N_opm_fol   = 0.
N_opm_frt   = 0.
N_opm_crt   = 0.
N_opm_tb    = 0.
C_opmfrt    = 0.
C_opmcrt    = 0.
N_opmfrt    = 0.
N_opmcrt    = 0.

diam_class_mvol = 0.
diam_classm = 0.

!Addition of biochar
if (flag_bc .gt. 0) then
    C_bc_tot    = 0.
    N_bc_tot    = 0.
    if (y_bc(y_bc_n) .eq. time) then     
        vol_bc = C_bc_appl(y_bc_n) * kgha_in_gm2 / dens_bc(y_bc_n)   ! cm�
        help = (cpart_bc(y_bc_n)*0.01) * C_bc_appl(y_bc_n) * kgha_in_gm2
        C_bc_appl(y_bc_n) = help
        help = help / cnv_bc(y_bc_n)
        N_bc_appl(y_bc_n) = help
        if (bc_appl_lay(y_bc_n) .gt. 0) then
         ! Ploughing until the layer of helpnr (meaning all layers are mixed to this depth)
            helpnl = bc_appl_lay(y_bc_n)
            thicki = 0.
            voli  = 0.
            pvi   = 0.
            fcapi = 0.
            wilti = 0.
            sandi = 0.
            clayi = 0.
            densi = 0.
            skeli = 0.
            pHi   = 0.
            wati  = 0.
            watsi = 0.
            wlami = 0.
            Copmi = 0.
            Nopmi = 0.
            Chumi = 0.
            Nhumi = 0.
            NH4i  = 0.
            NO3i  = 0.
            do i=1, helpnl
                thicki = thicki + thick(i)
            enddo
          ! all layers are proportionally combined
            do i=1, helpnl
                thickh = thick(i) / thicki
                sandi = sandi + thickh * sandv(i)
                clayi = clayi + thickh * clayv(i)
                pvi   = pvi + thickh * pv_v(i)
                skeli = skeli + thickh * skelv(i)
                densi = densi + thickh * dens(i)
                fcapi = fcapi + thickh * f_cap_v(i)
                wilti = wilti + thickh * wilt_p_v(i)
                wlami = wlami + thickh * wlam(i)
                wati  = wati + thickh * watvol(i)
                watsi = watsi + wats(i) 
                Copmi = Copmi + C_opm(i)
                Nopmi = Nopmi + N_opm(i)
                Chumi = Chumi + C_hum(i)
                Nhumi = Nhumi + N_hum(i)
                NH4i  = NH4i + NH4(i)
                NO3i  = NO3i + NO3(i)
            enddo
       ! new soil parameter calculation including biochar
                voli   = thicki * 10000.   ! cm�
                help   = voli + vol_bc 
                densi  = (voli * densi + vol_bc * dens_bc(y_bc_n)) / help    ! weighted mean or bulk density
                voli   = help
                thickh = voli / 10000.
             
            if (thickh .gt. depth(helpnl)) then 
               ! new soil profil calculated
                thicki1 = thickh - depth(helpnl) 
                thick(1)    = thicki1 
                depth(1)    = thicki1
                mid(1)      = 0.5 * thicki1
                 do i = 2, nlay           ! intercepted in last layer
                    depth(i) = depth(i-1) + thick(i)
                    mid(i)   = mid(i) + thick(i)
                enddo
            else
                thicki1 = thick(1)  
            endif

            do i = 1, helpnl
                thickh     = thick(i)/depth(helpnl)
                vol(i)      = thick(i) * 10000.
                sandv(i)    = sandi
                clayv(i)    = clayi
                siltv(i)    = 1. - sandi - clayi
                pv_v(i)     = pvi
                dens(i)     = densi
                skelv(i)    = skeli
                f_cap_v(i)  = fcapi
                wilt_p_v(i) = wilti 
                wlam(i)     = wlami
                watvol(i)   = wati
                wats(i)     = watsi * thickh   
                C_opm(i)    = Copmi * thickh
                N_opm(i)    = Nopmi * thickh
                C_hum(i)    = Chumi * thickh
                N_hum(i)    = Nhumi * thickh
                NH4(i)      = NH4i * thickh
                NO3(i)      = NO3i * thickh
                dmass(i)    = vol(i) * dens(i)
                humusv(i)   = C_hum(i) / (dmass(i) * cpart)
                C_bc(i)     = C_bc_appl(y_bc_n) * thickh
                N_bc(i)     = N_bc_appl(y_bc_n) * thickh
            enddo
            
            skelfact  = 1.
            pv        = skelfact * pv_v * thick * 0.1	       ! mm
            wilt_p    = skelfact * wilt_p_v * thick * 0.1	   ! mm
            field_cap = skelfact * f_cap_v * thick * 0.1	   ! mm
            thick_1   = thick(1)
            rmass1    = dmass(1) - (C_hum(1) + C_opm(1)) / cpart    ! adjustment amount of first layer

        ! calculation of surcharge of biochar 
            do i = 1, nlay
                if (C_bc(i) .gt. 0.) then
                    fcapi  = f_cap_v(i)
                    clayi = clayv(i)
                    silti = siltv(i)
                    humi  = humusv(i)*100.
                    hcbc   = C_bc(i)*100.*100. / (cpart_bc(y_bc_n) * dmass(i))
                    if ((clayi .le. 0.17) .and. (silti .le. 0.5)) then     ! sand
                        fcapi  = 0.0619 * hcbc
                        wilti = 0.0375 * hcbc
                        nfki = 7.0  
                    elseif ((clayi .le. 0.45) .and. (silti .gt. 0.17)) then   ! loam
                        fcapi  = 0.015 * hcbc
                        wilti = 0.0157 * hcbc
                        nfki = 10.
                    else                                          ! clay
                        fcapi  = -0.0109 * hcbc
                        wilti = -0.0318 * hcbc
                        nfki = 16.
                    endif
                    xfcap(i)  = xfcap(i) + fcapi
                    xwiltp(i) = xwiltp(i) + wilti
        
               endif       
            
            enddo

        else
            C_bc(1) = C_bc(1) + C_bc_appl(y_bc_n)
            N_bc(1) = N_bc(1) + N_bc_appl(y_bc_n)
        endif

        ! write into soil.ini
        WRITE (unit_soil,*)
        WRITE (unit_soil,'(A,I3,A,I4)') 'Adding of biochar up to layer',helpnl,' at the begin of year:', time
        WRITE (unit_soil,'(26A)') 'Layer',' Depth(cm)',' F-cap(mm)',' F-cap(Vol%)','   Wiltp(mm)', &
          ' Wiltp(Vol%)',' Pore vol.',' Skel.(Vol%)',' Density','  Spheat','      pH','    Wlam',    &
          ' Water(mm)',' Water(Vol%)',' Soil-temp.',' C_opm g/m2', &
          ' C_hum g/m2',' N_opm g/m2',' N_hum g/m2',' NH4 g/m2',' NO3 g/m2','  humus part',' d_mass g/m2', '  Clay','  Silt','  Sand'
        do i = 1,nlay
            WRITE (unit_soil,'(I5,2F10.2,3F12.2,F10.2,F12.2,4F8.2,F10.2,F12.2, 5F11.2,2F9.4,2E12.4, 3F6.1)') i,depth(i),field_cap(i),f_cap_v(i),wilt_p(i), &
            wilt_p_v(i),pv_v(i), skelv(i)*100., dens(i),spheat(i),phv(i),wlam(i),   &
            wats(i),watvol(i),temps(i),c_opm(i),c_hum(i),n_opm(i), n_hum(i),nh4(i),no3(i),humusv(i),dmass(i), clayv(i)*100., siltv(i)*100., sandv(i)*100.
        end do

        if (y_bc_n .lt. n_appl_bc) y_bc_n = y_bc_n + 1
    endif
endif    ! flag_bc  Addition of biochar

sumGPP      = 0.
sumTER      = 0.
GPP_mon     = 0.
GPP_week    = 0.
NEE_mon     = 0.
NPP_mon     = 0.
NPP_week    = 0.
TER_mon     = 0.
TER_week    = 0.

! save of last december value for calculation of seasons
aet_dec  = aet_mon(12)
temp_dec = temp_mon(12)
prec_dec = prec_mon(12)
rad_dec  = rad_mon(12)
hum_dec  = hum_mon(12)
GPP_dec  = GPP_mon(12)
NEE_dec  = NEE_mon(12)
NPP_dec  = NPP_mon(12)
TER_dec  = TER_mon(12)

temp_mon   = 0.
temp_week  = 0.
prec_mon   = 0.
prec_week  = 0.
tempmean_mo_a = 0.

rad_mon   = 0.
hum_mon   = 0.

flag_cumNPP = 1

  if (flag_wurz .eq. 4 .or. flag_wurz .eq. 6) then
      do k=1,nspecies
          svar(k)%Smean(1:nlay)=0.
      enddo
  endif

sumvsdead  = 0.
sumvsab    = 0.
sumvsab_m3 = 0.

do i = 1,nspec_tree
   do j = 1, lit_year-1
   ! shift of delayed litter 
     dead_wood(i)%C_tb(j)   = dead_wood(i)%C_tb(j+1)
     dead_wood(i)%N_tb(j)   = dead_wood(i)%N_tb(j+1)
     dead_wood(i)%C_stem(j) = dead_wood(i)%C_stem(j+1)
     dead_wood(i)%C_stem(j) = dead_wood(i)%C_stem(j+1)
   enddo  ! j (lit_year)
   dead_wood(i)%C_tb(lit_year)   = 0.
   dead_wood(i)%N_tb(lit_year)   = 0.
   dead_wood(i)%C_stem(lit_year) = 0.
   dead_wood(i)%C_stem(lit_year) = 0.
enddo   ! i (nspec_tree)

monrec=(/31,28,31,30,31,30,31,31,30,31,30,31/)

if (recs(time).eq.366) monrec(2)=29

photsum     =0.
npppotsum   =0.
nppsum      =0.
resosum     =0.
lightsum    =0.
nee         =0.
precsum     =0.
gppsum      =0.
tersum      =0.
resautsum   =0.
tempmean    =0.
tempmeanh  = 0.
med_air_cm = 0.
med_air_wm = 0.
laimax      =0.
drIndAl     =0.
gp_can_mean =0.
gp_can_min  =0.
gp_can_max  =0.
aet_sum     = 0.
pet_sum     = 0.

ind_arid_an = 0.
ind_lang_an = 0.
ind_cout_an = 0.
ind_wiss_an = 0.
ind_mart_an = 0.
ind_mart_vp = 0.
ind_emb =0.
ind_weck=0.
ind_reich =0.
con_gor = 0.
con_cur = 0.
con_con = 0.
cwb_an = 0.
ind_bud = 0.
ntindex = 0.

! species variables
svar%sumvsab   = 0.
svar%sumvsdead = 0.
svar%drIndAl   = 0.
svar%RedNm     = 0.
svar%Ndem      = 0.
svar%Nupt      = 0.
svar%Ndemp     = 0.
svar%Nuptp     = 0.

! fire index
Ndayshot      = 0
Psum_FP       = 0.
fire(1)%frequ = 0
fire(2)%frequ = 0
fire(3)%frequ = 0
fire_indi     = 0.
fire_indi_max = 0.
fire_indi_day = 0
fd_fire_indw  = 0
day_nest      = 0
p_nest        = 0.0

! species variable

spar%flag_endbb = 0.

if (flag_multi .eq. 6) then
   if (flag_sens .eq. 0) then
     ! save cohorts for the first time
      call save_cohort
      flag_sens = 1
   else
      call restore_cohort
   endif
endif


! initialize this year's summation variables of all cohorts to zero
  flag_tree = .TRUE.
  coh_ident_max = 0
  zeig => pt%first
  DO WHILE (ASSOCIATED(zeig))

    ! assimilation and NPP variables
    zeig%coh%NPP    = 0.
    zeig%coh%NPPpool = 0.
    zeig%coh%netAss = 0.
    zeig%coh%grossass  = 0.
    zeig%coh%maintres = 0.
    zeig%coh%t_leaf = 0.
    ! litter production variables
    zeig%coh%litC_fol  = 0.; zeig%coh%litN_fol  = 0.
    zeig%coh%litC_frt  = 0.; zeig%coh%litN_frt  = 0.
    zeig%coh%litC_stem = 0.; zeig%coh%litN_stem = 0.
    zeig%coh%litC_tb   = 0.; zeig%coh%litN_tb   = 0.
    zeig%coh%litC_crt  = 0.; zeig%coh%litN_crt  = 0.

    zeig%coh%litC_fold  = 0.; zeig%coh%litN_fold  = 0.
    zeig%coh%litC_frtd  = 0.; zeig%coh%litN_frtd  = 0.
    zeig%coh%litC_tbcd  = 0.; zeig%coh%litN_tbcd  = 0.
    zeig%coh%Nuptc_c    = 0.
    zeig%coh%Ndemc_c    = 0.
    zeig%coh%watuptc    = 0.

    ! annual drought index variables
    zeig%coh%drIndAl = 0.;  zeig%coh%nDaysGr = 0.
	zeig%coh%fl_sap = 1
    
    if ((zeig%coh%height .lt. thr_height .and. zeig%coh%species .le. nspec_tree) &
         .and. (flag_reg .eq. 15 .or. flag_reg .eq. 2 .or. flag_reg .eq. 18)) then
		 zeig%coh%fl_sap = 0

    end if
    coh_ident_max = max(coh_ident_max, zeig%coh%ident) 

!   number of tended trees
    zeig%coh%ntreet = 0
    zeig => zeig%next
  END DO

  zeig => pt%first
  DO WHILE (ASSOCIATED(zeig))

        if (zeig%coh%fl_sap.eq.0) then
		       flag_tree = .FALSE.
			   flag_sprout=1
			   exit
		end if
    zeig => zeig%next
  END DO


! Assisting field allocation; Hilfsfelder allok.
if (anz_coh > 0) then
    allocate (xwatupt(anz_coh, nlay))
    xwatupt = 0.
    allocate (xNupt(anz_coh, nlay))
    xNupt = 0.
    if (flag_wurz .eq. 6) then
        allocate (wat_left(anz_coh))
        wat_left=0.
    endif		
endif

! As cohort variable defined and initialised (not annual allocation); Als Kohorten-Var. vereinbart und dort initialisiert (nicht jaehrl. allok.!)
!  wat_mg is the watter absorption per cohort at flag_wred=9; wat_mg ist wasseraufnahme pro cohorte bei flag_wred=9

if (anz_tree .gt. 0) call root_distr

! calculation of additions for water capacities 
    call hum_add(xfcap, xwiltp, xpv)

    f_cap_v  = fcaph + xfcap        ! vol%
    wilt_p_v = wiltph + xwiltp      ! vol%
    pv_v     = pvh + xpv            ! vol%
    
    pv        = skelfact * pv_v * thick * 0.1	       ! mm
    wilt_p    = skelfact * wilt_p_v * thick * 0.1	   ! mm
    field_cap = skelfact * f_cap_v * thick * 0.1	   ! mm

do i=1,nlay
     if ((f_cap_v(i) < 0.) .or. (wilt_p_v(i) < 0.) .or. (pv_v(i) < 0.) .or. (pv(i) < 0.) .or. (wilt_p(i) < 0.)     &
      .or.(field_cap(i) < 0.) .or. (xfcap(i) < 0.) .or. (xwiltp(i) < 0.)) then
     continue
     endif
enddo

! output of new soil profile after recalculation of fcap etc.
if (flag_bc_add .gt. 0) then
    WRITE (unit_soil,'(26A)') 'Layer',' Depth(cm)',' F-cap(mm)',' F-cap(Vol%)','   Wiltp(mm)', &
      ' Wiltp(Vol%)',' Pore vol.',' Skel.(Vol%)',' Density','  Spheat','      pH','    Wlam',    &
      ' Water(mm)',' Water(Vol%)',' Soil-temp.',' C_opm g/m2', &
      ' C_hum g/m2',' N_opm g/m2',' N_hum g/m2',' NH4 g/m2',' NO3 g/m2','  humus part',' d_mass g/m2', '  Clay','  Silt','  Sand'
    do i = 1,nlay
        WRITE (unit_soil,'(I5,2F10.2,3F12.2,F10.2,F12.2,4F8.2,F10.2,F12.2, 5F11.2,2F9.4,2E12.4, 3F6.1)') i,depth(i),field_cap(i),f_cap_v(i),wilt_p(i), &
        wilt_p_v(i),pv_v(i), skelv(i)*100., dens(i),spheat(i),phv(i),wlam(i),   &
        wats(i),watvol(i),temps(i),c_opm(i),c_hum(i),n_opm(i), n_hum(i),nh4(i),no3(i),humusv(i),dmass(i), clayv(i)*100., siltv(i)*100., sandv(i)*100.
    end do
    flag_bc_add = 0
endif

! assigne CO2; CO2 belegen
  IF(flag_co2 > 0 .and. flag_co2 < 250) then
     co2 = co2_annual(time_cur)
  end if

end subroutine year_ini

!**************************************************************

SUBROUTINE RedN_ini
use data_soil_cn
use data_simul
use data_stand
use data_species
implicit none
INTEGER   i
REAL      cnv_tot


  cnv_tot=C_hum_tot/N_hum_tot

  do i=1,nspecies

    if (svar(i)%RedN .lt. 0.) then
       IF(cnv_tot.GT.13.2) THEN
        svar(i)%RedN = (1.-(120.-480./(cnv_tot-9.2))*spar(i)%Nresp)  !**0.7
        IF(svar(i)%RedN.LT.0) svar(i)%RedN = 0.
       ELSE
        svar(i)%RedN = 1.
       ENDIF
    endif
    IF(flag_limi==0.OR.flag_limi==1) svar(i)%RedN = 1.

  END DO

end subroutine RedN_ini

!**************************************************************

SUBROUTINE RedN_calc

use data_par
use data_soil_cn
use data_simul
use data_stand
use data_species

implicit none

integer i,j
real, dimension(20) :: reda, redb

do i = 1, 20
        reda(i) = 0.019
        redb(i) = 10.     ! min. N-availability necessary for growth in kg/ha; minimale N-Verfuegbarkeit kg/ha, die zum Wachsen benoetigt wird
enddo 

! after/nach Kopp & Schwanecke (1994)
  do j=1,anrspec
       i = nrspec(j)

       if ((flag_limi .eq. 15) .and. (i .ne. 10)) exit  ! only use flag_limi=14 for Douglas fir; f�r flag_limi=14 nur f�r Douglasie rechnen
       if(svar(i)%Ndem .gt. 0) then
           call RedN_Ndem(i)
       else
           if(svar(i)%RedN .le. 0.)  svar(i)%RedN = 1.
       endif

  END DO

end subroutine RedN_calc

!******************************************************************************

subroutine RedN_Ndem(ispec) 

!Adaptation for Ndem and Nupt; Adaption fuer Ndem und Nupt

use data_par
use data_soil_cn
use data_simul
use data_stand
use data_species

implicit none

integer ispec   ! species number
real reda, redb, hn

! If total demand is satisfied almost no limitation; Wenn gesamter Bedarf befriedigt wird, nahezu keine Limitierung (RedN=0.99)
! Koefficient reda calculated in dependency of Ndem; Koeff. reda wird in Abh. von Ndem berechnet

    reda = log(0.01) / ( - gm2_in_kgha*svar(ispec)%Ndem)
    redb = 0.5   

if (svar(ispec)%Nupt .lt. 1.)then
    if (svar(ispec)%Ndem .lt. 1) then
        hn = gm2_in_kgha * svar(ispec)%Nupt
    else
        hn = gm2_in_kgha * N_min
    endif
else
    hn = gm2_in_kgha * svar(ispec)%Nupt
endif

select case (ispec)
case (3)
    reda = 0.5 * reda
    redb = 0.3

case (10)
    reda = 2. * reda

end select

svar(ispec)%RedN = 1.- exp(-reda * hn - redb)
if (svar(ispec)%RedN .le. 0.) then
    continue
    svar(ispec)%RedN = 0.1
endif
end subroutine RedN_Ndem


!******************************************************************************

subroutine RedN_Ndem1(ispec, mini_N) 

!Adaptation for Ndem and Nupt after Bugmann/Lindner; Adaption fuer Ndem und Nupt nach Bugmann/Lindner

use data_par
use data_soil_cn
use data_simul
use data_stand
use data_species

implicit none

integer ispec   ! species number
real mini_N, reda

mini_N = 0.    ! Assumption: no growth if no N is absorbed, which means RedN=0.001; Ann.: kein Wachstum, wenn kein N aufgenommen wird, d.h. RedN = 0.001
! If total demand is satisfied almost no limitation; Wenn gesamter Bedarf befriedigt wird, nahezu keine Limitierung (RedN=0.99)
!  Koefficient reda calculated in dependency of Ndem; Koeff. reda wird in Abh. von Ndem berechnet

if((gm2_in_kgha*svar(ispec)%Ndem).gt. mini_N) then  
    reda = log(0.01) / (mini_N - gm2_in_kgha*svar(ispec)%Ndem)   ! Assumption/Ann.: RedN=0.99 bei Optimum
else
    reda = log(0.01) / (mini_N - 200)   ! Assumption/Ann.: bei 200 kg N/ha RedN=0.99, d.h. optimal
endif

svar(ispec)%RedN = 1.- exp(-reda * (gm2_in_kgha*svar(ispec)%Nupt - mini_N))
if (svar(ispec)%RedN .le. 0.) then
    continue
    svar(ispec)%RedN = 0.001
endif
   write (12345, '(F10.3)', advance='no') svar(ispec)%RedN
end subroutine RedN_Ndem1

!******************************************************************************

subroutine save_cohort

use data_soil
use data_stand

implicit none

type(coh_obj), pointer   :: p

integer i

anz_coh_save = anz_coh
allocate (coh_save(anz_coh))
zeig => pt%first
i = 1

do while (associated(zeig))
   coh_save(i) = zeig%coh
   i = i+1
   zeig => zeig%next
end do

end subroutine save_cohort

!******************************************************************************

subroutine restore_cohort 

use data_soil
use data_stand

implicit none

integer i

  zeig => pt%first
  do while (associated(zeig))
     pt%first => zeig%next
     deallocate(zeig)
     zeig => pt%first
  end do

   allocate(pt%first)
   pt%first%coh = coh_save(anz_coh_save)
   nullify(pt%first%next)

   do i = anz_coh_save-1,1, -1
      allocate(zeig)
      zeig%coh = coh_save(i)
      zeig%next => pt%first
      pt%first => zeig
   end do

end subroutine restore_cohort

!******************************************************************************

SUBROUTINE s_year

! yearly quantities

use data_climate
use data_depo
use data_evapo
use data_inter 
use data_out
use data_par
use data_simul
use data_site
use data_soil
use data_soil_cn
use data_species
use data_stand

implicit none

integer i,j
real help, helpd, h1, h2, h3
real temp_ampl   ! temperature amplitude between warmest and coldest month
logical l40, l80
real, dimension (nlay) :: xfcap, xwiltp, xpv

if (time .gt. 0) then
    ! climate quantites

    med_rad      = med_rad/recs(time)
    med_wind     = med_wind / recs(time)
    med_air      = med_air/recs(time)
    med_air_all  = med_air_all + med_air
    sum_prec_all = sum_prec_all + sum_prec

    med_rad_all  = med_rad_all + med_rad

	med_air_ms = med_air_ms/153
	med_air_mj = med_air_mj/92

    ! stress index
    mean_drIndAl = mean_drIndAl + drIndAl

    ! stomatal conductance of the canopy
    gp_can_mean = gp_can_mean / recs(time)

! climate indices
      if(pet_cum.ne.0.) then
		  ind_arid_an = sum_prec/pet_cum
	  else
		  ind_arid_an =0.
	  end if
	  cwb_an   = sum_prec-pet_cum
      cwb_an_m = cwb_an_m + cwb_an
	  if(med_air.ne. 0) then
		  ind_lang_an = sum_prec/med_air
	  else
		  ind_lang_an = 0
	  end if
	  temp_ampl = med_air_wm - med_air_cm

      ind_cout_an = ind_cout_an/12
      ind_wiss_an = ind_wiss_an/12
      ind_mart_vp = sum_prec_ms/(med_air_ms+10)
      if(med_air.lt. -9.9) then
	      ind_mart_an = 100
      else
	      ind_mart_an = sum_prec/(med_air+10)
      end if
      ind_weck = sum_prec_mj*days_rain_mj*(days_wof-60)/((med_air_mj+10)*92)/100
      h2 = sum_prec *days_rain
      h3 = 180*(med_air +10)
      ind_reich = h2/h3
      help = med_air_wm*med_air_wm - med_air_cm*med_air_cm
      if(help.ne.0) ind_emb = sum_prec*100/help
! Budyko
       ind_bud = rnet_cum*10/recs(time)       ! Radiation/Strahlung in kJ/m�
	   if(sum_prec.ne.0) then
	      ind_bud = ind_bud/(sum_prec*2.51)
	   end if


! continental indices
      h1 = (lat)/90*pi*0.5
        if (lat.ne.0) then
		  if(temp_ampl.gt.0) con_gor = 1.7*temp_ampl/sin(h1) -20.4
		  con_cur = temp_ampl/(1. + lat/3)
          h1 = (lat+10)/90*pi*0.5
		  if(temp_ampl .gt. 0.) con_con = 1.7*temp_ampl/sin(h1) - 14
        end if

   ind_arid_an_m = ind_arid_an_m + ind_arid_an
   ind_lang_an_m = ind_lang_an_m + ind_lang_an
   ind_cout_an_m = ind_cout_an_m + ind_cout_an
   ind_wiss_an_m =  ind_wiss_an_m + ind_wiss_an
   ind_mart_vp_m = ind_mart_vp_m + ind_mart_vp 
   ind_mart_an_m = ind_mart_an_m + ind_mart_an
   ind_weck_m = ind_weck_m + ind_weck
   ind_reich_m = ind_reich_m + ind_reich
   ind_emb_m = ind_emb_m + ind_emb
   con_gor_m = con_gor_m +con_gor
   con_cur_m = con_cur_m + con_cur
   con_con_m = con_con_m + con_con
   ind_bud_m = ind_bud_m + ind_bud
   ind_shc_m = ind_shc_m + ind_shc
endif

! water quantites
wat_tot  = SUM(wats)
perc_m   = perc_m + perc_cum
wupt_r_m = wupt_r_m + wupt_r_c
interc_m_can = interc_m_can + int_cum_can
interc_m_sveg= interc_m_sveg + int_cum_sveg
aet_m    = aet_m + aet_cum
pet_m = pet_m + pet_cum
dew_m    = dew_m + dew_cum

! C/N quantites
N_min_m      = N_min_m + N_min
Nupt_m       = Nupt_m + Nupt_c
Nleach_m     = Nleach_m + Nleach_c
resps_c_m    = resps_c_m + resps_c
autresp_m    = autresp_m + autresp
Ndep_cum_all = Ndep_cum_all + Ndep_cum

! C content up to 40, 80 and 100cm depth
l40 = .true.
l80 = .true.
C_hum_1   = C_hum(1)
C_tot_1   = C_hum(1) + C_opm(1)
C_hum_40  = C_hum_1
C_tot_40  = C_tot_1
C_hum_80  = C_hum_40
C_tot_80  = C_tot_40
C_hum_100 = C_hum_40
C_tot_100 = C_tot_40
do i = 2, nlay
    if ((depth(i)-depth(1)) .le. 40.) then
        C_hum_40 = C_hum_40 + C_hum(i)
        C_tot_40 = C_tot_40 + C_hum(i) + C_opm(i)
        C_hum_80  = C_hum_40
        C_tot_80  = C_tot_40
        C_hum_100 = C_hum_40
        C_tot_100 = C_tot_40
    else
        if (l40) then
            helpd = (40. - (depth(i-1)-depth(1))) / thick(i)
            C_hum_40 = C_hum_40 + C_hum(i)*helpd
            C_tot_40 = C_tot_40 + (C_hum(i) + C_opm(i))*helpd
            l40 = .false.
        endif
        if ((depth(i)-depth(1)) .le. 80.) then
            C_hum_80 = C_hum_80 + C_hum(i)
            C_tot_80 = C_tot_80 + C_hum(i) + C_opm(i)
            C_hum_100 = C_hum_80
            C_tot_100 = C_tot_80
        else
            if (l80) then
                helpd = (80. - (depth(i-1)-depth(1))) / thick(i)
                C_hum_80 = C_hum_80 + C_hum(i)*helpd
                C_tot_80 = C_tot_80 + (C_hum(i) + C_opm(i))*helpd
                l80 = .false.
            endif
            if ((depth(i)-depth(1)) .le. 100.) then
                C_hum_100 = C_hum_100 + C_hum(i)
                C_tot_100 = C_tot_100 + C_hum(i) + C_opm(i)
            else
                helpd = (100. - (depth(i-1)-depth(1))) / thick(i)
                C_hum_100 = C_hum_100 + C_hum(i)*helpd
                C_tot_100 = C_tot_100 + (C_hum(i) + C_opm(i))*helpd
                exit
            endif
        endif
    endif
enddo
C_hum_1  = C_hum_1 * gm2_in_kgha * 0.001         ! g/m2 --> t/ha
C_tot_1  = C_tot_1 * gm2_in_kgha * 0.001         ! g/m2 --> t/ha
C_hum_40 = C_hum_40 * gm2_in_kgha * 0.001         ! g/m2 --> t/ha
C_tot_40 = C_tot_40 * gm2_in_kgha * 0.001         ! g/m2 --> t/ha
C_hum_80 = C_hum_80 * gm2_in_kgha * 0.001         ! g/m2 --> t/ha
C_tot_80 = C_tot_80 * gm2_in_kgha * 0.001         ! g/m2 --> t/ha
C_hum_100 = C_hum_100 * gm2_in_kgha * 0.001       ! g/m2 --> t/ha
C_tot_100 = C_tot_100 * gm2_in_kgha * 0.001       ! g/m2 --> t/ha

! total anorganic N
N_an_tot = SUM(NH4) + SUM(NO3)

! N and C content of total humus
N_hum_tot = SUM(N_hum)
C_hum_tot = SUM(C_hum)

! N- and C-content befor litter fall
N_tot   = SUM(N_opm) + N_hum_tot + N_an_tot
C_tot   = SUM(C_opm) + C_hum_tot 

! N- and C-content of total biochar
if (flag_bc .gt. 0) then
    C_bc_tot = SUM(C_bc)
    N_bc_tot = SUM(N_bc)
endif

! Uptake per tree (conv. from cohort and m2)
   zeig => pt%first
   do while (associated(zeig))
     if (zeig%coh%watuptc .ge. 1E-8) then  
       do j = 1,nlay
         zeig%coh%rooteff(j) = zeig%coh%rooteff(j) / (zeig%coh%watuptc * thick(j)) 
       enddo
     endif
     zeig => zeig%next
     
   enddo

! Total foliage and fine root OPM
C_opm_stem  = 0.
do i=1,anrspec
   j = nrspec(i)
   C_opm_fol  = C_opm_fol + slit(j)%C_opm_fol
   C_opm_frt  = C_opm_frt + SUM(slit(j)%C_opm_frt)
   C_opm_crt  = C_opm_crt + SUM(slit(j)%C_opm_crt)
   C_opm_tb   = C_opm_tb  + slit(j)%C_opm_tb 
   C_opm_stem = C_opm_stem + slit(j)%C_opm_stem

    select case (flag_limi)
    case (4,5,6,7,8,9)
        if(svar(j)%sum_nTreeA .ne. 0  .or. svar(j)%sum_nTreeD .ne. 0) then
			 if(j.le.nspec_tree) then
			     svar(j)%RedNm = svar(j)%RedNm / (((svar(j)%sum_nTreeA+svar(j)%sum_nTreeD)*kpatchsize/10000.) * (spar(j)%end_bb-svar(j)%daybb))
        	 else
				svar(j)%RedNm = svar(j)%RedNm / (spar(j)%end_bb-svar(j)%daybb)
			 end if
        else
              svar(j)%RedN = 0.
		end if
    
    case default
        if (time .gt. 0) svar(j)%RedNm = svar(j)%RedNm / recs(time) 

    end select

   if (time .gt. 0) then
       if (svar(j)%RedN .gt. 0. .and. j .le. nspec_tree) then
            RedN_mean = RedN_mean + svar(j)%RedNm
            anz_RedN  = anz_RedN + 1
       endif
   endif

enddo

if (flag_hum .eq. 1) then
    ! Calculation of the new depth of cover layer; Berechnung der neuen Dicke fuer die Auflage
        help         = (C_opm(1) + C_hum(1)) / cpart  ! Masse (g
        thick(1)     = (rmass1 + help) / (dens(1)*10000.)       
        if (thick(1) .lt. 0.) then
        continue
        endif
    help = thick(1)-thick_1
    if (ABS(help) .ge. 1.)then   ! when first layer grows soil profile is shifted lower; bei Wachsen der 1.Schicht Profil nach unten verschieben
        helpd = depth(1) + help
        depth(1) = depth(1) + help
        mid(1)   = 0.5 * depth(1)
        do i=2, nlay-1           ! intercepted in last layer; wird in letzter Schicht aufgefangen 
            depth(i) = depth(i) + help
            mid(i)   = mid(i) + help
        enddo
        if (time .gt.0 .and. .not.flag_mult8910) then
            ! write/schreibe in soil.ini 
            WRITE (unit_soil,*)
            WRITE (unit_soil,'(A,I4)') 'Increase of first layer in year: ', time
            WRITE (unit_soil,'(26A)') 'Layer',' Depth(cm)',' F-cap(mm)',' F-cap(Vol%)','   Wiltp(mm)', &
              ' Wiltp(Vol%)',' Pore vol.',' Skel.(Vol%)',' Density','  Spheat','      pH','    Wlam',    &
              ' Water(mm)',' Water(Vol%)',' Soil-temp.',' C_opm g/m2', &
              ' C_hum g/m2',' N_opm g/m2',' N_hum g/m2',' NH4 g/m2',' NO3 g/m2','  humus part',' d_mass g/m2', '  Clay','  Silt','  Sand'
            do i = 1,nlay
                WRITE (unit_soil,'(I5,2F10.2,3F12.2,F10.2,F12.2,4F8.2,F10.2,F12.2, 5F11.2,2F9.4,2E12.4, 3F6.1)') i,depth(i),field_cap(i),f_cap_v(i),wilt_p(i), &
                wilt_p_v(i),pv_v(i), skelv(i)*100., dens(i),spheat(i),phv(i),wlam(i),   &
                wats(i),watvol(i),temps(i),c_opm(i),c_hum(i),n_opm(i), n_hum(i),nh4(i),no3(i),humusv(i),dmass(i), clayv(i)*100., siltv(i)*100., sandv(i)*100.
            end do
        endif
        thick_1 = thick(1)
    endif
    
    if (2.*C_hum(1) .lt. humusv(1)*dmass(1)) then
        humusv(1) = C_hum(1) / (dmass(1) * cpart)
    endif
    do i=2, nlay
        humusv(i) = C_hum(i) / (dmass(i) * cpart)
    enddo
    
endif

! Assisting fields deallok (due to probable changes in cohort amount)/Hifsfelder deallok (wegen evtl. geaenderter Koh.-Anzahl)
if (allocated(xwatupt)) deallocate (xwatupt)
if (allocated(xNupt)) deallocate (xNupt)
if (allocated(wat_left)) deallocate (wat_left)

END  subroutine s_year

!***************************************************************

SUBROUTINE fire_year

!calculation of mean fire risk index of a year

USE data_biodiv
use data_climate

implicit none

integer i,j
real    hsum1, hsum2

do i = 1,3
   hsum1 = 0.
   hsum2 = SUM(fire(i)%frequ)
   do j = 1,5
      hsum1 = hsum1 + fire(i)%frequ(j) * j
   enddo
   if (hsum2 .ne. 0) then
       fire(i)%mean   = hsum1 / hsum2
   else
        fire(i)%mean   = -99.0
   endif
   fire(i)%mean_m = fire(i)%mean_m + fire(i)%mean
enddo

! fire index Bruschek
if(flag_climtyp .ge. 3) then
    if(Psum_FP.ne.0.) fire_indb = Ndayshot/Psum_FP
    fire_indb_m = fire_indb_m + fire_indb
else
    fire_indb   = -99.0
    fire_indb_m = -99.0
endif

END subroutine fire_year

!***************************************************************

SUBROUTINE t_indices (htempm)

use data_biodiv 
use data_simul

implicit none

real, dimension(12):: htempm
ntindex = 0.

! Nonnen-Temperatur-Index after/nach Zw�lfer (1935)
if(time.gt.0) then
    htempm = htempm / monrec
    ntindex = (htempm(4)-4.9) * 30. + (htempm(5)-4.9) * 3.  + (htempm(5)-3.2) * 17. + (htempm(5)-5.7) * 8. &
            + (htempm(5)-7.2) * 3.  + (htempm(6)-7.2) * 6.  + (htempm(6)-7.6) * 10. + (htempm(6)-7.8) * 14. &
            + (htempm(7)-6.0) * 18. + (htempm(7)-8.4) * 13. + (htempm(8)-8.4) * 2.  + (htempm(8)-6.8) * 29. &
            + (htempm(9)-6.8) * 30.

    ntindex = ntindex/1240
endif

END subroutine t_indices

!***************************************************************
