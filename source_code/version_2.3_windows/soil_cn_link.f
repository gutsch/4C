!*****************************************************************!
!*                                                               *!
!*              4C (FORESEE) Simulation Model                    *!
!*                                                               *!
!*                                                               *!
!*                    Subroutines for:                           *!
!*        linking SOIL_C/N - Programs with forest module         *!
!*                                                               *!
!*                    Author: F. Suckow                          *!
!*                                                               *!
!*   contains:                                                   *!
!*   S_CN_INI                                                    *!
!*   S_CN_GENER                                                  *!
!*   CN_INP                                                      *!
!*   N_UPT(jlay)                                                 *!
!*   READ_LITTER_INPUT                                           *!
!*                                                               *!
!*                  Copyright (C) 1996-2018                      *!
!*     Potsdam Institute for Climate Impact Reserach (PIK)       *!
!*          Authors and contributors see AUTHOR file             *!
!*  This file is part of 4C and is licensed under BSD-2-Clause   *!
!*                   See LICENSE file or under:                  *!
!*     http://www.https://opensource.org/licenses/BSD-2-Clause   *!
!*                           Contact:                            *!
!*       https://gitlab.pik-potsdam.de/foresee/4C                *!
!*                                                               *!
!*****************************************************************!

    SUBROUTINE s_cn_ini

! Initialisation of soil data and parameters for C/N-module

use data_simul
use data_soil
use data_soil_cn
use data_species
use data_stand

implicit none

integer i, j
type (species_litter)  :: sliti
real, external         :: f_cnv, rmin_p, rnit_p
real                   :: xx, xcnv

! turnover biochar
k_bc = 0.00005
k_syn_bc =0.03

do j = 1, nlay
    if (C_hum(j) .lt.0.) then
        if (.not.flag_mult8910) call error_mess(time, 'missing value of C_hum set to 0.0 in layer ', real(j))
        C_hum(j) = 0.0
    endif
    if (N_hum(j) .lt.0.) then
        if (.not.flag_mult8910) call error_mess(time, 'missing value of N_hum set to 0.0 in layer ', real(j))
        N_hum(j) = 0.0
    endif
enddo

!!! zum Test ohne Primaersubstanz !!!
C_opm = 0.
N_opm = 0.

!!! zum Test ohne Primaersubstanz !!!
call s_cn_gener  

! Convert concentration (mg/l) into contents (g/m2) per layer
NH4 = NH4 * 0.001 *wats
NO3 = NO3 * 0.001 *wats


if (flag_lit .eq. 0) then
 ! to even out balance for generated values ==> new values for C_ / N_hum 
    do j = 1, nlay
    xx = C_hum(j)
    if (N_hum(j) .gt. 1E-6) then
      xcnv = f_cnv(C_hum(j), N_hum(j))
      if (xx .gt. C_opm(j)) then
	     C_hum(j) = xx - C_opm(j) 
         N_hum(j) = C_hum(j) / xcnv
      endif
    endif	    
  enddo
endif

! reduction of mineralization and nitrification depending on pH
do j=1,nlay
   if (phv(j) .lt. 0) then
       rmin_phv(j) = 1
       rnit_phv(j) = 1
   else
       rmin_phv(j) = rmin_p(phv(j))
       rnit_phv(j) = rnit_p(phv(j))
   endif
   cnv_opm(j)  = f_cnv(C_opm(j), N_opm(j))
   cnv_hum(j)  = f_cnv(C_hum(j), N_hum(j))
enddo

call s_year    ! calculate a year's values for start year as well
wats(1) = field_cap(1)   ! ensuring consistency, in case novel calculation was done in s_year
 
! yearly cumulative quantities
N_min     = 0.
N_lit     = 0.
C_lit     = 0.
C_accu    = C_tot
Nleach_c = 0.
Nupt_c   = 0.
Nupt_d   = 0.
resps_c  = 0.

END subroutine s_cn_ini

!**************************************************************

SUBROUTINE s_cn_gener

! Initialisation of soil data and parameters for C/N-module

use data_par
use data_simul
use data_soil
use data_soil_cn
use data_species
use data_stand

implicit none

integer i, j
real    dbm_c     ! C content of dead biomass
real    dbm_frt   ! C content of dead fine root biomass
real    dbm_part  ! part of dead biomass of previous years
real    e_part    ! part of dead biomass of one year
real    t_day
real    hconvd    ! conversion factor kg/patchsize ==> g/m2
real    hconvda   ! conversion in C content and from tree to cohort  
real, external         :: f_cnv
type (species_litter)  :: sliti

C_opm = 0.
N_opm = 0.

do i = 1, nspecies
     if (i .eq. nspec_tree+2) then
        continue
     endif
  sliti  = slit(i)
  sliti%species_name = spar(i)%species_name

  if (flag_lit .eq. 0) then
    sliti%C_opm_fol  = 0. 
    sliti%C_opm_tb   = 0. 
    sliti%C_opm_stem = 0. 
    sliti%C_opm_frt  = 0. 
    sliti%C_opm_crt  = 0. 
  endif

  sliti%N_opm_fol  = 0.
  sliti%N_opm_tb   = 0.
  sliti%N_opm_stem = 0.
  sliti%N_opm_frt  = 0.
  sliti%N_opm_crt  = 0.

  slit(i) = sliti

enddo

hconvd = 1000. / kpatchsize  

if (flag_lit .eq. 0) then
   zeig => pt%first
   do 
     if (.not. associated(zeig)) exit

     i = zeig%coh%species
         if (i .ne. nspec_tree+2) then   ! no litter initialisation for Mistletoe
         sliti   = slit(i)
         hconvda = cpart * zeig%coh%ntreea 
        ! consider decomposition rate, i.e. biomass of previous years
         j = 1
         t_day = 365.
         dbm_part = 0.
         do 
           ! consider dependency on temp. and water 
            e_part = exp (-spar(i)%k_opm_fol * 0.2 * j * t_day)
            dbm_part = dbm_part + e_part
	        if (e_part .gt. 0.001) then
    	       j = j+1
            else
	           exit
	        endif
         enddo

	     select case (flag_dis)
	     case (1,2)
	     zeig%coh%litC_fol = spar(i)%psf * zeig%coh%x_fol * hconvda  !(spar(i)%psf * zeig%coh%x_fol+zeig%coh%x_fol_loss) * hconvda, diese idee wieder weg   ! Umrechnung in g/m2 erst in subr. litter 
	     case (0)
	     zeig%coh%litC_fol = spar(i)%psf * zeig%coh%x_fol * hconvda   ! conversion in g/m2 first into subr. litter
	     end select
         
         zeig%coh%litN_fol = zeig%coh%litC_fol * (1.-spar(i)%reallo_fol) / spar(i)%cnr_fol
         dbm_c = dbm_part * zeig%coh%litC_fol * hconvd
         sliti%C_opm_fol  = sliti%C_opm_fol + dbm_c 
         
         !dead fine root biomass of humus layer
         ! consider decomposition rate, i.e. biomass of previous years
         j = 1
         t_day = 365.
         dbm_part = 0.
         do 
           ! consider dependency on temp. and water 
            e_part = exp (-spar(i)%k_opm_frt * 0.2 * j * t_day)
            dbm_part = dbm_part + e_part
            if (e_part .gt. 0.001) then
	           j = j+1
	        else
	           exit
	        endif
         enddo
         
    ! change see foliage
	     select case (flag_dis)
	     case (1,2)
	     zeig%coh%litC_frt = spar(i)%psr * zeig%coh%x_frt * hconvda  !  conversion in g/m2 first into subr. litter 
	     case (0)
	     zeig%coh%litC_frt = spar(i)%psr * zeig%coh%x_frt * hconvda   ! conversion in g/m2 first into subr. litter 
	     end select
	 
         zeig%coh%litN_frt = zeig%coh%litC_frt * (1.-spar(i)%reallo_frt) / spar(i)%cnr_frt
         dbm_c   = dbm_part * zeig%coh%litC_frt * hconvd * (1.-spar(i)%reallo_frt) / spar(i)%cnr_frt 
         dbm_frt = dbm_c * zeig%coh%frtrel(1)   
         sliti%C_opm_frt(1)  = sliti%C_opm_frt(1) + dbm_frt 
         sliti%N_opm_frt(1)  = sliti%N_opm_frt(1) + dbm_frt

     !    Total fine root biomass must be distributed over all soil layers
         do  j = 2, nlay
            dbm_frt = dbm_c * zeig%coh%frtrel(j)   
            sliti%C_opm_frt(j)  = sliti%C_opm_frt(j) + dbm_frt 
            sliti%N_opm_frt(j)  = sliti%N_opm_frt(j) + dbm_frt
         enddo
      
         slit(i) = sliti
     endif   ! (i .ne. nspec_tree+2)
     
     zeig => zeig%next
   enddo
endif


do i = 1, (nspec_tree+1)   !exclusion of mistletoe
  sliti  = slit(i) 

  if (flag_lit .gt. 0) then
     dbm_frt = sliti%C_opm_frt(1)
     dbm_c   = sliti%C_opm_crt(1)
     do  j = 1, nlay
        sliti%C_opm_frt(j)  = dbm_frt * root_fr(j) 
        sliti%N_opm_frt(j)  = sliti%C_opm_frt(j) * (1.-spar(i)%reallo_frt) / spar(i)%cnr_frt
        sliti%C_opm_crt(j)  = dbm_c * root_fr(j) 
     enddo    
  endif

  sliti%N_opm_fol  = sliti%C_opm_fol * (1.-spar(i)%reallo_fol) / spar(i)%cnr_fol
     
 ! pools of dead biomass without stems
  C_opm(1) = C_opm(1) + sliti%C_opm_fol + sliti%C_opm_tb + sliti%C_opm_frt(1) + sliti%C_opm_crt(1)
  N_opm(1) = N_opm(1) + sliti%N_opm_fol + sliti%N_opm_tb + sliti%N_opm_frt(1) + sliti%N_opm_crt(1)
  slit(i) = sliti

enddo

do j=2,nlay
   C_opm(j) = SUM(slit%C_opm_frt(j))  ! + SUM(slit%C_opm_crt(j))
   N_opm(j) = SUM(slit%N_opm_frt(j))  ! + SUM(slit%N_opm_crt(j))
   if (C_opm(j) < 0.) then
   continue
   endif
enddo

! Total OPM of all species

END subroutine s_cn_gener

!**************************************************************
 
SUBROUTINE cn_inp

! Input of dead biomass (all fractions) into soil C- and N-pools
! call from simulation_4C

use data_simul
use data_par
use data_soil
use data_soil_cn
use data_species
use data_stand

implicit none

integer i, j    
real hconvd, hf, hc, hfc, hfn, hfrtc, hfrtn, hfc1, Copm, Nopm, Clitf, Nlitf    
type (species_litter)  :: sliti
real, external         :: f_cnv

Clitf = 0.
Nlitf = 0.
N_lit = 0.
C_lit = 0.
C_lit_fol  = 0.
N_lit_fol  = 0.
C_lit_frt  = 0.
N_lit_frt  = 0.
C_lit_crt  = 0.
N_lit_crt  = 0.
C_lit_tb   = 0.
N_lit_tb   = 0.
C_lit_stem = 0.
N_lit_stem = 0.

select case (flag_decomp)
case (20,21)
    if (time .gt. 0) call read_litter_input

case(30,31)
    continue

case default
    ! Input of litter into primary organic matter pools
    ! litter: x kg/tree to g/m2  (n*x*1000g/(kPatchSize m2)) 
    ! values are aggregated already as cohort
    hconvd = 1000. / kpatchsize
    zeig => pt%first
    do while (associated(zeig)) 
      ns = zeig%coh%species
      sliti  = slit(ns)

      sliti%C_opm_fol  = sliti%C_opm_fol  + zeig%coh%litC_fol  * hconvd  
      sliti%N_opm_fol  = sliti%N_opm_fol  + zeig%coh%litN_fol  * hconvd  

      sliti%C_opm_stem = sliti%C_opm_stem + zeig%coh%litC_stem * hconvd
      sliti%N_opm_stem = sliti%N_opm_stem + zeig%coh%litN_stem * hconvd

      sliti%C_opm_tb   = sliti%C_opm_tb + zeig%coh%litC_tb * hconvd
      sliti%N_opm_tb   = sliti%N_opm_tb + zeig%coh%litN_tb * hconvd

      hfc    = zeig%coh%litC_frt * hconvd   
      hfn    = zeig%coh%litN_frt * hconvd   
      hfrtc  = hconvd * zeig%coh%litC_crt
      hfrtn  = hconvd * zeig%coh%litN_crt
  
      do i = 1,nroot_max
         hfc1 = zeig%coh%frtrel(i)
         sliti%C_opm_frt(i)  = sliti%C_opm_frt(i) + hfc * hfc1
         sliti%N_opm_frt(i)  = sliti%N_opm_frt(i) + hfn * hfc1
         sliti%C_opm_crt(i)  = sliti%C_opm_crt(i) + hfrtc * hfc1
         sliti%N_opm_crt(i)  = sliti%N_opm_crt(i) + hfrtn * hfc1
      enddo   ! i (nroot_max)

      C_lit_frt  = C_lit_frt + zeig%coh%litC_frt 
      N_lit_frt  = N_lit_frt + zeig%coh%litN_frt 
      C_lit_crt  = C_lit_crt + zeig%coh%litC_crt 
      N_lit_crt  = N_lit_crt + zeig%coh%litN_crt 
      C_lit_fol  = C_lit_fol + zeig%coh%litC_fol
      N_lit_fol  = N_lit_fol + zeig%coh%litN_fol
      C_lit_tb   = C_lit_tb + zeig%coh%litC_tb
      N_lit_tb   = N_lit_tb + zeig%coh%litN_tb
      C_lit_stem = C_lit_stem + zeig%coh%litC_stem
      N_lit_stem = N_lit_stem + zeig%coh%litN_stem
  
      slit(ns) = sliti
      zeig => zeig%next
    enddo  ! show (cohorts)  

    do i = 1,nspec_tree
       ! input of delayed litter fall from dead stems
         slit(i)%C_opm_tb    = slit(i)%C_opm_tb + dead_wood(i)%C_tb(1)
         slit(i)%N_opm_tb    = slit(i)%N_opm_tb + dead_wood(i)%N_tb(1)
         slit(i)%C_opm_stem  = slit(i)%C_opm_stem + dead_wood(i)%C_stem(1)
         slit(i)%N_opm_stem  = slit(i)%N_opm_stem + dead_wood(i)%N_stem(1)
         C_lit_tb            = C_lit_tb + dead_wood(i)%C_tb(1)
         N_lit_tb            = N_lit_tb + dead_wood(i)%N_tb(1)
         C_lit_stem          = C_lit_stem + dead_wood(i)%C_stem(1)
         N_lit_stem          = N_lit_stem + dead_wood(i)%N_stem(1)
    enddo   ! i (nspec_tree)

    !  conversion g/m2/patch -->  g/m2
       C_lit_fol = C_lit_fol * hconvd
       N_lit_fol = N_lit_fol * hconvd
       C_lit_frt = C_lit_frt * hconvd
       N_lit_frt = N_lit_frt * hconvd
       C_lit_crt = C_lit_crt * hconvd
       N_lit_crt = N_lit_crt * hconvd
       C_lit_tb  = C_lit_tb * hconvd
       N_lit_tb  = N_lit_tb * hconvd
       C_lit_stem= C_lit_stem * hconvd
       N_lit_stem= N_lit_stem * hconvd
end select   ! flag_decomp

do j=1,nlay
   cnv_opm(j)  = f_cnv(C_opm(j), N_opm(j))
   cnv_hum(j)  = f_cnv(C_hum(j), N_hum(j))
enddo

   Clitf   = C_lit_frt + C_lit_crt 
   Nlitf   = N_lit_frt + N_lit_crt 
   C_lit   = C_lit_fol + C_lit_tb + Clitf
   N_lit   = N_lit_fol + N_lit_tb + Nlitf
   C_lit_m = C_lit + C_lit_m
   N_lit_m = N_lit + N_lit_m

   C_opm = 0.
   N_opm = 0.
   C_opm_stem = 0.
   do i = 1,nspecies
      C_opm(1) = C_opm(1) + slit(i)%C_opm_frt(1) + slit(i)%C_opm_crt(1)  &
                 + slit(i)%C_opm_fol + slit(i)%C_opm_tb
      N_opm(1) = N_opm(1) + slit(i)%N_opm_frt(1) + slit(i)%N_opm_crt(1)  &
                 + slit(i)%N_opm_fol + slit(i)%N_opm_tb
      C_opm_stem = C_opm_stem + slit(i)%C_opm_stem
      do j = 2,nlay
         C_opm(j) = C_opm(j) + slit(i)%C_opm_frt(j) + slit(i)%C_opm_crt(j)
         N_opm(j) = N_opm(j) + slit(i)%N_opm_frt(j) + slit(i)%N_opm_crt(j)
      enddo
   enddo
   
END subroutine cn_inp

!**************************************************************

SUBROUTINE read_litter_input

!   Reading of litter input data

use data_soil_cn
use data_simul

integer lyear, lspec, ios
real helpC, helpN
logical :: lin = .TRUE.
type (species_litter)  :: sliti

  if (lin) read(unit_litter,*,iostat=ios) lyear, lspec, helpC, helpN
    if (ios .lt. 0) lin = .FALSE.

  do while (lyear .lt. time_cur)
    if (lin) read(unit_litter,*,iostat=ios) lyear, lspec, helpC, helpN
    if (ios .lt. 0) then
        lin = .FALSE.
        exit
    endif
  enddo

  do while (lyear .eq. time_cur) 
      sliti = slit(lspec)
      sliti%C_opm_fol  = sliti%C_opm_fol  + helpC  
      sliti%N_opm_fol  = sliti%N_opm_fol  + helpN
      C_lit_fol  = C_lit_fol + helpC
      N_lit_fol  = N_lit_fol + helpN

      slit(lspec) = sliti
    if (lin) read(unit_litter,*,iostat=ios) lyear, lspec, helpC, helpN
    if (ios .lt. 0) then
        lin = .FALSE.
        exit
    endif
  enddo

  if (lin) backspace (unit_litter)

END subroutine read_litter_input

!**************************************************************

SUBROUTINE n_upt(jlay)

!   N uptake by roots

use data_climate
use data_par
use data_simul
use data_soil
use data_soil_cn
use help_soil_cn
use data_species
use data_stand

implicit none

! input:
integer jlay        ! number of actual layer
integer i, ntr 

!-----------------------------------------------------------

real NH4f, NO3f        ! free available NH4-, NO3-N
real NH4u, NO3u, Nutot ! uptake of NH4-N, NO3-N, Nan_tot
real NH4jl, NO3jl      ! NH4-, NO3-N
real watlay            ! total water content of layer before uptake and perc.
real upt_w             ! relative part of uptake water
real :: etau = 0.036   ! parameter from A. Friend (1997)
real :: fft            ! temperature function of uptake from Thornley (1991)
real :: ft0   =  0. , &
        ftmax = 30. , &
        ftref = 20.    ! parameter (°C) of temperature function from Thornley (1991)
real help, hNupt, hNupt1, Nutot1, h1, h2, N_ava, frtrel, hfrtrel, hxw
real, dimension(1:anz_coh) :: N_dem   ! auxilary array for cohorts
real, external :: fred1

! no roots -> no N-uptake
if (root_fr(jlay) .lt. 1E-10) then
   Nupt(jlay) = 0.
   return
endif    

! all NH4 and NO3 plant available
NH4jl = NH4(jlay) 
NO3jl = NO3(jlay) 
NH4f  = NH4jl 
NO3f  = NO3jl 

! relative part of uptake water
watlay = wats(jlay) + wupt_r(jlay)
upt_w  = wupt_r(jlay) / watlay
! uptake of total available N
upt_w = 1.

fft = (temps(jlay)-ft0)*(2.*ftmax-ft0-temps(jlay))/((ftref-ft0)*(2.*ftmax-ft0-ftref))
if (fft .lt. 0.) then
    fft = 0.
else 
    if (fft .gt. 1) fft = 1.
endif

NH4u   = NH4f * fred1(jlay)
NO3u   = NO3f * fft * fred1(jlay)
Nutot  = (NH4u + NO3u)
Nutot1 = 0.    	! actual N uptake per layer

! Uptake per cohort and m2

select case (flag_decomp)
  case (0, 1, 10, 11, 20, 21, 30, 31)
    if (wupt_r(jlay) .lt. 1E-10) then
       Nupt(jlay) = 0.
       return
    else 
        ! new balance
        NH4jl  = NH4(jlay) - NH4u
        NO3jl  = NO3(jlay) - NO3u
        if (Nutot .ge. zero) then
            i = 1
            hxw = 0.
            zeig => pt%first
            do while (associated(zeig))
              if (zeig%coh%species.ne.nspec_tree+2) then   !exclude mistletoe
                  ntr = zeig%coh%ntreea
                  hNupt = Nutot * xwatupt(i,jlay) / wupt_r(jlay)   
                  Nutot1 = Nutot1 + hNupt
                  N_ava = hNupt * kpatchsize
                  N_ava = N_ava /ntr          ! g per tree

                  zeig%coh%Nuptc_d = zeig%coh%Nuptc_d + N_ava    ! in g per tree

                 N_ava     = N_ava * ntr    ! Balance in g/m2

                 i = i+1
                 endif   !exclusion of mistletoe
                 zeig => zeig%next
            enddo
            Nutot1 = Nutot
        else
            Nutot1 = 0.
            do i = 1, anz_coh
                 xNupt(i,jlay) = 0.
            enddo
        endif
    endif

  case (40, 41)
   ! Ansatz A. Friend (1997, Gl. 13)
    if (Nutot .ge. 1.e-6) then
        i = 1
        hxw = 0.
        zeig => pt%first
        do while (associated(zeig))
           if (zeig%coh%species.ne.nspec_tree+2) then   !exclude mistletoe
             ntr = zeig%coh%ntreea
             frtrel = zeig%coh%frtrelc(jlay)       ! root percentage of the entire cohort
             hNupt  = frtrel * Nutot        ! available nitrogen per tree cohort and layer g
             hxw = hxw + frtrel
             h2     = Nutot * kpatchsize
             h1     = h2 * frtrel
             N_ava  = h1/ntr
             hNupt1 = N_ava
             h1 = zeig%coh%Ndemc_c - zeig%coh%Nuptc_c
             h2 = zeig%coh%Ndemc_d - zeig%coh%Nuptc_d
             help = h1 + h2      ! limited by actual and resudual cohort N-demand in g/m2
             ! limited by actual and residual cohort N-demand in g per tree
             if (help .gt. N_ava) then   

                 zeig%coh%Nuptc_d = zeig%coh%Nuptc_d + N_ava    ! in g per tree

             else
                 if (help .gt. 0.) then
                     zeig%coh%Nuptc_d = zeig%coh%Nuptc_d + help     ! in g/m2 per Coh.
                     N_ava = help
                 else
                     N_ava = 0.
                 endif
             endif
             N_ava = N_ava * ntr    ! balance in g/m2

             h1 = N_ava
             if (NH4jl .lt. h1+zero) then
                h1    = h1 - NH4jl
                NH4jl = zero
                 if (NO3jl .lt. h1+zero) then
                    h1    = h1 - NO3jl
                    NO3jl = zero
                 else
                    NO3jl = NO3jl - h1 
                 endif
             else
                NH4jl = NH4jl - h1
                h1    = 0. 
             endif
             if ((NH4jl .lt. 0.) .or. (NO3jl .lt. 0.)) then
                continue
             endif
             Nutot1 = Nutot1 + N_ava 
             xNupt(i,jlay) = N_ava

          i = i+1
          endif  !exclusion of mistletoe
          zeig => zeig%next

        enddo
        Nutot1 = Nutot1/kpatchsize

    else
        Nutot1 = 0.
        do i = 1, anz_coh
             xNupt(i,jlay) = 0.
        enddo
    endif

  end select
  
NH4(jlay)  = NH4jl 
NO3(jlay)  = NO3jl 
Nupt(jlay) = Nutot1	! N uptake per layer

END subroutine n_upt

!**************************************************************
