!*****************************************************************!
!*                                                               *!
!*              4C (FORESEE) Simulation Model                    *!
!*                                                               *!
!*                                                               *!
!*                    Subroutines for:                           *!
!*      readspec:   Read species parameters from file            *!
!*                                                               *!
!*                  Copyright (C) 1996-2018                      *!
!*     Potsdam Institute for Climate Impact Reserach (PIK)       *!
!*          Authors and contributors see AUTHOR file             *!
!*  This file is part of 4C and is licensed under BSD-2-Clause   *!
!*                   See LICENSE file or under:                  *!
!*     http://www.https://opensource.org/licenses/BSD-2-Clause   *!
!*                           Contact:                            *!
!*       https://gitlab.pik-potsdam.de/foresee/4C                *!
!*                                                               *!
!*****************************************************************!

 SUBROUTINE readspec

! input of species data from file

use data_par
use data_simul
use data_species
use data_stand
use data_soil_cn
use data_soil
implicit none
integer i,ios,nowunit
character text
logical ex

nowunit=getunit()
     if (.not.flag_mult8910) then
         print *,' '
         print *,' >>>foresee message: now reading species parameter file...'
     endif

do
  call testfile(specfile(ip),ex)
  if (.not.flag_mult8910) print *,' '
  if(ex .eqv. .false.) cycle
  exit
end do

open(nowunit, FILE=trim(specfile(ip)), ACTION="READ")
do
  read(nowunit,'(A)') text
  if (text .ne. '!') then
    exit
  end if
end do
backspace nowunit

  read(nowunit,*) text, nspecies
  read(nowunit,*) text, nspec_tree
  
  if(.not.allocated(spar)) allocate(spar(nspecies))
  if(.not.allocated(svar)) allocate(svar(nspecies))
  if(.not.allocated(nrspec)) allocate(nrspec(nspecies))
  nrspec = 0

! read intermediate lines
  do
    read(nowunit,'(A)') text
    if (text .ne. '!') then
        exit
    end if
  end do
  backspace nowunit

  do i=1,nspecies
    read(nowunit,*) text,spar(i)%species_name
    if (text .ne. '!') then
 	    svar(i)%daybb      = 0
	    svar(i)%ext_daybb  = 0
	    svar(i)%sum_nTreeA = 0
	    svar(i)%anz_coh    = 0
	    svar(i)%RedN       = -99.0
	    svar(i)%RedNm      = 0.0
	    svar(i)%med_diam   = 0.0
	    svar(i)%dom_height = 0.0
	    svar(i)%drIndAl    = 0.0
	    svar(i)%sumNPP     = 0.0
	    svar(i)%sum_bio    = 0.0
	    svar(i)%sum_lai    = 0.0
	    svar(i)%act_sum_lai= 0.0
	    svar(i)%fol        = 0.0
	    svar(i)%hrt        = 0.0
	    svar(i)%sap        = 0.0
	    svar(i)%frt        = 0.0
	    svar(i)%totsteminc = 0.0
	    svar(i)%totstem_m3 = 0.0
	    svar(i)%sumvsab    = 0.0
        svar(i)%sumvsdead     = 0.0
        svar(i)%sumvsdead_m3 = 0.
	    svar(i)%crown_area = 0.0
	    svar(i)%Ndem       = 0.0
	    svar(i)%basal_area = 0.0
	    svar(i)%sumvsab    = 0.0
    else
        write (*,*) '! *** not enough species in ', specfile(ip), (i-1),' of ', nspecies 
        call errorfile (specfile(ip), 0, nowunit)
        call error_mess(time, 'not enough species in '//specfile(ip), real(i-1))
        exit
    endif
  enddo

! read intermediate lines
  read(nowunit,'(A)') text
  if (text .ne. '!') then
      do
        read(nowunit,'(A)') text
        if (text .eq. '!') then
            do
              read(nowunit,'(A)') text
              if (text .ne. '!') then
                exit
              end if
            end do
            exit
        end if
      end do
  else
    do
      read(nowunit,'(A)') text
      if (text .ne. '!') then
        exit
      end if
    end do
  endif
  backspace nowunit

  read(nowunit,*) text,(spar(i)%species_short_name,i=1,nspecies)  ! read abbreviated names
  read(nowunit,*) text,(spar(i)%max_age,i=1,nspecies)
  read(nowunit,*) text,(spar(i)%yrec,i=1,nspecies)
  read(nowunit,*) text,(spar(i)%stol,i=1,nspecies)
  read(nowunit,*) text,(spar(i)%pfext, i=1,nspecies)
  read(nowunit,*) text,(spar(i)%sigman,i=1,nspecies)
  read(nowunit,*) text,(spar(i)%respcoeff,i=1,nspecies)
  read(nowunit,*) text,(spar(i)%prg,i=1,nspecies)
  read(nowunit,*) text,(spar(i)%prms,i=1,nspecies)
  read(nowunit,*) text,(spar(i)%prmr,i=1,nspecies)
  read(nowunit,*) text,(spar(i)%psf,i=1,nspecies)
  read(nowunit,*) text,(spar(i)%pss,i=1,nspecies)
  read(nowunit,*) text,(spar(i)%psr,i=1,nspecies)
  read(nowunit,*) text,(spar(i)%pcnr,i=1,nspecies)
  read(nowunit,*) text,(spar(i)%ncon_fol,i=1,nspecies)
  read(nowunit,*) text,(spar(i)%ncon_frt,i=1,nspecies)
  read(nowunit,*) text,(spar(i)%ncon_crt,i=1,nspecies)
  read(nowunit,*) text,(spar(i)%ncon_tbc,i=1,nspecies)
  read(nowunit,*) text,(spar(i)%ncon_stem,i=1,nspecies)
  read(nowunit,*) text,(spar(i)%reallo_fol,i=1,nspecies)
  read(nowunit,*) text,(spar(i)%reallo_frt,i=1,nspecies)
  read(nowunit,*) text,(spar(i)%alphac,i=1,nspecies)
  read(nowunit,*) text,(spar(i)%cr_frac,i=1,nspecies)
  read(nowunit,*) text,(spar(i)%prhos,i=1,nspecies)
  read(nowunit,*) text,(spar(i)%pnus,i=1,nspecies)
  read(nowunit,*) text,(spar(i)%pha,i=1,nspecies)
  read(nowunit,*) text,(spar(i)%pha_coeff1,i=1,nspecies)
  read(nowunit,*) text,(spar(i)%pha_coeff2,i=1,nspecies)
  read(nowunit,*) text,(spar(i)%pha_v1,i=1,nspecies)
  read(nowunit,*) text,(spar(i)%pha_v2,i=1,nspecies)
  read(nowunit,*) text,(spar(i)%pha_v3,i=1,nspecies)
  read(nowunit,*) text,(spar(i)%crown_a,i=1,nspecies)
  read(nowunit,*) text,(spar(i)%crown_b,i=1,nspecies)
  read(nowunit,*) text,(spar(i)%crown_c,i=1,nspecies)
  read(nowunit,*) text,(spar(i)%psla_min,i=1,nspecies)
  read(nowunit,*) text,(spar(i)%psla_a,i=1,nspecies)
  read(nowunit,*) text,(spar(i)%phic,i=1,nspecies)
  read(nowunit,*) text,(spar(i)%pnc,i=1,nspecies)
  read(nowunit,*) text,(spar(i)%kCO2_25,i=1,nspecies)
  read(nowunit,*) text,(spar(i)%kO2_25,i=1,nspecies)
  read(nowunit,*) text,(spar(i)%pc_25,i=1,nspecies)
  read(nowunit,*) text,(spar(i)%q10_kCO2,i=1,nspecies)
  read(nowunit,*) text,(spar(i)%q10_kO2,i=1,nspecies)
  read(nowunit,*) text,(spar(i)%q10_pc,i=1,nspecies)
  read(nowunit,*) text,(spar(i)%pb,i=1,nspecies)
  read(nowunit,*) text,(spar(i)%PItmin,i=1,nspecies)
  read(nowunit,*) text,(spar(i)%PItopt,i=1,nspecies)
  read(nowunit,*) text,(spar(i)%PItmax,i=1,nspecies)
  read(nowunit,*) text,(spar(i)%PIa,i=1,nspecies)
  read(nowunit,*) text,(spar(i)%PPtmin,i=1,nspecies)
  read(nowunit,*) text,(spar(i)%PPtopt,i=1,nspecies)
  read(nowunit,*) text,(spar(i)%PPtmax,i=1,nspecies)
  read(nowunit,*) text,(spar(i)%PPa,i=1,nspecies)
  read(nowunit,*) text,(spar(i)%PPb,i=1,nspecies)
  read(nowunit,*) text,(spar(i)%CSTbC,i=1,nspecies)
  read(nowunit,*) text,(spar(i)%CSTbT,i=1,nspecies)
  read(nowunit,*) text,(spar(i)%CSa,i=1,nspecies)
  read(nowunit,*) text,(spar(i)%CSb,i=1,nspecies)
  read(nowunit,*) text,(spar(i)%LTbT,i=1,nspecies)
  read(nowunit,*) text,(spar(i)%LTcrit,i=1,nspecies)
  read(nowunit,*) text,(spar(i)%Lstart,i=1,nspecies)
  read(nowunit,*) text,(spar(i)%Phmodel,i=1,nspecies)
  read(nowunit,*) text,(spar(i)%end_bb,i=1,nspecies)
  read(nowunit,*) text,(spar(i)%fpar_mod,i=1,nspecies)
  read(nowunit,*) text,(spar(i)%ceppot_spec,i=1,nspecies)
  read(nowunit,*) text,(spar(i)%Nresp,i=1,nspecies)
  read(nowunit,*) text,(spar(i)%regflag, i=1,nspecies)
  read(nowunit,*) text,(spar(i)%seedrate, i=1,nspecies)
  read(nowunit,*) text,(spar(i)%seedmass, i=1,nspecies)
  read(nowunit,*) text,(spar(i)%seedsd, i=1,nspecies)
  read(nowunit,*) text,(spar(i)%seeda, i=1,nspecies)
  read(nowunit,*) text,(spar(i)%seedb, i=1,nspecies)
  read(nowunit,*) text,(spar(i)%pheight1, i=1,nspecies)
  read(nowunit,*) text,(spar(i)%pheight2, i=1,nspecies)
  read(nowunit,*) text,(spar(i)%pheight3, i=1,nspecies)
  read(nowunit,*) text,(spar(i)%pdiam1, i=1,nspecies)
  read(nowunit,*) text,(spar(i)%pdiam2, i=1,nspecies)
  read(nowunit,*) text,(spar(i)%pdiam3, i=1,nspecies)
  read(nowunit,*) text,(spar(i)%k_opm_fol , i=1,nspecies)
  read(nowunit,*) text,(spar(i)%k_syn_fol , i=1,nspecies)
  read(nowunit,*) text,(spar(i)%k_opm_frt , i=1,nspecies)
  read(nowunit,*) text,(spar(i)%k_syn_frt , i=1,nspecies)
  read(nowunit,*) text,(spar(i)%k_opm_crt , i=1,nspecies)
  read(nowunit,*) text,(spar(i)%k_syn_crt , i=1,nspecies)
  read(nowunit,*) text,(spar(i)%k_opm_tb  , i=1,nspecies)
  read(nowunit,*) text,(spar(i)%k_syn_tb  , i=1,nspecies)
  read(nowunit,*) text,(spar(i)%k_opm_stem, i=1,nspecies)
  read(nowunit,*) text,(spar(i)%k_syn_stem, i=1,nspecies)
  read(nowunit,*) text,(spar(i)%spec_rl, i=1,nspecies)
  read(nowunit,*) text,(spar(i)%tbase, i=1,nspecies)
  read(nowunit,*) text,(spar(i)%topt, i=1,nspecies)
  read(nowunit,*) text,(spar(i)%bdmax_coef, i=1,nspecies)
  read(nowunit,*) text,(spar(i)%porcrit_coef, i=1,nspecies)
  read(nowunit,*) text,(spar(i)%ph_opt_max, i=1,nspecies)
  read(nowunit,*) text,(spar(i)%ph_opt_min, i=1,nspecies)
  read(nowunit,*) text,(spar(i)%ph_max, i=1,nspecies)
  read(nowunit,*) text,(spar(i)%ph_min, i=1,nspecies)
  read(nowunit,*) text,(spar(i)%v_growth, i=1,nspecies)

ios = 0
call errorfile (specfile(ip), ios, nowunit)

do i=1,nspecies
   spar(i)%cnr_fol  = cpart / (spar(i)%ncon_fol  / 1000.)
   spar(i)%cnr_frt  = cpart / (spar(i)%ncon_frt  / 1000.)
   spar(i)%cnr_crt  = cpart / (spar(i)%ncon_crt  / 1000.)
   spar(i)%cnr_tbc  = cpart / (spar(i)%ncon_tbc  / 1000.)
   spar(i)%cnr_stem = cpart / (spar(i)%ncon_stem / 1000.)
enddo

close(nowunit)

end subroutine  readspec
!------------------------------------------------------------------------



