!*****************************************************************!
!*                                                               *!
!*              4C (FORESEE) Simulation Model                    *!
!*                                                               *!
!*                                                               *!
!*        data module for various output files (Header ,...)     *!
!*                                                               *!
!*                  Copyright (C) 1996-2018                      *!
!*     Potsdam Institute for Climate Impact Reserach (PIK)       *!
!*          Authors and contributors see AUTHOR file             *!
!*  This file is part of 4C and is licensed under BSD-2-Clause   *!
!*                   See LICENSE file or under:                  *!
!*     http://www.https://opensource.org/licenses/BSD-2-Clause   *!
!*                           Contact:                            *!
!*       https://gitlab.pik-potsdam.de/foresee/4C                *!
!*                                                               *!
!*****************************************************************!

module data_out

! definition of output form each output type (kind_name) with 4 DATA statements
! character strings with more than 1 row must be separated only by &
! Attention!  Blanks are normally significant, but problematic:
!             at the beginning of the row only one blank is significant
!
! Recipe for new output files:
!    add 1 to the dimension field "(type (out_struct),dimension(x+1),target :: out??)"
!    a n d  to the number of files "out??_n"+1
!    add the specifier of output file to DATA kind_name
!    add the comments on first and second line to the respective DATA statements
!    add the column header to DATA header (pay attention to the above remarks regarding blanks!)
!    add the write statements to the case construct with the kind_name (in output.f)
!    depending on the output structure special open statements might have to be added
!    in OLD_OUT in output.f

! data structure of skalar and field output
type out_struct

   character (10)  :: kind_name   ! specifies the kind and the name of the output file
   integer         :: unit_nr     ! output unit, set in output.f
   integer         :: out_flag    ! output flag
   character (200) :: f_line      ! first comment line
   character (500) :: s_line      ! second comment line
   character (900) :: header      ! header of output columns

end type out_struct

! daily output of scalars and fields
type (out_struct),dimension(24),target :: outd   ! daily output files
integer       ::    outd_n = 24   ! number of all declared daily output files

DATA outd%kind_name /'Cday','Chumd','Copmd','Copmfractd','Cbcd', 'day', 'day_short','NH4','NH4c','NO3','NO3c','Nhumd','Nopmd', &
                     'NOPMfract', 'Nuptd', 'Nmind', 'perc', 'specd', 'temp', 'wat_potent', 'wat_res', 'water', 'watvol', 'wupt'/

DATA outd%f_line /'#   Daily C balance', &                                     ! Cday   
                  '#   C content of humus (hum) per layer', &                  ! Chumd
                  '#   C content of organic primary matter (OPM) per layer', & ! Copmd
                  '#   C content of organic primary matter (OPM) fractions', & ! Copmfractd
                  '#   C content of biochar per layer', &                      ! Cbcd
                  '#   Daily output', &                                        ! day
                  '#   Short daily output', &                                  ! day_short
                  '#   NH4 content per layer',  &                              ! NH4
                  '#   NH4 concentration per layer',  &                        ! NH4c
                  '#   NO3 content per layer',  &                              ! NO3
                  '#   NO3 concentration per layer',  &                        ! NO3c
                  '#   N content of humus (hum) per layer', &                  ! Nhumd
                  '#   N content of organic primary matter (OPM) per layer', & ! Nopmd
                  '#   N content of organic primary matter (OPM) fractions', & ! NOPMfract
                  '#   Daily nitrogen uptake by roots per layer  Nupt', &      ! Nuptd
                  '#   Daily nitrogen mineralisation per layer  Nmin', &       ! Nmind
                  '#   Daily percolation of water per layer   perc', &         ! perc
                  '#   Daily species variables   svar', &                      ! specd
                  '#   Daily soil temperature per layer   temps', &            ! temp
                  '#   Daily soil water potential per layer   wat_potential',& ! wat_potent
                  '#   Daily water uptake resistance per layer   wat_res', &   ! wat_res
                  '#   Daily soil water content per layer   wats', &           ! water
                  '#   Daily soil water content per layer   watvol', &         ! watvol
                  '#   Daily water uptake by roots per layer  wupt_r'/         ! wupt

DATA outd%s_line /  &
'#                  gC/m2       gC/m2       gC/m2       gC/m2       gC/m2       gC/m2       gC/m2       gC/m2       gC/m2       gC/m2&
            &       gC/m2       gC/m2       gC/m2      %' , &                     ! Cday
'#   gC_m2', &                                                                    ! Chumd
'#   gC_m2', &                                                                    ! Copmd
'#   gC/m2',&                                                                     ! Copmfractd
'#   gC_m2', &                                                                    ! Cbcd
'#              Grad C     J/cm2        mm        mm        mm        mm        mm        mm        mm        mm&
          &     mol/m2     gC/m2     gN/m2     gN/m2     gN/m2     gN/m2    mgN/m2                              &
          &                                                                 mm        mm                  �C&
                                  J/cm2     J/cm2', &                                    ! day
'#                -          mm', &                                               ! day_short
'#   gN/m2', &                                                                    ! NH4
'#   mgN/l', &                                                                    ! NH4c
'#   gN/m2', &                                                                    ! NO3
'#   mgN/l', &                                                                    ! NO3c
'#   gN/m2', &                                                                    ! Nhumd
'#   gN/m2', &                                                                    ! Nopmd
'#   gN/m2  |-------------   Fagus sylvatica   ----------------|---------------   Picea abies   -----------------|&
            &------------   Pinus sylvestris   ----------------|---------------  Quercus robur   ----------------|&
            &-------------   Betula pendula   -----------------|--------------   Pinus contorta   ---------------|&
            &-------------  Pinus ponderosa   -----------------|--------------   Populus tremula  ---------------|&
            &-------------   Bodenvegetation   ----------------|',  &             ! NOPMfract
'#   gN/m2', &                                                                    ! Nuptd
'#   gN/m2', &                                                                    ! Nmind
'#   mm/day', &                                                                   ! perc
'#   ', &                                                                         ! specd
'#   �C', &                                                                       ! temp
'#   hPa', &                                                                      ! wat_potent
'#   ', &                                                                         ! wat_res
'#   mm', &                                                                       ! water
'#   vol%', &                                                                     ! watvol
'#   mm/day'/                                                                     ! wupt

DATA outd%header /  &
'#   Day Year  gross_Phot   gross_Ass     net_Ass     pot_NPP         NPP     NPP_day     GPP_day         NEE &
             &    TER_day     autresp    Resp_aut    Resp_het    Resp_fol  FaPar',&   ! Cday
'#   Day Year    Chum_1    Chum_2    Chum_3    Chum_4    Chum_5    Chum_6     ....',& ! Chumd
'#   Day Year    Copm_1    Copm_2    Copm_3    Copm_4    Copm_5    Copm_6     ....',& ! Copmd
'#   Day Year species C_opm_fol  C_opm_tb C_opm_frt C_opm_crt C_opm_stm species C_opm_fol  C_opm_tb C_opm_frt C_opm_crt C_opm_stm &
            &    ....',&                                                              ! Copmfractd
'#   Day Year     Cbc_1     Cbc_2     Cbc_3     Cbc_4     Cbc_5     Cbc_6     ....',& ! Cbcd
'#   Day Year      Temp       Rad      Prec  Intercep      Snow       PET       AET  Transdem Transtree Transsveg&
            &    GP_can  Resp_het  Nleach_d    Nupt_d  Nmin_d_c   N_antot    N_Depo     Cover&
            &       LAI   s_Light toFPARcan fire_indi fire_e fire_w fire_n snowday  drIndd&
            &  buckroot   buck100 cl_WatBal dewp.temp  dew/rime  Rnet_tot   Rad_max',&         ! day
'#       Date  fire_e cl_WatBal',&                                                    ! day_short
'#   Day Year     NH4_1     NH4_2     NH4_3     NH4_4     NH4_5     NH4_6     ....',& ! NH4
'#   Day Year     NH4_1     NH4_2     NH4_3     NH4_4     NH4_5     NH4_6     ....',& ! NH4c
'#   Day Year     NO3_1     NO3_2     NO3_3     NO3_4     NO3_5     NO3_6     ....',& ! NO3
'#   Day Year     NO3_1     NO3_2     NO3_3     NO3_4     NO3_5     NO3_6     ....',& ! NO3c
'#   Day Year    Nhum_1    Nhum_2    Nhum_3    Nhum_4    Nhum_5    Nhum_6     ....',& ! Nhumd
'#   Day Year    Nopm_1    Nopm_2    Nopm_3    Nopm_4    Nopm_5    Nopm_6     ....',& ! Nopmd
'#   Day Year N_opm_fol  N_opm_tb N_opm_frt N_opm_crt N_opm_stm N_opm_fol  N_opm_tb N_opm_frt N_opm_crt N_opm_stm &
            & N_opm_fol  N_opm_tb N_opm_frt N_opm_crt N_opm_stm N_opm_fol  N_opm_tb N_opm_frt N_opm_crt N_opm_stm &
            & N_opm_fol  N_opm_tb N_opm_frt N_opm_crt N_opm_stm N_opm_fol  N_opm_tb N_opm_frt N_opm_crt N_opm_stm &
            & N_opm_fol  N_opm_tb N_opm_frt N_opm_crt N_opm_stm ',&                   ! NOPMfract
'#   Day Year    Nupt_1    Nupt_2    Nupt_3    Nupt_4    Nupt_5    Nupt_6     ....',& ! Nuptd
'#   Day Year    Nmin_1    Nmin_2    Nmin_3    Nmin_4    Nmin_5    Nmin_6     ....',& ! Nmind
'#   Day Year  Percol_1  Percol_2  Percol_3  Percol_4  Percol_5  Percol_6     ....',& ! perc
'#   Day Year     species_name  number        Ndem        Nupt       Ndemp       Nuptp      RedN  ',& ! specd
'#   Day Year Temp_surf   Temps_1   Temps_2   Temps_3   Temps_4   Temps_5   Temps_6     ....',& ! temp
'#   Day Year    Pot_1    Pot_2    Pot_3    Pot_4    Pot_5    Pot_6     ....',&       ! wat_potent
'#   Day Year Wat_res_1 Wat_res_2 Wat_res_3 Wat_res_4 Wat_res_5 Wat_res_6     ....',& ! wat_res
'#   Day Year    Wats_1    Wats_2    Wats_3    Wats_4    Wats_5    Wats_6     ....',& ! water
'#   Day Year    Wats_1    Wats_2    Wats_3    Wats_4    Wats_5    Wats_6     ....',& ! watvol
'#   Day Year  Wupt_r_1  Wupt_r_2  Wupt_r_3  Wupt_r_4  Wupt_r_5  Wupt_r_6     ....'/  ! wupt

!   ----------------------------------------------------- !
 
! yearly output of scalars and fields
type (out_struct),dimension(58),target :: outy   ! yearly output files
integer       ::    outy_n = 58   ! number of all declared yearly output files

DATA outy%kind_name /'AET_mon','c_bal','Cbc','Chum','Copm','Copmfract','classd','classage','classmvol','classd_h','classdm', 'classdm_h',&
                     'classh', 'classt', 'clim', 'clim_temp', 'clim_prec', 'clim_rad', 'clim_hum', &
                     'fcap_av','fcapv_av', 'fr_loss','GPP_mon', 'humusv', 'indi',  &
                     'litter','Nbc','Nhum','Nopm','NEE_mon','NPP_mon','manrec', 'mansort', 'redis', 'root', 'sdrought',&
                     'soil', 'spec', 'standsort','TER_mon','veg', 'veg_in', 'veg_out', &
                     'veg_be','veg_bi','veg_pi', 'veg_pc', 'veg_pp', 'veg_pt', &
                     'veg_oa','veg_sp','veg_ph', 'veg_dg', 'veg_rb', 'veg_egl', 'veg_egr','veg_sveg','veg_mist'/
 
DATA outy%f_line /'#  Monthly sum of actual evapotranspiration (AET)', &       ! AET_mon
                  '#  Yearly C-Balance, C-stocks and -fluxes; C_sumvsab is part of C_biomass', &  ! c_bal
                  '#   C content of biochar (C_bc) per layer', &               ! Cbc
                  '#   C content of humus (hum) per layer', &                  ! Chum
                  '#   C content of organic primary matter (OPM) per layer', & ! Copm
                  '#   C content of organic primary matter (OPM) fractions', & ! Copmfract
                  '#', &                                                       ! classd
                  '#', &                                                       ! classage
                  '#', &                                                       ! classmvol
                  '#', &                                                       ! classd_h
                  '#', &                                                       ! classdm
                  '#', &                                                       ! classdm_h
                  '#', &                                                       ! classh
                  '#', &                                                       ! classt
                  '#   Climate data', &                                        ! clim
                  '#   Air temperature: monthly climate data', &               ! clim_temp
                  '#   Precipitation: monthly climate data', &                 ! clim_prec
                  '#   Radiation: monthly climate data', &                     ! clim_rad
                  '#   Relative humidity: monthly climate data', &             ! clim_hum
                  '#   Available field capacity per layer', &                  ! fcap_av
                  '#   Available field capacity per layer', &                  ! fcapv_av
                  '#   Percentage fine root C-loss per soil layer',  &         ! fr_loss
                  '#   Monthly GPP of all cohorts and species', &              ! GPP_mon
                  '#   Content of humus per layer', &                          ! humusv
				  '#   Indices of fire and biodiversity', &                    ! indi
                  '#   Yearly litter fractions', &                             ! litter
                  '#   N content of biochar (N_bc) per layer', &               ! Nbc
                  '#   N content of humus (hum) per layer', &                  ! Nhum
                  '#   N content of organic primary matter (OPM) per layer', & ! Nopm
                  '#   Monthly NEE of all cohorts and species', &              ! NEE_mon
                  '#   Monthly NPP of all cohorts and species', &              ! NPP_mon
                  '#   Management record', &                                   ! manrec
                  '#   Management sortiment',&                                 ! mansort
                  '#   Redistribution of root C (redis)',  &                   ! redis
				  '#   Root distribution (root_fr)',  &                        ! root
                  '#   Data from soil model',  &                               ! sdrought
                  '#   Data from soil model',  &                               ! soil
                  '#   Species number and name',  &                            ! spec
                  '#   sortiment of whole stand (without harvested trees)',&   ! standsort
                  '#   Monthly TER of all cohorts and species', &              ! TER_mon
                  '#   Values for the whole stand (per ha); see files veg_in, veg_out in addition', &   ! veg
                  '#   New trees (by planting or regeneration), values for the whole stand (per ha)', &  ! veg_in
                  '#   Removed trees (by mortality or management) with number of cohorts from which trees are removed (per ha)', &  ! veg_out
                  '#   Values for the whole stand (per ha) for beech', &             ! veg_be
                  '#   Values for the whole stand (per ha) for birch', &             ! veg_bi
                  '#   Values for the whole stand (per ha) for pinus sylvestris', &  ! veg_pi
                  '#   Values for the whole stand (per ha) for pinus contorta', &    ! veg_pc
                  '#   Values for the whole stand (per ha) for pinus ponderosa', &   ! veg_pp
                  '#   Values for the whole stand (per ha) for populus tremula', &  ! veg_pt
                  '#   Values for the whole stand (per ha) for oak', &               ! veg_oa
                  '#   Values for the whole stand (per ha) for spruce', &            ! veg_sp
				  '#   Values for the whole stand (per ha) for pinus halepensis', &  ! veg_ph
                  '#   Values for the whole stand (per ha) for douglas fir', &       ! veg_dg
                  '#   Values for the whole stand (per ha) for black locust', &      ! veg_rb
                  '#   Values for the whole stand (per ha) for E.globulus', &        ! veg_egl
                  '#   Values for the whole stand (per ha) for E.grandis', &         ! veg_egr
                  '#   Values for the whole stand (per ha) for ground vegetation', & ! veg_sveg
                  '#   Values for the whole stand (per ha) for mistletoe (Visc. a.)'/! veg_mist

DATA outy%s_line /  &
'# mm', &						                                                     ! AET_mon
'#          kg/ha     kg/ha     kg/ha     kg/ha     kg/ha     kg/ha     kg/ha     kg/ha&
&     kg/ha     kg/ha      t/ha      t/ha      t/ha      t/ha      t/ha      t/ha      t/ha      t/ha      t/ha&
&    mol/m2    mol/m2    mol/m2    mol/m2    mol/m2    mol/m2    mol/m2    mol/m2&
&    mol/m2    mol/m2    mol/m2     kg/ha', &                                                  ! c_bal
'#   gC/m2', &                                                                       ! Cbc
'#   gC/m2', &                                                                       ! Chum
'#   gC/m2', &                                                                       ! Copm
'#   gC/m2', &                                                                       ! Copmfract
'# diam_class: Number of trees (per ha) in diameter classes, step 5 cm',  &               ! classd
'# diam_class: Mean age of trees (per ha) in diamter classes, step 5 cm',  &              ! classage
'# diam_class: Mean volume (m�/ha) of harvested trees in diamter classes, step 5 cm',  &  ! classmvol
'# diam_class: Mean height of trees in diameter classes, step 5 cm',  &                   ! classd_h
'# diam_class: Number of harvested trees (per ha) in diameter classes, step 5 cm',  &     ! classdm
'# diam_class: Mean height of trees in diameter classes, step 5 cm',  &              ! classdm_h
'# height_class: Number of trees in height classes, bis 1,5,6,7,...,50,55,>55m',  &  ! classh
'# diam_class: Number of dead trees (per ha) in diameter classes, step 5 cm',     &  ! classt
'#         �C           mm     J/cm2       m/s       ppm      �C',  &                ! clim
'#         �C ...          ',  &                                                     ! clim_temp
'#         mm ...          ',  &                                                     ! clim_prec
'#         J/cm2 ...       ',  &                                                     ! clim_rad
'#         % ...           ',  &                                                     ! clim_hum
'# mm', &						                                                     ! fcap_av
'# %', &						                                                     ! fcapv_av
'# yearly mean fine root C-loss', &						                             ! fr_loss
'#   gC/m2', &                                                                       ! GPP_mon
'# %', &						                                                     ! humusv
'#    fire index |------------   fire index west    ------------|&
&|-----------------------   fire index east   -----------------------|&
&|-------   fire index Nesterov   -------|',  &                                      ! indi
'#       |--------------------------------   Dry mass   kg DW/ha_yr   ---------------------------------|&
       & |-----------------   Carbon content   kg C/ha_yr   -------------------|&
       & |-----------------   Nitrogen content   kg N/ha_yr   -----------------|', & ! litter
!&         %          %          %           %  ', &                                   ! litter
'#   gN/m2', &                                                                       ! Nbc
'#   gN/m2', &                                                                       ! Nhum
'#   gN/m2', &                                                                       ! Nopm
'#   gC/m2', &                                                                       ! NEE_mon
'#   gC/m2', &                                                                       ! NPP_mon
'  ',                                    &                                           ! manrec
'                           cm       cm      cm     cm      cm    m�/ha    kg C/ha ', &          ! mansort
'#  relative share of redistributed C per layer (whole stand) ', &                   ! redis
'#  relative share of root mass per layer (whole stand) ', &                         ! root
'# s_drought: Number of days with water content near wilting point (drought days) per layer',  &  ! sdrought
'#         Grad_C        mm        mm        mm        mm        mm        mm        mm        mm        mm        mm    mol_m2     gN_m2&
&     gN_m2     gC_m2     gN_m2     gN_m2     gC_m2     gN_m2     gC_m2     gN_m2     gC_m2     gC_m2     gC_m2     gC_m2     gC_m2     gC_m2&
&     gN_m2     gN_m2     gN_m2     gC_m2        mm        mm        cm        mm     J/cm2     gN_m2     gC_m2     gC_m2',&   ! soil
'#', &                                                                                ! spec
'                           cm       cm      cm     cm      cm    m�/ha    kg C/ha ', &             ! standsort
'#   gC/m2', &                                                                       ! TER_mon
'#                               /ha      m2_m2    kg_DW/ha kg_DW_yr/ha          cm          cm&
&    kg_DW/ha    kg_DW/ha    kg_DW/ha    kg_DW/ha    kg_DW/ha       m3/ha    kg_DW/ha    kg_DW/ha       m2_m2&
&                   gN/m2      mol/m2      mol/m2      mol/m2          cm          cm      m�         m�/ha       m�/ha', &                                   ! veg
2*'#                               /ha      m2_m2    kg_DW/ha          cm          cm&
&    kg_DW/ha    kg_DW/ha    kg_DW/ha    kg_DW/ha    kg_DW/ha       m3/ha    kg_DW/ha    kg_DW/ha       m2_m2&
&                   gN/m2      mol/m2      mol/m2      mol/m2', &                      ! veg_in, veg_out
15*'#                               /ha      m2_m2    kg_DW/ha kg_DW_yr/ha          cm          &
&cm    kg_DW/ha    kg_DW/ha    kg_DW/ha    kg_DW/ha    kg_DW/ha       m3/ha    kg_DW/ha    kg_DW/ha       m2_m2&
&                    gN/m2       gN/m2         -       -               cm          cm      m�         m�/ha        m�/ha       mm'/                        ! veg_be, bi, pi, oa, sp, sveg

DATA outy%header /  &
'# Year     AET_1     AET_2     AET_3     AET_4     AET_5     AET_6     AET_7     AET_8     AET_9    AET_10&
      &    AET_11    AET_12 AET_Quar1 AET_Quar2 AET_Quar3 AET_Quar4   AET_DJF   AET_MAM   AET_JJA   AET_SON', &   ! AET_mon
'# Year       GPP       NPP       NEP  Aut_Resp  Het_Resp  Tot_Resp C_dead_st C_sumvsab C_biomass&
&  C_tot_ES    C_soil   C_tot_1   C_hum_1  C_tot_40  C_hum_40  C_tot_80  C_hum_80 C_tot_100 C_hum_100&
&       GPP       NPP       NEP  Aut_Resp  Het_Resp  Tot_Resp C_dead_st&
& C_sumvsab C_biomass  C_tot_ES    C_soil    gppsum', &                                    ! c_bal
'# Year     Cbc_1     Cbc_2     Cbc_3     Cbc_4     Cbc_5     Cbc_6     ....',&  ! Cbc
'# Year    Chum_1    Chum_2    Chum_3    Chum_4    Chum_5    Chum_6     ....',&  ! Chum
'# Year    Copm_1    Copm_2    Copm_3    Copm_4    Copm_5    Copm_6     ....',&  ! Copm
'# Year species C_opm_fol  C_opm_tb C_opm_frt C_opm_crt C_opm_stm species C_opm_fol  C_opm_tb C_opm_frt C_opm_crt C_opm_stm &
            &    ....',&                                                              ! Copmfract
'# Year',  &                                                                     ! classd
'# Year',  &                                                                     ! classage
'# Year',  &                                                                     ! classmvol
'# Year',  &                                                                     ! classd_h
'# Year',  &                                                                     ! classdm
'# Year',  &                                                                     ! classdm_h
'# Year',  &                                                                     ! classh
'# Year',  &                                                                     ! classt
'# Year     Temp      Prec Radiation      Wind       CO2       GDD summerdays  hotdays   icedays  &
& drydays hraindays  snowdays  Ind_arid    CWB     Ind_Lang  Ind_Cout Ind_Wissm  Ind_Mart Ind_Mart_VP  &
&Ind_Emb Ind_Weck Ind_Reich   Ind_Gor  I_Currey  I_Conrad  NTIndex   Ind_Budyko F_day F_day_sp l_frost l_frosttot  anzfd  sumtfd iday_vp  Ind_SHC',  &       ! clim
'# Year    Temp_1    Temp_2    Temp_3    Temp_4    Temp_5    Temp_6    Temp_7    Temp_8    Temp_9   Temp_10&
      &   Temp_11   Temp_12  T_Quart1  T_Quart2  T_Quart3  T_Quart4     T_DJF     T_MAM     T_JJA     T_SON', &   ! clim_temp
'# Year    Prec_1    Prec_2    Prec_3    Prec_4    Prec_5    Prec_6    Prec_7    Prec_8    Prec_9   Prec_10&
      &   Prec_11   Prec_12  P_Quart1  P_Quart2  P_Quart3  P_Quart4     P_DJF     P_MAM     P_JJA     P_SON', &   ! clim_prec
'# Year     Rad_1     Rad_2     Rad_3     Rad_4     Rad_5     Rad_6     Rad_7     Rad_8     Rad_9    Rad_10&
      &    Rad_11    Rad_12  R_Quart1  R_Quart2  R_Quart3  R_Quart4     R_DJF     R_MAM     R_JJA     R_SON', &   ! clim_rad
'# Year     Hum_1     Hum_2     Hum_3     Hum_4     Hum_5     Hum_6     Hum_7     Hum_8     Hum_9    Hum_10&
      &    Hum_11    Hum_12  H_Quart1  H_Quart2  H_Quart3  H_Quart4     H_DJF     H_MAM     H_JJA     H_SON', &   ! clim_hum
'# Year fcap_av_1 fcap_av_2 fcap_av_3 fcap_av_4 fcap_av_5 fcap_av_6     ....',&  ! fcap_av
'# Year fcapvav_1 fcapvav_2 fcapvav_3 fcapvav_4 fcapvav_5 fcapvav_6     ....',&  ! fcapv_av
'# Year    lay_1    lay_2    lay_3    lay_4    lay_5    lay_6     ....',&        ! fr_loss
'# Year     GPP_1     GPP_2     GPP_3     GPP_4     GPP_5     GPP_6     GPP_7     GPP_8     GPP_9    GPP_10&
      &    GPP_11    GPP_12 GPP_Quar1 GPP_Quar2 GPP_Quar3 GPP_Quar4   GPP_DJF   GPP_MAM   GPP_JJA   GPP_SON', &   ! GPP_mon
'# Year   humus_1   humus_2   humus_3   humus_4   humus_5   humus_6     ....',&  ! humusv
'#      Bruschek    Mean  class1  class2  class3  class4  class5&
&    Mean  class1  class2  class3  class4  class5   Ind_max   Ind_day    Mean  class1  class2  class3  class4',  &  ! indi
'# Year  fol_litter  fol_lit_tr  frt_litter  frt_lit_tr  crt_litter   tb_litter stem_litter  tot_litter&
      &  fol_litter  frt_litter  crt_litter   tb_litter stem_litter  tot_litter&
      &  fol_litter  frt_litter  crt_litter   tb_litter stem_litter  tot_litter', &       ! litter
!'# Year  fol_litter  frt_litter  crt_litter   tb_litter stem_litter  fol_litter  frt_litter&
!&  crt_litter   tb_litter stem_litter', &                                                    ! litter
'# Year     Nbc_1     Nbc_2     Nbc_3     Nbc_4     Nbc_5     Nbc_6     ....',&  ! Nbc
'# Year    Nhum_1    Nhum_2    Nhum_3    Nhum_4    Nhum_5    Nhum_6     ....',&  ! Nhum
'# Year    Nopm_1    Nopm_2    Nopm_3    Nopm_4    Nopm_5    Nopm_6     ....',&  ! Nopm
'# Year     NEE_1     NEE_2     NEE_3     NEE_4     NEE_5     NEE_6     NEE_7     NEE_8     NEE_9    NEE_10&
      &    NEE_11    NEE_12 NEE_Quar1 NEE_Quar2 NEE_Quar3 NEE_Quar4   NEE_DJF   NEE_MAM   NEE_JJA   NEE_SON', &   ! NEE_mon
'# Year     NPP_1     NPP_2     NPP_3     NPP_4     NPP_5     NPP_6     NPP_7     NPP_8     NPP_9    NPP_10&
      &    NPP_11    NPP_12 NPP_Quar1 NPP_Quar2 NPP_Quar3 NPP_Quar4   NPP_DJF   NPP_MAM   NPP_JJA   NPP_SON', &   ! NPP_mon
'# Year         management                      measure            ',  &         ! manrec
'# year   count  spec type  len     diam  diam wob  top_d  t_d wob  Volume      DW        number  type', & ! mansort
'# Year    lay_1    lay_2    lay_3    lay_4    lay_5    lay_6     ....',&  ! redis
'# Year    root_1    root_2    root_3    root_4    root_5    root_6     ....',&  ! root
'# Year  layer1  layer2  layer3  layer4  ........',  &                           ! sdrought
'# Year      Temp      Prec    Interc    Percol      Wupt  Wuptroot Transtree Transsveg  Wuptsoil       AET  Wats_tot&
      &    GP_can     N_min     N_tot     C_tot   N_antot  N_humtot  C_humtot  N_hum(1)  C_hum(1)  N_litter&
      &  C_litter C_opm_fol C_opm_frt C_opm_crt C_opm_tbc C_opm_stm      Nupt    Nleach    N_depo Soil_Resp&
      &       PET interc_sv    thick1  dew/rime  Rnet_tot  N_bc_tot  C_bc_tot  C_bc_app', &    ! soil
'# Year', &                                                                          ! spec
' year    count  spec type  len     diam  diam wob  top_d   t_d wob    Volume      DW     number ', & ! standsort
'# Year     TER_1     TER_2     TER_3     TER_4     TER_5     TER_6     TER_7     TER_8     TER_9    TER_10&
      &    TER_11    TER_12 TER_Quar1 TER_Quar2 TER_Quar3 TER_Quar4   TER_DJF   TER_MAM   TER_JJA   TER_SON', &   ! TER_mon
'# Year  num_Spec       Coh      Tree       LAI     Biomass      NPPsum     Meddiam&
 &      Domhei     Fol_Bio     Sap_Bio     Frt_Bio     Hrt_Bio    Stem_inc     Stemvol   rem_stems&
 &  dead_stems       cover     drIndAl        Ndem gp_can_mean  gp_can_min  gp_can_max   mean_diam mean_height   basal_area dead_stems_m3  stem_inc_m3', &  ! veg
2* '# Year  num_Spec       Coh      Tree       LAI     Biomass     Meddiam&
 &      mean_hei     Fol_Bio     Sap_Bio     Frt_Bio     Hrt_Bio    Stem_inc     Stemvol   rem_stems&
 &  dead_stems       cover     drIndAl        Ndem gp_can_mean  gp_can_min  gp_can_max', &  ! veg_in, veg_out
15*'# Year   Spec_id       Coh      Tree       LAI     Biomass      NPPsum     Meddiam&
 &      Domhei     Fol_Bio     Sap_Bio     Frt_Bio     Hrt_Bio    Stem_inc     Stemvol   rem_stems&
 &  dead_stems       cover     drIndAl        Ndem        Nupt       Red_N daybb endbb   mean_diam mean_height   basal_area dead_stems_m3  stem_inc_m3   YRW'/    ! veg_be, bi, pi, oa, sp,lp, sveg, mist

!   ----------------------------------------------------- !

! daily output of cohorts
type (out_struct),dimension(23),target :: outcd
integer       ::    outcd_n = 23   ! number of all declared cohort output files

DATA outcd%kind_name /'ass', 'aevi', 'ddi', 'dem', 'dips', 'gp', 'gsdps', 'intcap', 'interc', &
                      'Ndemc_d', 'Nuptc_d', 'N_fol', 'N_pool', 'RedNc', 'resp', 'respaut', &
                      'respbr', 'respfol', 'resphet', 'respsap', 'respfrt', 'sup', 'totfpar'/

DATA outcd%s_line /23*'#   Cohort output'/               ! ass, ddi, dem, gp, gsdps, res,
                                                         ! resbr, ressap, resfrt, sup

DATA outcd%f_line /  &
'# Optimum gross assimilation rate (kg DW/d)   assi',  &             ! ass
'# Daily evaporation of intercepted water (mm/day)  aev_i',  &       ! aevi
'# Daily drought index   drindd',  &                                 ! ddi
'# Demand for soil water of the cohort (mm/day)   demand',  &        ! dem
'# Drought index for Photosyntheses calculation (cum)   drindps',  & ! dips
'# Unstressed stomatal conductance (mol/m2*d)   gp',  &              ! gp
'# Number of growing season days per time step of photosynthesis   ndaysps', &  ! gsdps
'# Interception capacity (mm)  sum of intcap(layer)',  &             ! intcap
'# Interception storage (mm)  interc_st',  &                         ! interc
'# Daily N demand per tree (g)', &                                   ! Ndemc_d
'# Daily N uptake per tree (g)', &                                   ! Nuptc_d
'# Daily N content of foliage per tree (g)', &                       ! N_fol
'# Daily N_pool per tree (g)', &                                     ! N_pool
'# Daily photosynthesis nitrogen reduction factor [-]', &            ! RedNc
'# Leaf respiration rate (g C/d)   resp',  &                         ! resp
'# Daily autotrophic respiration rate (g C/d)   respaut',  &         ! respaut
'# Daily respiration rate of branches (g C/d)   respbr',  &          ! respbr
'# Daily respiration rate of leaves (g C/d)   respfol',  &           ! respfol
'# Daily heterotrophic respiration rate (g C/d)   resphet',  &       ! resphet
'# Daily respiration rate of sapwood (g C/d)   respsap',  &          ! respsap
'# Daily respiration rate of frt (g C/d)   respfrt',  &              ! respfrt
'# Supply of soil water to roots of the cohort (mm/day)   supply', & ! sup
'# Total fraction of PAR absorbed per m� patch area (-)   totFPAR'/  ! totfpar

DATA outcd%header /  &
23*'# Day Year        Coh1        Coh2        Coh3        Coh4        ...'/ ! ass, ddi, dem, gp, gsdps, res,
                                                                           ! resbr, ressap, resfrt, sup

!   ----------------------------------------------------- !

! yearly output of cohorts
type (out_struct),dimension(58),target :: outcy
integer       ::    outcy_n = 58   ! number of all declared cohort output files

DATA outcy%kind_name /'age', 'ahb', 'ahbasrel', 'ahc', 'ahcasrel', 'asapw', 'atr', 'bioi', 'botlayer','cpa', 'crt', 'daybb', 'dcrb', 'diac', 'diam', &
                      'dtr', 'dwd','fol', 'foli', 'frt', 'frti', 'frtrel', 'frtrelc', 'geff', 'gfol', 'gfrt', 'grossass', 'gsap', &
                      'gsd', 'hbo', 'hea', 'hei', 'hrt', 'leaf', 'maintres', 'nas', 'npp', 'rdpt', 'rld', 'sap', &
                      'sfol', 'sfrt', 'spn', 'ssap', 'stem', 'str', 'tdb','toplayer', 'trman', 'ttb','Ndemc_c','Nuptc_c', &
					  'Nfol', 'Npool', 'Nstr','rooteff', 'watleft', 'yrw'/

DATA outcy%s_line /58*'#   Cohort output'/            ! age, ahb, ahc, atr, asapw, bioi, botLayer, cpa, crt, daybb, dcrb, diac, diam,  
                                                      ! dtr, dwd, fol, foli, frt, frti, frtrel, geff, gfol, gfrt,
                                                      ! grossass, gsap, gsd, hbo, hea, hrt, hei,
                                                      ! leaf, maintres, nas, npp, rdpt, rld, sap, sfol, sfrt, spn,
                                                      ! ssap, stem, str, tdb, topLayer, trman,ttb, Ndemc,Nuptc, rooteff,watleft

DATA outcy%f_line /  &
'# Tree age (year)',  &                               ! age 
'# Cross sectional area of heartwood at stem base [cm**2]  x_Ahb', &   ! ahb
'# Relation of heartwood to sapwood at stem base', &                   ! ahbasrel
'# Cross sectional area of heartwood at crown base [cm**2]  Ahc', &    ! ahc
'# Relation of heartwood to sapwood at crown base', &                  ! ahcasrel
'# Cross sectional area of sapwood in bole space [cm**2]  Asapw', &    ! asapw
'# Number of alive trees per cohort', &               ! atr
'# Net biomass increment (kg DM/year)', &             ! bioi
'# Number of bottom layer of crown [-]', &            ! botLayer
'# Cohort crown projection area (m2)', &              ! cpa
'# coarse root biomass (kg DM/tree)', &               ! crt
'# Day of leaf bud burst', &                          ! daybb
'# Diameter of stem at crown base (cm)',&             ! dcrb
'# Drought index for allocation calculation (cum)', & ! diac
'# Diameter at breast height (cm)', &                 ! diam
'# Number of dead trees per cohort', &                ! dtr
'# Stem biomass of dead trees per cohort', &          ! dwd
'# Foliage biomass (kg DM/tree)', &                   ! fol
'# Foliage increment (kg DM/year/tree)', &            ! foli
'# Fine root biomass (kg DM/tree)', &                 ! frt
'# Net fine root increment (kg DM/year/tree)', &      ! frti
'# Relative fine root fraction of tree per soil layer (root profile)', &                   ! frtrel
'# Relative fine root fraction of cohort of total layer fine root mass per soil layer', &  ! frtrel
'# Growth efficiency kg/m2', &                        ! geff
'# Gross growth rate foliage (kg DM/year/tree)', &    ! gfol
'# Gross growth rate fine root (kg DM/year/tree)', &  ! gfrt
'# Gross assimilation rate (kg DM/year/tree)', &      ! grossass
'# Gross growth rate sapwood (kg DM/year/tree)', &    ! gsap
'# Number of growing season days per year  ndaysgr',& ! gsd
'# Bole height (cm)', &                               ! hbo
'# Number of years without stress', &                 ! hea
'# Total tree height (cm)', &                         ! hei
'# Heartwood biomass (kg DM/tree)', &                 ! hrt
'# Leaf area per tree (m2)', &                        ! leaf
'# Maintenance respiration (kg DM/year/tree)', &      ! maintres
'# Net foliage assimilation rate (kg DM/year/tree)', & ! nas
'# NPP (kg DM/year/tree)', &                          ! npp
'# Rooting depth calculated with TRAP model[cm]', &   ! rdpt
'# estimated root length density [cm]', &             ! rld
'# Sapwood biomass (kg DM)', &                        ! sap
'# Senescence rate foliage (kg DM/year/tree)', &      ! sfol
'# Senescence rate fine roots (kg DM/year/tree)', &   ! sfrt
'# Species number of the cohort',  &                  ! spn
'# Senescence rate sapwood (kg DM/year/tree)', &      ! ssap
'# Stemwood biomass increment (kg DM/year/tree)', &   ! stem
'# Number of stress years',  &                        ! str
'# Total cohort dead biomass (kg DM/year/cohort)', &  ! tdb
'# Number of top layer of crown [-]', &               ! topLayer
'# Number of trees harvested by managment',   &       ! trman
'# Total tree biomass (kg DM/tree)', &                ! ttb
'# N demand per tree and year (g)', &                 ! Ndemc_c
'# N uptake per tree and year (g)', &                 ! Nuptc_c
'# N content of foliage per tree and year (g)', &     ! Nfol
'# N pool per tree and year (g)', &                   ! Npool
'# Ratio of N uptake to demand per tree and year', &  ! Nstr
'# Root uptake efficiency factor', &                  ! rooteff
'# Water left in next layer', &						  ! watleft
'# Year ring width [mm]' /                             ! yrw

DATA outcy%header /  &
58*'# Year        Coh1        Coh2        Coh3        Coh4        ...'/  !age, ahb, ahc, atr, bioi, cpa, crt, daybb, dcrb, diac, diam, 
                                                      ! dtr, dwd, fol, foli, frt, frti, frtrel, geff, gfol, gfrt,
                                                      ! gsap, gsd, hbo, hea, hrt, hei,
                                                      ! leaf, maintres, nas, npp, rdpt, rld, sap, sfol, sfrt, spn,
                                                      ! stem, str, tdb,trman, ttb, Ndemc,Nuptc, rooteff,watleft, yrw

! output at simulation end 
type (out_struct),dimension(6),target :: oute
integer       ::    oute_n = 6   ! number of all declared end output files

DATA oute%kind_name /'sea', 'sea_ms', 'sea_npv', 'sea_st','wpm', 'wpm_inter'/

DATA oute%f_line /										&
'# SEA: Costs and assets of standing stock, harvested timber, silvicultural costs, fix costs, and subsidies in euro/ha',	&
'# SEA: Timber grading for harvested wood, m3/ha',	&
'# SEA: liquidation value, npv, npv+ in euro/ha',	&
'# SEA: Timber grading for standing stock, m3/ha',	&
'# Wood product model output',							&
'# Wood product model intermediate steps'/               ! 

DATA oute%s_line / &
'#      shotcuts: sum: summe, st: standing stock, ms: harvested wood, fc: fix costs, sv: silvicultural costs, co: costs, as: assets, sub: subsidies, sp: spruce, be: beech, pi: pine, oa: oak, bi: birch, ' ,	&
'# Timber grades 1-7: 1-fue, 2-in, 3-LAS1a, 4-LAS1b, 5-LAS2a, 6-LAS2b, 7-LAS3a, 8-L2b, 9-L3a, 10-L3b ' ,	&
'#      a: without discounting, b-d: interest rate (see "sea_prices.wpm" file) ' ,	&
'# Timber grades 1-7: 1-fue, 2-in, 3-LAS1a, 4-LAS1b, 5-LAS2a, 6-LAS2b, 7-LAS3a, 8-L2b, 9-L3a, 10-L3b ' ,	&
'# Carbon in different products, kg C/ha ' ,	&
'# Carbon in different products, kg C/ha tg: timber grades, il: industrial lines, pl: product lines'/ 
DATA oute%header /  &
'# Year sum_all	  sum_st    sum_ms    sum_sv    sum_fc    sum_sub   be_st_co  sp_st_co  pi_st_co  oa_st_co  bi_st_co |be_st_as  sp_st_as  pi_st_as  oa_st_as  bi_st_as |be_ms_co  sp_ms_co  pi_ms_co  oa_ms_co  bi_ms_co |be_ms_as  sp_ms_as  pi_ms_as  oa_ms_as  bi_ms_as  fix_costs sub_har   sub_sv_co sub_fix   ', &! sea        
'# Year be_tg1    be_tg2    be_tg5    be_tg6    be_tg7    be_tg8    be_tg9    be_tg10   &
       &sp_tg1    sp_tg2    sp_tg4    sp_tg5    sp_tg6    sp_tg7    sp_tg8    sp_tg9    sp_tg10   &
       &pi_tg1    pi_tg2    pi_tg3    pi_tg4    pi_tg5    pi_tg6    pi_tg7    pi_tg8    pi_tg9    pi_tg10   &
       &oa_tg1    oa_tg2    oa_tg5    oa_tg6    oa_tg7    oa_tg8    oa_tg9    oa_tg10   &        
       &bi_tg1    bi_tg2    bi_tg5    bi_tg6    bi_tg7    bi_tg8    bi_tg9    bi_tg10', &! sea_ms
'# Year LVa       LVb       LVc       LVd       NPVa      NPVb      NPVc      NPVd      NPV+a     NPV+b     NPV+c     NPV+d ', &! sea_npv      
'# Year be_tg1    be_tg2    be_tg5    be_tg6    be_tg7    be_tg8    be_tg9    be_tg10   &
       &sp_tg1    sp_tg2    sp_tg4    sp_tg5    sp_tg6    sp_tg7    sp_tg8    sp_tg9    sp_tg10   &
       &pi_tg1    pi_tg2    pi_tg3    pi_tg4    pi_tg5    pi_tg6    pi_tg7    pi_tg8    pi_tg9    pi_tg10   &
       &oa_tg1    oa_tg2    oa_tg5    oa_tg6    oa_tg7    oa_tg8    oa_tg9    oa_tg10   &        
       &bi_tg1    bi_tg2    bi_tg5    bi_tg6    bi_tg7    bi_tg8    bi_tg9    bi_tg10', &! sea_st
'# Year sum_input u1        u2        u3        u4        u5        u6        u7        sum_u1-7  burn&
        &      landfill  atmo     atmo_cum  emission  sub_energ sub_mat   sub_sum',	& ! wpm	
'# Year tg1       tg2       tg3       tg4       tg5       tg6       il1       il2       il3       il4       il5       il6       il7       pl1       pl2       pl3       pl4       pl5       pl6       pl7       u1        u2        u3        u4        u5        u6        u7        '/ ! wpm_inter	

! special output forms
INTEGER :: out_flag_light           ! output flag light-file
INTEGER :: unit_err                 ! unit for error log file
INTEGER :: unit_trace               ! unit for trace log file
INTEGER :: unit_sum                 ! unit for summation output (fluxes) file
INTEGER :: unit_comp1, unit_comp2   ! ncompressed output
INTEGER :: unit_light, unit_wat
INTEGER :: unit_ctr, unit_prod, unit_allo, unit_soil
INTEGER :: unit_soicnd, unit_soicna, unit_soicnr

! store output variables of veg-file
type out_veg
  integer,dimension(3)::  help_veg1
  real,dimension(11)::    help_veg2
  real                    help_veg3
  real              ::    help_veg4
  real              ::    help_veg5
  real              ::    help_veg6
end type out_veg

type (out_veg),allocatable,dimension(:),target :: sout
type (out_veg) :: vout

type out_C
    real, dimension(366)::  NEE         ! net ecosystem exchange
    real, dimension(366)::  Resp_aut    ! autotrophic respiration
end type out_C

type (out_C) :: Cout

character(100)  ::   mess_info = '# '    ! output of measurements: information line

end module data_out

!**************************************************************

