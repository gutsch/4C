!*****************************************************************!
!*                                                               *!
!*              4C (FORESEE) Simulation Model                    *!
!*                                                               *!
!*              Subroutines for standard tasks                   *!
!*                                                               *!
!*   contains:                                                   *!
!*   SOLV_QUADR		solving quadratic equation, real*4           *!
!*   DSOLV_QUADR	solving quadratic equation, real*8           *!
!*   NEWT           Newton method                                *!
!*   TRICOF     	Harmonic Analysis                            *!
!*   SORT_INDEX    	Sorts two arrays                             *!
!*   SORT        	sort an array by quicksort method            *!
!*   MOMENT      	Descriptive statistics of a data set         *!
!*                                                               *!
!*                  Copyright (C) 1996-2018                      *!
!*     Potsdam Institute for Climate Impact Reserach (PIK)       *!
!*          Authors and contributors see AUTHOR file             *!
!*  This file is part of 4C and is licensed under BSD-2-Clause   *!
!*                   See LICENSE file or under:                  *!
!*     http://www.https://opensource.org/licenses/BSD-2-Clause   *!
!*                           Contact:                            *!
!*       https://gitlab.pik-potsdam.de/foresee/4C                *!
!*                                                               *!
!*****************************************************************!

SUBROUTINE solv_quadr (meth, p, q, x1, x2, res1, res2, rnum)

! Solution of quadratic equation in normal form
!     x*x + p*x + q = 0

IMPLICIT NONE

! Input
integer meth        ! solver method
real    p, q        ! parameter of the quadratic equation

! Output
real    x1, x2      ! solutions; initial value of Newton method
real    res1, res2  ! residua
integer rnum        ! return code

real    discr

! Variables of Newton method
real      df       ! quotient of quadratic function and its first derivative
real   :: precision = 1E-5 
integer:: maxloop, iloop

! Variables of solver program ZPORC of ISML Library

!external ZPLRC

discr = (p*p/4.)-q
if (discr .lt. 0.) then
    rnum = -1     ! no real solution
    return
else
    select case (meth)
    case (1)
  ! standard solution  
          discr = SQRT(discr)
          x1    = -p/2. + discr
          x2    = -p/2. - discr
          rnum  = 0

    case (2)
  ! Vieta's formulae (root theorem) 
          discr = SQRT(discr)
          x2    = -p/2. - discr
          x1    = q / x2
          rnum  = 0
      
    case (3)
  ! Newton method
  ! initial value x2
          maxloop = 100
          iloop   = 1
          df    = (x2*x2 + p*x2 + q) / (2.* x2 + p) 
          do while (abs(df) .gt. precision .and. iloop .le. maxloop)
             x2 = x2 - df
             df = (x2*x2 + p*x2 + q) / (2.* x2 + p) 
             iloop = iloop + 1
          enddo
         if (iloop .lt. maxloop) then
             rnum = 0
          else
             rnum = 1    
          endif

    end select
endif   ! discr

  res1  = x1*x1 + p*x1 + q
  res2  = x2*x2 + p*x2 + q

END SUBROUTINE solv_quadr

!**************************************************************

SUBROUTINE dsolv_quadr (meth, p, q, x1, x2, res1, res2, rnum)

! Solution of quadratic equation in normal form
!     x*x + p*x + q = 0
! with double precision

IMPLICIT NONE

! Input
integer meth                        ! solver method
real (kind (0.0D0)) :: p, q         ! parameter of the quadratic equation

! Output
real (kind (0.0D0)) :: x1, x2       ! solutions
real (kind (0.0D0)) :: res1, res2   ! residua
integer rnum                        ! return code

real (kind (0.0D0)) :: discr

! Variables of Newton method
real (kind (0.0D0)) :: df           ! quotient of quadratic function and its first derivative
real (kind (0.0D0)) :: precision = 1E-5 
integer:: maxloop, iloop

! Variables for solver program ZPORC of ISML Library
real (kind (0.0D0)) :: coeff(3)
real (kind (0.0D0)) :: zero(2)

discr = (p*p/4.)-q
if (discr .lt. 0.) then
    rnum = -1     ! no real solution
    return
else
    select case (meth)
    case (1)
  ! standard solution  
          discr = DSQRT(discr)
          x1    = -p/2. + discr
          x2    = -p/2. - discr
          rnum  = 0

    case (2)
  ! Vieta's formulae (root theorem) 
          discr = DSQRT(discr)
          x2    = -p/2. - discr
          x1    = q / x2
          rnum  = 0

    case (3)
  ! Newton method
  ! initial value x2
          maxloop = 100
          iloop   = 1
          df    = (x2*x2 + p*x2 + q) / (2.* x2 + p) 
          do while (abs(df) .gt. precision .and. iloop .le. maxloop)
             x2 = x2 - df
             df = (x2*x2 + p*x2 + q) / (2.* x2 + p) 
             iloop = iloop + 1
          enddo
          if (iloop .lt. maxloop) then
             rnum = 0
          else
             rnum = 1    
          endif
    end select
endif   ! discr

  res1  = x1*x1 + p*x1 + q
  res2  = x2*x2 + p*x2 + q

END SUBROUTINE dsolv_quadr

!**************************************************************

SUBROUTINE newt (x, f, df, ddf, prec, maxit, rnum)

! Newton method

implicit none
integer :: maxit    ! maximum number of iteration   
real    :: x        ! initial value and result
real    :: prec     ! precision
real    :: dx       ! quotient of function and its first derivative
real    :: hf, hdf, hddf
integer :: rnum     ! options: 0 - change of sign allowed,
                    !          1 - no change of sign
                    ! return code
integer :: i
real, external :: f, df, ddf  ! function and its first derivative


hf   = f(x)
hdf  = df(x)
hddf = ddf(x)
dx   = hddf * hf

if (abs(dx) .lt. abs(hdf*hdf)) then    ! Test of convergence 

 ! Iteration
  i = 1
  if (abs(dx) .gt. 0.) then
     dx = hf / hdf 
  endif
  do while (abs(hf) .gt. prec .and. i .le. maxit)
     if (dx .gt. x .and. rnum .gt. 0) dx = x/2.  
     x   = x - dx
     hdf = df(x)
     if (abs(hdf) .gt. 0.) then
        hf = f(x)
        dx = hf/hdf 
     endif
     i  = i + 1
  enddo
  if (i .lt. maxit) then
     rnum = 0
  else
     rnum = 1   ! not enough iteration steps
  endif

else
    rnum = -1   ! no convergence
endif

END SUBROUTINE newt

!**************************************************************

      SUBROUTINE TRICOF(F,NF,A,NE,B,NO,IOP)

!    PURPOSE = TO COMPUTE THE COEFFICIENTS IN A TRIGONOMETRIC EXPANSION
!              FOR A FUNCTION GIVEN IN EQUIDISTANT POINTS

!    PARAMETERS

!    F       = AN ARRAY USED FOR STORING THE FUNCTION VALUES F(X)
!    NF      = THE NUMBER OF FUNCTION VALUES IN THE ARRAY F.NF MUST
!              HAVE THE STRUCTURE , NF = 2*N+1 , THE GENERAL CASE
!                                   NF = N+1   , THE EVEN CASE
!                                   NF = N-1   , THE ODD CASE
!    A       = AN ARRAY USED FOR RETURNING THE COEFFICIENTS OF THE COS-
!             INE TERMS
!    NE      = THE NUMBER OF COEFFICIENTS IN THE ARRAY A , NE = N+1
!    B       = AN ARRAY USED FOR RETURNING THE COEFFICIENTS OF THE SINE
!              TERMS
!    NO      = THE NUMBER OF COEFFICIENTS IN THE ARRAY B , NO = N-1
!    IOP     = OPTION NUMBER , IOP = 1 , THE GENERAL CASE
!                              IOP = 2 , THE EVEN CASE
!                              IOP = 3 , THE ODD CASE

      DIMENSION F(NF) , A(NE) , B(NO)

      REAL KSI0 , KSI1 , KSIK

      DATA ZERO , FOURTH , HALF , ONE , TWO , PI / 0. , .25 , .5 , 1. , 2. , 3.14159265358979 /

!    COMPUTE THE NUMBER N (SEE EXPLANATION OF PARAMETERS)

 1000 N=0
      IF (IOP.EQ.1) N=(NF-1)/2
      IF (IOP.EQ.2) N=NF-1
      IF (IOP.EQ.3) N=NF+1
      IF (N.EQ.0) STOP

!    STOP IF IOP DOES NOT HAVE A CORRECT VALUE

      IF (IOP.GT.1) GO TO 1030
      IF ((2*N-NF+1).NE.0) STOP

!    STOP IF NF DOES NOT HAVE THE CORRECT STRUCTURE IN THE GENERAL CASE

!    SPLIT THE FUNCTION F(X) IN AN EVEN AND ODD PART

      M=N+1
      DO 1020 J=1,N
      COF1=HALF*(F(M+J)+F(M-J))
      COF2=HALF*(F(M+J)-F(M-J))
      F(M+J)=COF2
      F(M-J)=COF1
 1020 CONTINUE

!    REWRITE N IN POWERS OF 2 I.E. N=NBASE*2**NEXP

 1030 NBASE=N
      NEXP =0
 1040 NINT =NBASE/2
      IF ((NBASE-2*NINT).NE.0) GO TO 1050
      NBASE=NINT
      NEXP =NEXP+1
      GO TO 1040

!    DO SOME INITIAL CALCULATIONS

 1050 REALN=NBASE
      ARG  =HALF*PI/REALN
      KSI0 =COS(ARG)
      ETA0 =SIN(ARG)

!    START CALCULATION OF COEFFICIENTS

      IF (IOP.EQ.3) GO TO 1160

!    **********     EVEN COEFFICIENT CALCULATION     **********

!    COMPUTE THE BASIC COEFFICIENTS A(K) , K=1(1)(NBASE+1)

!    START CALCULATION OF A(1)

      NN    =NBASE-1
      NPOINT=1
      NINCRE=2**NEXP
      NLOCAL=NINCRE+1
      BASEIN=ONE/REALN
      A(1)  =HALF*(F(1)+F(N+1))
      IF (NN.EQ.0) GO TO 1065
      DO 1060 J=1,NN
      A(1)  =A(1)+F(NLOCAL)
      NLOCAL=NLOCAL+NINCRE
 1060 CONTINUE
 1065 A(1)  =TWO*BASEIN*A(1)

!    START CALCULATION OF A(K) , K=2(1)(NBASE+1)

      KSI1=KSI0
      KSIK=KSI1
      ETA1=ETA0
      ETAK=ETA1
      CONST=HALF*F(N+1)
      DO 1090 K=1,NBASE
      COF1=TWO*(TWO*KSIK**2-ONE)
      A2  =ZERO
      A1  =A2
      A0  =CONST
      NLOCAL=N+1-NINCRE
      DO 1070 J=1,NBASE
      A2=A1
      A1=A0
      A0=F(NLOCAL)+COF1*A1-A2
      NLOCAL=NLOCAL-NINCRE
 1070 CONTINUE

 1080 A(K+1)=BASEIN*(A0-A2)
      COF1  =KSIK
      COF2  =ETAK
      KSIK =KSI1*COF1-ETA1*COF2
      ETAK =ETA1*COF1+KSI1*COF2
 1090 CONTINUE


!    CALCULATION OF THE BASIC EVEN COEFFICIENTS FINISHED

      IF (NEXP.EQ.0) GO TO 1145

!    CONTINUE CALCULATION OF EVEN COEFFICIENTS

      NUMCOF=NBASE
      DO 1140 NSTEP=1,NEXP
      NINCRE=2**(NEXP-NSTEP)
      NPOINT=NINCRE+1
      NINCRE=2*NINCRE
      NLOCAL=NPOINT
      NUMBER=2*NUMCOF+1

!    COMPUTE CONSTANT TERM IN MID-POINT APPROXIMATION I.E. K=1

      SUM=ZERO
      DO 1100 J=1,NUMCOF
      SUM=SUM+F(NLOCAL)
      NLOCAL=NLOCAL+NINCRE
 1100 CONTINUE

      SUM =TWO*BASEIN*SUM
      COF1=A(1)
      A(1)=HALF*(COF1+SUM)
      A(NUMBER)=HALF*(COF1-SUM)

      IF (NUMCOF.EQ.1) GO TO 1135


!    COMPUTE MID-POINT APPROXIMATION FOR K=2(1)NUMCOF

 1105 NN  =NUMCOF-1
      KSIK=KSI1
      ETAK=ETA1
      DO 1130 K=1,NN
      COF1=TWO*(TWO*KSIK**2-ONE)
      A2=ZERO
      A1=A2
      NLOCAL=N+2-NPOINT
      A0=F(NLOCAL)
      DO 1110 J=1,NN
      A2=A1
      A1=A0
      NLOCAL=NLOCAL-NINCRE
      A0=F(NLOCAL)+COF1*A1-A2
 1110 CONTINUE

 1120 SUM=TWO*BASEIN*(A0-A1)*KSIK
      COF1=A(K+1)
      A(K+1)=HALF*(COF1+SUM)
      A(NUMBER-K)=HALF*(COF1-SUM)

      COF1=KSIK
      COF2=ETAK
      KSIK=KSI1*COF1-ETA1*COF2
      ETAK=ETA1*COF1+KSI1*COF2

 1130 CONTINUE
 1135 A(NUMCOF+1)=HALF*A(NUMCOF+1)

!    CALCULATIONS OF MID-POINT APPROXIMATIONS FINISHED

!    DO CHANGES RELATED TO HALVING OF THE INTERVAL

      ARG =HALF*ARG
      COF1=ETA1
      ETA1=SIN(ARG)
      KSI1=HALF*COF1/ETA1
      BASEIN=HALF*BASEIN
      NUMCOF=2*NUMCOF

 1140 CONTINUE
 1145 IF (NEXP.EQ.0) NUMBER=NBASE+1
      A(NUMBER)=HALF*A(NUMBER)

!    CALULATION OF EVEN COEFFICIENTS FINISHED

 1150 IF (IOP.EQ.2) RETURN

!    RETURN TO CALLING PROGRAM IF F(X) WAS AN EVEN FUNCTION
!    IF IOP=1 CHANGE SIGN OF EACH SECOND COEFFICIENTS

      NINT=(N+1)/2
      IF (NINT.EQ.0) GO TO 1166
      DO 1164 K=1,NINT
      A(2*K)=-A(2*K)
 1164 CONTINUE


!    **********     ODD COEFFICIENT CALCULATION     **********

!    COMPUTE THE BASIC COEFFICIENTS B(K) , K=1(1)NBASE

 1166 ARG=HALF*PI/REALN
 1160 IF (IOP.EQ.1) NMAX=2*N+1
      IF (IOP.EQ.3) NMAX=N
      NINCRE=2**NEXP
      NPOINT=NMAX-NINCRE
      NLOCAL=NPOINT
      BASEIN=ONE/REALN
      B(1)=ZERO
      IF (NBASE.EQ.1) GO TO 1200
      KSI1=TWO*KSI0**2-ONE
      KSIK=KSI1
      ETA1=TWO*KSI0*ETA0
      ETAK=ETA1
      NN =NBASE-1
      NNN=NN-1
      DO 1190 K=1,NN
      COF1=TWO*KSIK
      A2  =ZERO
      A1  =A2
      A0  =F(NPOINT)
      NLOCAL=NPOINT-NINCRE
      IF (NNN.EQ.0) GO TO 1180
      DO 1170 J=1,NNN
      A2=A1
      A1=A0
      A0=F(NLOCAL)+COF1*A1-A2
      NLOCAL=NLOCAL-NINCRE
 1170 CONTINUE

 1180 B(K)=TWO*BASEIN*A0*ETAK
      COF1=KSIK
      COF2=ETAK
      KSIK=KSI1*COF1-ETA1*COF2
      ETAK=ETA1*COF1+KSI1*COF2
 1190 CONTINUE

!    CALCULATION OF THE BASIC ODD COEFFICIENTS FINISHED

 1200 IF (NEXP.EQ.0) GO TO 1260

!    CONTINUE CALCULATION OF ODD COEFFICIENTS

      KSI1=KSI0
      ETA1=ETA0

      NUMCOF=NBASE
      DO 1250 NSTEP=1,NEXP
      KSIK=KSI1
      ETAK=ETA1
      NINCRE=2**(NEXP-NSTEP)
      NPOINT=NMAX-NINCRE
      NINCRE=2*NINCRE
      NUMBER=2*NUMCOF
      B(NUMCOF)=ZERO

!    COMPUTE MID-POINT APPROXIMATIONS FOR K=1(1)NUMCOF

      NN  =NUMCOF-1
      DO 1240 K=1,NUMCOF
      COF1=TWO*(TWO*KSIK**2-ONE)
      A2  =ZERO
      A1  =A2
      NLOCAL=NPOINT
      A0  =F(NLOCAL)
      IF (NN.EQ.0) GO TO 1220
      DO 1210 J=1,NN
      A2=A1
      A1=A0
      NLOCAL=NLOCAL-NINCRE
      A0=F(NLOCAL)+COF1*A1-A2
 1210 CONTINUE

 1220 SUM=TWO*BASEIN*(A0+A1)*ETAK
      COF1=B(K)
      B(K)=HALF*(COF1+SUM)
      IF (K.EQ.NUMCOF) GO TO 1230
      B(NUMBER-K)=-HALF*(COF1-SUM)

 1230 COF1=KSIK
      COF2=ETAK
      KSIK=KSI1*COF1-ETA1*COF2
      ETAK=ETA1*COF1+KSI1*COF2

 1240 CONTINUE

!    CALCULATION OF MID-POINT APPROXIMATION FINISHED

!    DO CHANGES RELATED TO HALVING OF INTERVAL

      ARG =HALF*ARG
      COF1=ETA1
      ETA1=SIN(ARG)
      KSI1=HALF*COF1/ETA1
      BASEIN=HALF*BASEIN
      NUMCOF=2*NUMCOF

 1250 CONTINUE

!    CALCULATION OF ODD COEFFICIENTS FINISHED

 1260 IF (IOP.EQ.3) RETURN

!    IF IOP=1 RECOMPUTE FUNCTION VALUES

      DO 1270 J=1,N
      COF2=F(M+J)
      COF1=F(M-J)
      F(M+J)=COF1+COF2
      F(M-J)=COF1-COF2
 1270 CONTINUE

      RETURN

      END SUBROUTINE TRICOF

!**************************************************************

      SUBROUTINE sort_index(n,arr,brr)

! variation of sort2 for integer array
! sorts array arr(1:n) into an ascending order and 
! makes the corresponding rearrangement of the array brr(1:n)      

      INTEGER n,M,NSTACK

      Integer arr(n)
      INTEGER brr(n)

      PARAMETER (M=7,NSTACK=50)

      INTEGER i,ir,j,jstack,k,l,istack(NSTACK)

      REAL a,b,temp

      jstack=0
      l=1
      ir=n

1     if(ir-l.lt.M)then

        do 12 j=l+1,ir
          a=arr(j)
          b=brr(j)

          do 11 i=j-1,1,-1
            if(arr(i).le.a)goto 2
            arr(i+1)=arr(i)
            brr(i+1)=brr(i)

11        continue

          i=0
2         arr(i+1)=a
          brr(i+1)=b

12      continue

        if(jstack.eq.0)return
        ir=istack(jstack)
        l=istack(jstack-1)
        jstack=jstack-2
      else
        k=(l+ir)/2
        temp=arr(k)
        arr(k)=arr(l+1)
        arr(l+1)=temp
        temp=brr(k)
        brr(k)=brr(l+1)
        brr(l+1)=temp
        if(arr(l+1).gt.arr(ir))then
          temp=arr(l+1)
          arr(l+1)=arr(ir)
          arr(ir)=temp
          temp=brr(l+1)
          brr(l+1)=brr(ir)
          brr(ir)=temp
        endif

        if(arr(l).gt.arr(ir))then
          temp=arr(l)
          arr(l)=arr(ir)
          arr(ir)=temp
          temp=brr(l)
          brr(l)=brr(ir)
          brr(ir)=temp
        endif

        if(arr(l+1).gt.arr(l))then
          temp=arr(l+1)
          arr(l+1)=arr(l)
          arr(l)=temp
          temp=brr(l+1)
          brr(l+1)=brr(l)
          brr(l)=temp
        endif

        i=l+1
        j=ir
        a=arr(l)
        b=brr(l)

3       continue

          i=i+1
        if(arr(i).lt.a)goto 3

4       continue

          j=j-1
        if(arr(j).gt.a)goto 4
        if(j.lt.i)goto 5
        temp=arr(i)
        arr(i)=arr(j)
        arr(j)=temp
        temp=brr(i)
        brr(i)=brr(j)
        brr(j)=temp

        goto 3

5       arr(l)=arr(j)
        arr(j)=a
        brr(l)=brr(j)
        brr(j)=b
        jstack=jstack+2

        if(jstack.gt.NSTACK)pause 'NSTACK too small in sort2'
        if(ir-i+1.ge.j-l)then
          istack(jstack)=ir
          istack(jstack-1)=i
          ir=j-1
        else
          istack(jstack)=j-1
          istack(jstack-1)=l
          l=i
        endif
      endif

      goto 1

      END

!  (C) Copr. 1986-92 Numerical Recipes Software "!D#+.

!**************************************************************

      SUBROUTINE sort(n,arr)
 
 ! sort a n-dimensional array arr(1:n) by quicksort method 
 
      INTEGER n,M,NSTACK
      REAL arr(n)             
      PARAMETER (M=7,NSTACK=50)
      INTEGER i,ir,j,jstack,k,l,istack(NSTACK)
      REAL a,temp
      jstack=0
      l=1
      ir=n
1     if(ir-l.lt.M)then
        do 12 j=l+1,ir
          a=arr(j)
          do 11 i=j-1,1,-1
            if(arr(i).le.a)goto 2
            arr(i+1)=arr(i)
11        continue
          i=0
2         arr(i+1)=a
12      continue
        if(jstack.eq.0)return
        ir=istack(jstack)
        l=istack(jstack-1)
        jstack=jstack-2
      else
        k=(l+ir)/2
        temp=arr(k)
        arr(k)=arr(l+1)
        arr(l+1)=temp
        if(arr(l+1).gt.arr(ir))then
          temp=arr(l+1)
          arr(l+1)=arr(ir)
          arr(ir)=temp
        endif
        if(arr(l).gt.arr(ir))then
          temp=arr(l)
          arr(l)=arr(ir)
          arr(ir)=temp
        endif
        if(arr(l+1).gt.arr(l))then
          temp=arr(l+1)
          arr(l+1)=arr(l)
          arr(l)=temp
        endif
        i=l+1
        j=ir
        a=arr(l)
3       continue
          i=i+1
        if(arr(i).lt.a)goto 3
4       continue
          j=j-1
        if(arr(j).gt.a)goto 4
        if(j.lt.i)goto 5
        temp=arr(i)
        arr(i)=arr(j)
        arr(j)=temp
        goto 3
5       arr(l)=arr(j)
        arr(j)=a
        jstack=jstack+2
        if(jstack.gt.NSTACK)pause 'NSTACK too small in sort'
        if(ir-i+1.ge.j-l)then
          istack(jstack)=ir
          istack(jstack-1)=i
          ir=j-1
        else
          istack(jstack)=j-1
          istack(jstack-1)=l
          l=i
        endif
      endif
      goto 1
      END
! C  (C) Copr. 1986-92 Numerical Recipes Software )$!.

!**************************************************************

      SUBROUTINE moment(array,n,ave,adev,sdev,var,skew,curt)

! Calculates statistics of array (n-dimensional array of data)
!   n       - number of observations
!   adev    - average deviation
!   ave     - average
!   curt    - curtosis
!   sdev    - standard deviation
!   skew    - skewness
!   var     - variance 

      INTEGER n
      REAL adev,ave,curt,sdev,skew,var,array(n)
      INTEGER j
      REAL p,s,ep
      if(n.le.1)pause 'n must be at least 2 in moment'
      s=0.
      do 11 j=1,n
        s=s+array(j)
11    continue
      ave=s/n
      adev=0.
      var=0.
      skew=0.
      curt=0.
      ep=0.
      do 12 j=1,n
        s=array(j)-ave
        ep=ep+s
        adev=adev+abs(s)
        p=s*s
        var=var+p
        p=p*s
        skew=skew+p
        p=p*s
        curt=curt+p
12    continue
      adev=adev/n
      var=(var-ep**2/n)/(n-1)
      sdev=sqrt(var)
      if(var.ne.0.)then
        skew=skew/(n*sdev**3)
        curt=curt/(n*var**2)-3.
      else
         skew = -99.
         curt = -99.
      endif
      return
      END
!  (C) Copr. 1986-92 Numerical Recipes Software )$!.

	  FUNCTION rtbis(func,x1,x2,xacc)
      INTEGER JMAX
      REAL rtbis,x1,x2,xacc,func
      EXTERNAL func
      PARAMETER (JMAX=40)
      INTEGER j
      REAL dx,f,fmid,xmid
      fmid=func(x2)
      f=func(x1)
      if(f.lt.0.)then
        rtbis=x1
        dx=x2-x1
      else
        rtbis=x2
        dx=x1-x2
      endif
      do  j=1,JMAX
        dx=dx*.5
        xmid=rtbis+dx
        fmid=func(xmid)
        if(fmid.le.0.)rtbis=xmid
        if(abs(dx).lt.xacc .or. fmid.eq.0.) return

      end do
      END function