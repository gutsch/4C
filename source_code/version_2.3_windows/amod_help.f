!*****************************************************************!
!*                                                               *!
!*              4C (FORESEE) Simulation Model                    *!
!*                                                               *!
!*                                                               *!
!*      data module                                              *!
!*                 data_help                                     *!
!*                 data_help_dbh                                 *!
!*                                                               *!
!*                  Copyright (C) 1996-2018                      *!
!*     Potsdam Institute for Climate Impact Reserach (PIK)       *!
!*          Authors and contributors see AUTHOR file             *!
!*  This file is part of 4C and is licensed under BSD-2-Clause   *!
!*                   See LICENSE file or under:                  *!
!*     http://www.https://opensource.org/licenses/BSD-2-Clause   *!
!*                           Contact:                            *!
!*       https://gitlab.pik-potsdam.de/foresee/4C                *!
!*                                                               *!
!*****************************************************************!
Module data_help

integer    :: hnspec=0  ! species number

real       :: mschelp ! weight of seed class
real       ::heihelp  ! height of plant class
REAL       :: x_sap,x_hrt,x_fol,x_frt,x_Ahb   !inital values for cohorts
integer    :: fail
end module data_help


module data_help_dbh

! for function in calc_dbh 

real ::     fAhb = 0.,      &      ! cross sectional area heartwood at tree base
            fB   = 0.,        &      ! bole height,
            fH   = 0.,        &      ! heartwood
            fHt  = 0.,       &      ! total tree height
            fsprhos

end module data_help_dbh
