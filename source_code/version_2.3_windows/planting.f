!*****************************************************************!
!*                                                               *!
!*                      4C (FORESEE)                             *!
!*                                                               *!
!*                                                               *!
!*             Subroutines for planting                          *!
!*                                                               *!
!*             contains:                                         *!
!*             SR planting                                       *!
!*             function sapwood                                  *!
!*             SR gener_coh                                      *!
!*                                                               *!
!*   comment: planting is controlled by the flag flag_reg,       *!
!*            soe standardized planting ensembles are definded   *!
!*                                                               *!
!*                  Copyright (C) 1996-2018                      *!
!*     Potsdam Institute for Climate Impact Reserach (PIK)       *!
!*          Authors and contributors see AUTHOR file             *!
!*  This file is part of 4C and is licensed under BSD-2-Clause   *!
!*                   See LICENSE file or under:                  *!
!*    http://www.https://opensource.org/licenses/BSD-2-Clause    *!
!*                           Contact:                            *!
!*       https://gitlab.pik-potsdam.de/foresee/4C                *!
!*                                                               *!
!*****************************************************************!

SUBROUTINE planting
 USE data_stand
 USE data_simul
 USE data_species
 USE data_soil
 USE data_help
 USE data_plant
 USE data_manag
 use data_wpm

 IMPLICIT NONE
 integer    :: nplant,       &
               taxid,        &
               i,j,nr,       &
               taxnum,       &
               outunit, ctrlunit
 real       :: age,          &
               pl_height,    &
               sdev,         &
               plhmin,       &
               rsap,         &
               hbc,           &
               bhd,           &
               cform,         &
               hlp_lai,       &
               rednpl_sh

real         :: rtflsp, sapwood, height, plots
integer, dimension(nspec_tree) :: infhelp
character(80)          :: infile
character              :: text
 CHARACTER :: source
integer                :: inunit,ios
integer                :: parunit
integer                :: nkoh, nplanth, numb

REAL p0(nspec_tree),p1(nspec_tree),p2(nspec_tree),p3(nspec_tree),p4(nspec_tree), &
       c1(nspec_tree),c2(nspec_tree),ku_a0(nspec_tree),ku_a1(nspec_tree),ku_a2(nspec_tree),&
       ku_b0(nspec_tree),ku_b1(nspec_tree),ku_b2(nspec_tree),ku_c0(nspec_tree),&
       ku_c1(nspec_tree),ku_c2(nspec_tree),wei_k1(nspec_tree),wei_k2(nspec_tree)
real      :: crown_base, crown_base_eg
TYPE(cohort)    ::tree_ini

real corr_la
real      :: troot2

real, dimension(20)   :: hhei

external sapwood
external rtflsp
 do i =1,nspec_tree
    infhelp(i) = infspec(i)
 end do

   parunit=GETUNIT()
    OPEN (parunit, FILE='input/generreg.par', STATUS='old')
   DO i=1,nspec_tree
      READ (parunit,*) p0(i),p1(i),p2(i),p3(i),p4(i),c1(i),c2(i),ku_a0(i),ku_a1(i),ku_a2(i), &
           ku_b0(i),ku_b1(i),ku_b2(i),ku_c0(i),ku_c1(i),ku_c2(i),wei_k1(i),wei_k2(i)
   ENDDO
   CLOSE(parunit)


!*********************** sea **************************************
	plant_year = time
	flag_plant = flag_reg
!******************************************************************

 rednpl_sh = 1.
! modification uf number of planted trees in the case of shelterwood management
 if(flag_shelter.eq.1) rednpl_sh = 0.7

 taxid = 0
 if( flag_reg .ge.10) quspec = 1

 if(flag_reg.ge.10.and.flag_reg.lt. 30) then

  ! planting of mono-species stands

     select case(flag_reg)

       case(10)
    ! planting pine
         taxnum = 3
       case(11)
    ! planting beech
        taxnum = 1
       case(12)
    ! planting oak
        taxnum =4
       case(13)
    ! planting spruce
        taxnum = 2
       case(14)
    ! planting birch
        taxnum = 5
       case(15)
    ! planting aspen
       taxnum = 8
       case(16)
    ! planting aleppo pine
       taxnum = 9
      case(17)
	! planting douglas fir
	    taxnum =10
      case(18)
	! planting black locust
	    taxnum =11

      case(20)
! reading planting data from file and generating tree cohorts

        inunit=getunit()
        write(*,'(a)') ' *** Planting of small trees ***'
        write(*,'(A)',advance='no')' Input directory and file for planting: '
        read (*,'(A)') infile
        open (inunit,FILE=trim(infile),STATUS='old')
! read head of data-file
       outunit=getunit()
       open(outunit, FILE=TRIM(treefile(ip)),STATUS='replace')
       ctrlunit = getunit() +1
       OPEN (ctrlunit,FILE=TRIM(site_name(ip))//'.initctrl',STATUS='replace')
       plots=10000.
        do
            read(inunit,*) text
            if(text .ne. '!')then
               backspace(inunit);exit
            endif
        enddo
! modification AB  19.9.11
      CALL header(outunit,infile,source,cform,rsap,flag_volfunc,kpatchsize)

        do
           READ(inunit,*,IOSTAT=ios)numb, nplant ,taxid,pl_height, age, bhd, hbc
           if(ios<0) exit
           height = pl_height
! Modification (Alexander Borys), generating of nkoh cohorts from given data, 19.9.11
		   nkoh =10
		   do i = 1, nkoh
                  hhei(i) = height*(0.8 + (i-1)*0.025)
		   end do  
		    write(outunit,*) numb, plots

           do i = 1, nkoh
             pl_height = hhei(i)*100.
			 height = hhei(i)
             if(taxid.eq.12.or. taxid.eq.13) then
! Eucalyptus
		         hbc = crown_base_eg(height, bhd)
		     else	      
                 hbc=crown_base(height,c1(taxid),c2(taxid),bhd)
		     end if
			 nplanth = int(nplant/nkoh)
			 rsap = 0.5
             source = 'd'
             cform=1;hlp_lai=0
             corr_la = 1.
             call treeini(outunit,ctrlunit,taxid,source,bhd,height,hbc,nplanth,cform,rsap,int(age),hlp_lai,corr_la)
             max_coh = max_coh + 1
   ! initialise tree_ini with zero
              call coh_initial (tree_ini)

             tree_ini%ident =  max_coh
             tree_ini%species = taxid
             tree_ini%ntreea = nplant
             tree_ini%ntreed = 0.
             tree_ini%nta = tree_ini%ntreea
             tree_ini%x_age = age
             tree_ini%x_hbole = hbc
             tree_ini%resp = 0.
             tree_ini%height = pl_height
             tree_ini%x_sap = x_sap
             tree_ini%x_fol = x_fol
             tree_ini%x_frt = x_frt
             tree_ini%x_hrt = x_hrt
             tree_ini%x_ahb = x_ahb
             tree_ini%x_crt = (tree_ini%x_sap + tree_ini%x_hrt) * spar(taxid)%alphac*spar(taxid)%cr_frac
             tree_ini%x_tb = (tree_ini%x_sap + tree_ini%x_hrt) * spar(taxid)%alphac*(1.-spar(taxid)%cr_frac)

! Borys
             tree_ini%diam = bhd
             tree_ini%med_sla = spar(taxid)%psla_min + spar(taxid)%psla_a*0.5
             tree_ini%t_leaf = tree_ini%med_sla* tree_ini%x_fol                              ! [m-2]
             tree_ini%ca_ini = tree_ini%t_leaf
             tree_ini%crown_area = tree_ini%ca_ini
! initialize pheno state variables
    IF(spar(tree_ini%species)%Phmodel==1) THEN
       tree_ini%P=0
       tree_ini%I=1
    ELSE
       tree_ini%P=0
       tree_ini%I=0
       tree_ini%Tcrit=0
    END IF

    IF(nplant.ne.0.) then
       IF (.not. associated(pt%first)) THEN
          ALLOCATE (pt%first)
          pt%first%coh = tree_ini
          NULLIFY(pt%first%next)

!     root distribution
        call root_depth (1, pt%first%coh%species, pt%first%coh%x_age, pt%first%coh%height, pt%first%coh%x_frt, pt%first%coh%x_crt, nr, troot2, pt%first%coh%x_rdpt, pt%first%coh%nroot)
        pt%first%coh%nroot = nr
        do j=1,nr
           pt%first%coh%rooteff = 1.   ! assumption for the first use
        enddo
        do j=nr+1, nlay
           pt%first%coh%rooteff = 0.   ! layers with no roots
        enddo

      ELSE
          ALLOCATE(zeig)
          zeig%coh = tree_ini
          zeig%next => pt%first
          pt%first => zeig

!     root distribution
        call root_depth (1, zeig%coh%species, zeig%coh%x_age, zeig%coh%height, zeig%coh%x_frt, zeig%coh%x_crt, nr, troot2, zeig%coh%x_rdpt, zeig%coh%nroot)
        zeig%coh%nroot = nr
        do j=1,nr
           zeig%coh%rooteff = 1.   ! assumption for the first use
        enddo
        do j=nr+1, nlay
           zeig%coh%rooteff = 0.   ! layers with no roots
        enddo

      END IF   !  associated
      anz_coh=anz_coh+1
    END IF  ! nplant
       
	   end do   ! nkoh
	   end do
        close(inunit)
        close (outunit)
      close (ctrlunit)
        return
      
      end select

! liocourt management with regeneration if flag_mg = 44
if(flag_mg.eq.44) then

  do i= 1, M_numclass
     taxid = m_specpl(spec_lic,i)
	 age = m_pl_age(spec_lic,i)
     pl_height = m_plant_height(spec_lic,i)
	 plhmin = m_plant_hmin(spec_lic,i)
     nplant = m_numplant(spec_lic,i) * kpatchsize/10000 
     sdev = m_hsdev(spec_lic,i)
     call gener_coh(taxid, age, pl_height, plhmin, nplant,sdev)
  end do
else
     taxid = taxnum
     age = pl_age(taxnum)
     pl_height = plant_height(taxnum)
     plhmin = plant_hmin(taxnum)

!  number of seedling from data_plant
     nplant = rednpl_sh*nint(numplant(taxnum)*kpatchsize/10000)
! number of seedlings from seedrate
      if(flag_reg.eq.15.or.flag_reg.eq.16.or.flag_reg.eq.18) nplant = spar(taxnum)%seedrate*kpatchsize
     sdev = hsdev(taxnum)
     call gener_coh(taxid, age, pl_height, plhmin, nplant,sdev)
end if

 else
  ! planting of mixed stands
  ! mixture given by ara<meters in data_plant
   if(flag_reg.eq.9) then
      do i = 1,nspec_tree
         if (infspec(i).eq.1 .and. infhelp(i).eq.1) then
               taxid = i
               age = pl_age(taxid)
               pl_height = plant_height(taxid)
               plhmin = plant_hmin(taxid)
               nplant = rednpl_sh*nint(npl_mix(taxid)*kpatchsize/10000)
               sdev = hsdev(taxid)
               call gener_coh(taxid, age, pl_height, plhmin, nplant,sdev)
               infhelp(i) = 0
          end if
      end do  ! i
  else if(flag_reg.lt.9.or.flag_reg.gt.30) then
     infspec = 0
     npl_mix = 0

     select case (flag_reg)
! planting of well definded mixtures of pine and oak
       case(8)
           infspec(3)=1
           infspec(4)=1
           npl_mix(3) = 9000.
           npl_mix(4) = 1000.
       case(7)
           infspec(3)=1
           infspec(4)=1
           npl_mix(3) = 7000.
           npl_mix(4) = 3000.
       case(6)
           infspec(3)=1
           infspec(4)=1
            npl_mix(3) = 5000.
            npl_mix(4) =  5000.
       case(5)
           infspec(3)=1
           infspec(4)=1
           npl_mix(3) = 3000.
           npl_mix(4) = 7000.
       case(4)
           infspec(3)=1
           infspec(4)=1
           npl_mix(3) = 2000.
           npl_mix(4) = 8000.
       case(33)
           infspec(2) = 1
           infspec(3) = 1
           npl_mix(2) = 5000.
           npl_mix(3) = 5000.
     end select
     do i =1,nspec_tree
         infhelp(i) = infspec(i)
     end do
     do i = 1,nspec_tree
         if (infspec(i).eq.1 .and. infhelp(i).eq.1) then

               taxid = i
               age = pl_age(taxid)
               pl_height = plant_height(taxid)
               plhmin = plant_hmin(taxid)
               nplant = rednpl_sh*nint(npl_mix(taxid)*kpatchsize/10000)
               sdev = hsdev(taxid)
               call gener_coh(taxid, age, pl_height, plhmin, nplant,sdev)
               infhelp(i) = 0
          end if
     end do  ! i
 end if
 end if

END SUBROUTINE planting

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!         gener_coh
!        SR for planting seedling cohorts
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

SUBROUTINE gener_coh(taxid,age,pl_height,plhmin, nplant,sdev)

 USE data_simul
 USE data_stand
 USE data_par
 USE data_species
 USE data_soil
 USE data_help
 USE data_plant
 USE data_manag
 
 IMPLICIT NONE
 integer    :: nplant,       &
               taxid,        &
               nclass,       &
               i,j,nr
 real       :: age,          &
               pl_height,    &
               sdev,         &
               plhmin,       &
			   plhmax,       &
			   plhinc,       &
               help,         &
               nstot,        &
               hhelp,x1,x2,xacc,shelp
real         :: rtflsp, sapwood
real         :: troot2

real, dimension(:), save, allocatable   :: hei,     &
                                     nschelp

integer,dimension(:),allocatable  :: nsc
TYPE(cohort)    ::tree_ini

external sapwood
external rtflsp

flag_standup = 2   ! call stand_balance and root_distribution later

! number of classes
  nclass = nint(plhmin + (pl_height-plhmin))
! Liocourt management 
 if(flag_reg.eq.17) nclass = 20
 if(flag_reg.eq.11 .and. flag_mg.eq.44) nclass = 1
 if(flag_reg.eq.18) nclass =20
 if (flag_reg.eq.15)    nclass = 20
  allocate(hei(nclass))
  allocate(nschelp(nclass))
  allocate(nsc(nclass))

   plhmax = pl_height + (pl_height-plhmin)
   plhinc = (plhmax-plhmin)/nclass
   nstot = 0
   help =  (1/(sqrt(2*pi)*sdev))
   do i = 1, nclass
   if ( nclass.eq.1) then 
         hei(i) = pl_height
   else
! height per class
          hei(i) = plhmin + (i-1)
          nschelp(i) = help*exp(-((hei(i)-pl_height)**2)/(2*(sdev)**2))
          nstot = nstot + nschelp (i)
    end if
   end do
   do i = 1,nclass
   if(nclass.eq.1) then
        nsc(i) = nplant *kpatchsize/10000
    else
         nsc(i) = nint((nschelp(i)*nplant/nstot) + 0.5)
    end if
   end do

   do i = 1,nclass

     max_coh = max_coh + 1
! initialise tree_ini with zero
   call coh_initial (tree_ini)

   tree_ini%ident =  max_coh
   tree_ini%species = taxid
   tree_ini%ntreea = nsc(i)
   tree_ini%nta = tree_ini%ntreea
   tree_ini%x_age = age
   tree_ini%height = hei(i)

    hhelp =  tree_ini%height

    IF (taxid.ne.2) tree_ini%x_sap = exp(( LOG(hhelp)-LOG(spar(taxid)%pheight1))/spar(taxid)%pheight2)/1000000.
    IF (taxid.eq.2) THEN
        x1 = 1.
        x2 = 2.
        xacc=(1.0e-10)*(x1+x2)/2
        heihelp = tree_ini%height
        hnspec = taxid
        shelp=rtflsp(sapwood,x1,x2,xacc)
        tree_ini%x_sap = (10**shelp)/1000000         !  transformation mg ---> kg
    ENDIF

! Leaf mass
    tree_ini%x_fol = (spar(taxid)%seeda*(tree_ini%x_sap** spar(taxid)%seedb))   ![kg]
    tree_ini%Fmax = tree_ini%x_fol
! Fine root mass rough estimate
     tree_ini%x_frt = tree_ini%x_fol
! cross sectional area of heartwood
     tree_ini%x_crt = (tree_ini%x_sap + tree_ini%x_hrt) * spar(taxid)%alphac*spar(taxid)%cr_frac
     tree_ini%x_tb = (tree_ini%x_sap + tree_ini%x_hrt) * spar(taxid)%alphac*(1.-spar(taxid)%cr_frac)

    tree_ini%med_sla = spar(taxid)%psla_min + spar(taxid)%psla_a*0.5
    tree_ini%t_leaf = tree_ini%med_sla* tree_ini%x_fol                              ! [m-2]
    tree_ini%ca_ini = tree_ini%t_leaf
    tree_ini%crown_area = tree_ini%ca_ini
!   1 f�r Vincent kint, 2 oakchain
    tree_ini%underst = 2
!   initialize pheno state variables
    IF(spar(tree_ini%species)%Phmodel==1) THEN
       tree_ini%P=0
       tree_ini%I=1
    ELSE
       tree_ini%P=0
       tree_ini%I=0
       tree_ini%Tcrit=0
    END IF

    IF(nsc(i).ne.0.) then
       IF (.not. associated(pt%first)) THEN
          ALLOCATE (pt%first)
          pt%first%coh = tree_ini
          NULLIFY(pt%first%next)
        call root_depth (1, pt%first%coh%species, pt%first%coh%x_age, pt%first%coh%height, pt%first%coh%x_frt, pt%first%coh%x_crt, nr, troot2, pt%first%coh%x_rdpt, pt%first%coh%nroot)
        pt%first%coh%nroot = nr
        do j=1,nr
           pt%first%coh%rooteff(j) = 1.   ! assumption for the first use
        enddo
        do j=nr+1, nlay
           pt%first%coh%rooteff(j) = 0.   ! layers with no roots
        enddo

      ELSE
          ALLOCATE(zeig)
          zeig%coh = tree_ini
          zeig%next => pt%first
          pt%first => zeig
        call root_depth (1, zeig%coh%species, zeig%coh%x_age, zeig%coh%height, zeig%coh%x_frt, zeig%coh%x_crt, nr, troot2, zeig%coh%x_rdpt, zeig%coh%nroot)
        zeig%coh%nroot = nr
        do j=1,nr
           zeig%coh%rooteff(j) = 1.   ! assumption for the first use
        enddo
        do j=nr+1, nlay
           zeig%coh%rooteff(j) = 0.   ! layers with no roots
        enddo

      END IF
      anz_coh=anz_coh+1
    END IF

end do

   deallocate(hei)
  deallocate(nschelp)
  deallocate(nsc)

END SUBROUTINE gener_coh

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!    weight
!     seed mass function
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

function sapwood (x)

use data_help
use data_species

real     :: x
real     :: p1,p2,p3

p1 = spar(hnspec)%pheight1
p2 = spar(hnspec)%pheight2
p3 = spar(hnspec)%pheight3

sapwood = p3*(x**2) + p2*x +p1-alog10(heihelp)

end function sapwood
