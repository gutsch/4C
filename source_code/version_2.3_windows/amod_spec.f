!*****************************************************************!
!*                                                               *!
!*           4C Simulation Model: Module data_species            *!
!*                                                               *!
!*                                                               *!
!*               module for species parameters                   *!
!*                                                               *!
!*                  Copyright (C) 1996-2018                      *!
!*     Potsdam Institute for Climate Impact Reserach (PIK)       *!
!*          Authors and contributors see AUTHOR file             *!
!*  This file is part of 4C and is licensed under BSD-2-Clause   *!
!*                   See LICENSE file or under:                  *!
!*     http://www.https://opensource.org/licenses/BSD-2-Clause   *!
!*                           Contact:                            *!
!*       https://gitlab.pik-potsdam.de/foresee/4C                *!
!*                                                               *!
!*****************************************************************!

MODULE data_species

  ! general parameters
  INTEGER   :: nspecies          ! number of all species (incl. ground vegetation)
  INTEGER   :: nspec_tree        ! number of tree species
  INTEGER   :: spec_help = 1     ! aux var for species number
  REAL      :: weibal = 1.5      ! mortality parameter (NOT species-specific)
  REAL      :: weibal_int = 0.1  ! mortality parameter of intrinsic mortality
  REAL      :: NPP_demand_mistletoe !helping var. to substract demand of mistletoe from pine cohort

  ! species-specific parameters
  TYPE species_par
    CHARACTER (len=30) :: species_name
    CHARACTER (len=15) :: species_short_name

    ! mortality parameters
    INTEGER :: max_age        ! maximum tree age                                  [yr]
    INTEGER :: yrec           ! stress recovery time                              [yr]
    INTEGER :: stol           ! shade tolerance class                             [1=intol, 5=tol]
    REAL    :: intr           ! intrinsic mortality rate                          [?]
    REAL    :: weibla         ! lambda parameter of Weibull distribution          [?]

   ! photosynthesis parameters
    REAL    :: psla_min       ! minimum specific one-sided leaf area                      [m2/kg DW]
    REAL    :: psla_a         ! light dep. specific one-sided leaf area                      [m2/kg DW]
    REAL    :: phic           ! efficiency parameter, different for everg/decid   [-]
    REAL    :: pnc            ! leaf N content                                    [mg/g]
    REAL    :: kco2_25        ! Michaelis constant for CO2 (base 25 °C)           [Pa]
    REAL    :: ko2_25         ! inhibition constant of O2  (base 25 °C)           [kPa]
    REAL    :: pc_25          ! CO2/O2 specificity ratio   (base 25 °C)           [-]
    REAL    :: q10_kco2       ! Q10 coefficients (acclimated to 25 °C)            [-]
    REAL    :: q10_ko2        !                                                   [-]
    REAL    :: q10_pc         !                                                   [-]
    REAL    :: pb             ! Rd to Vm ratio                                    [-]
    REAL    :: Nresp          ! slope of photosynthesis response to Nitrogen      [yr/kg/ha]

    ! NPP parameters
    REAL    :: respcoeff      ! respiration coefficient
    REAL    :: prg            ! growth respiration                                [/day]
    REAL    :: prms           ! maintenance resp. (base 15 °C): sapwood,          [/day]
    REAL    :: prmr           !                                 fine roots        [/day]
    REAL    :: q10_prms       ! Q10 coefficients (acclimated to 15 °C)            [-]
    REAL    :: q10_prmr       !                                                   [-]

    ! allocation parameters
    REAL    :: pfext          ! extinction coefficient
    REAL    :: sigman         ! root activity rate (N uptake)                     [/yr]
    REAL    :: psf            ! senescence rates: foliage,                        [/yr]
    REAL    :: pss            !                   sapwood,                        [/yr]
    REAL    :: psr            !                   fine roots                      [/yr]
    REAL    :: pcnr           ! N/C ratio of biomass                              [kg N/kg C]
    REAL    :: cnr_fol        ! C/N ratio of foliage                              [kg C/kg N]
    REAL    :: cnr_frt        ! C/N ratio of fine roots                           [kg C/kg N]
    REAL    :: cnr_crt        ! C/N ratio of coarse roots                         [kg C/kg N]
    REAL    :: cnr_tbc        ! C/N ratio of twigs and branches                   [kg C/kg N]
    REAL    :: cnr_stem       ! C/N ratio of stemwood                             [kg C/kg N]
    REAL    :: ncon_fol       ! N concentration of foliage                        [mg/g]
    REAL    :: ncon_frt       ! N concentration of fine roots                     [mg/g]
    REAL    :: ncon_crt       ! N concentration of coarse roots                   [mg/g]
    REAL    :: ncon_tbc       ! N concentration of twigs and branches             [mg/g]
    REAL    :: ncon_stem      ! N concentration of stemwood                       [mg/g]
    REAL    :: reallo_fol     ! reallocation parameter of foliage
    REAL    :: reallo_frt     ! reallocation parameter of fine root
    REAL    :: prhos          ! sapwood density                                   [kg/cm3]
    REAL    :: pnus           ! foliage to sapwood area relationship              [kg/cm2]
    REAL    :: alphac         ! (twigs, branches & coarse roots) to sapwood ratio [-]
    REAL    :: cr_frac        ! fraction of tbc (twigs, branches, roots) that is coarse roots [-]
    REAL    :: pha            ! height growth rate                                [cm/kg]
    REAL    :: pha_coeff1     !   "   coefficient 1
    REAL    :: pha_coeff2     !   "   coefficient 2
    REAL    :: pha_v1         ! parameter for non-linear height-foliage relationship
    REAL    :: pha_v2         !   "
    REAL    :: pha_v3         !   "
    REAL    :: crown_a        ! parameter to calculate crown radius from DHB       [m/cm]
    REAL    :: crown_b        ! parameter to calculate crown radius from DHB       [m]
    REAL    :: crown_c        ! parameter to calculate crown radius from DHB       [m]

    ! decomposition parameters per fraction
    REAL    :: k_opm_fol      ! mineralization constant of foliage litter / per day
    REAL    :: k_syn_fol      ! synthesis coefficient of foliage litter / fraction
    REAL    :: k_opm_tb       ! mineralization constant of twigs and branches litter / per day
    REAL    :: k_syn_tb       ! synthesis coefficient of twigs and branches litter / fraction
    REAL    :: k_opm_stem     ! mineralization constant of stemwood / per day
    REAL    :: k_syn_stem     ! synthesis coefficient of stemwood / fraction
    REAL    :: k_opm_frt      ! mineralization constant of fine root / per day
    REAL    :: k_syn_frt      ! synthesis coefficient of fine root / fraction
    REAL    :: k_opm_crt      ! mineralization constant of coarse root / per day
    REAL    :: k_syn_crt      ! synthesis coefficient of coarse root / fraction

    ! phenology parameters
    ! PIM: Promotor-Inhibitor model
    ! CSM: Cannel and Smoth model
    ! TSM: linear temperature sum model
    REAL    :: PItmin         ! PIM: Inhibitor min temp.  [°C]
    REAL    :: PItopt         ! PIM: Inhibitor opt temp. [°C]
    REAL    :: PItmax         ! PIM: Inhibitor max temp. [°C]
    REAL    :: PIa            ! PIM: Inhibitor scaling factor [-]
    REAL    :: PPtmin         ! PIM: Promotor min temp.  [°C]
    REAL    :: PPtopt         ! PIM: Promotor opt temp. [°C]
    REAL    :: PPtmax         ! PIM: Promotor max temp. [°C]
    REAL    :: PPa            ! PIM: Promotor scaling factor [-]
    REAL    :: PPb            ! PIM: Promotor scaling factor [-]
    REAL    :: CSTbC          ! CSM: chilling base temp.  [°C]
    REAL    :: CSTbT          ! CSM: base temp.           [°C]
    REAL    :: CSa            ! CSM: scaling factor       [-]
    REAL    :: CSb            ! CSM: scaling factor       [-]
    REAL    :: LTbT           ! TSM: base temp.            [°C]
    REAL    :: LTcrit         ! TSM: critical temperature sum  [°C]
    integer :: Lstart         ! TSM: start day after 1.11.
    integer :: Phmodel        ! used pheno model 0: no model, 1: PIM, 2: CSM, 3: TSM

    REAL    :: end_bb         ! last day for vegetation period
    integer :: flag_endbb = 0

    ! Canopy parameters
    REAL    :: ceppot_spec    ! species parameter for pot. intercept.  [mm/m2 leaf area]
    REAL    :: fpar_mod       ! Parameter in canopy_geom (Petra) temp?

   ! regeneration parameter
    REAL    :: regflag        ! flag for regenration control
    REAL    :: seedrate       ! maximum seed rate  per m2
    REAL    :: seedmass       ! mass of single seed [g DW], mean value
    REAL    :: seedsd         ! standard deviation of seed mass
    REAL    :: seeda          ! parameter of shoot biomass - foliage mass emp. relation
    REAL    :: seedb          !    ------------"-------------
    REAL    :: pheight1       ! parameter of shoot biomass - height emp. relation
    REAL    :: pheight2       !         ---------"--------------
    REAL    :: pheight3       !         ---------"--------------
    REAL    :: pdiam1         ! parameter of shoot biomass -diameter emp. relation
    REAL    :: pdiam2         !        -------------"-----------
    REAL    :: pdiam3         !        -------------"-----------

   ! parameter for root growth model
    REAL    :: spec_rl        ! specific root length [m/g DW]
    REAL    :: tbase          ! minimum temperature for root growth [°C]
    REAL    :: topt           ! optimum temperature for root growth [°C]
    REAL    :: bdmax_coef      ! for equation of maximum bulk density for root growth []
    REAL    :: porcrit_coef    ! for equation critical pore space for aeration []
    REAL    :: ph_opt_max     ! maximum pH-value for optimal root growth
    REAL    :: ph_opt_min     ! minimum pH-value for optimal root growth
    REAL    :: ph_max         ! maximum pH-value for root growth
    REAL    :: ph_min         ! minimum pH-value for root growth
    REAL    :: v_growth       ! maximum velocity of coarse root growth [cm/day]
 
  END type species_par

  TYPE (species_par),allocatable,save,dimension(:),target :: spar

END MODULE data_species
